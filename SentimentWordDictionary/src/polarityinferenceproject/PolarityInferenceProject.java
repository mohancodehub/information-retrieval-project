package polarityinferenceproject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import data.DictionaryEntry;
import data.Utils.Speech;

public class PolarityInferenceProject {
	
	Speech runForSpeech;
	ArrayList<DictionaryEntry> dictionary;
	
	public ArrayList<DictionaryEntry> getDictionary() {
		return dictionary;
	}

	public void setDictionary(ArrayList<DictionaryEntry> dictionary) {
		this.dictionary = dictionary;
	}

	public PolarityInferenceProject(ArrayList<DictionaryEntry> dict,Speech run) {
		runForSpeech = run;
		this.dictionary = dict;
	}
	
	public void invokePolarityInferenceProject(String path) {
		System.out.println("Running Polarity Inference Project");
		writeDictionaryToSQLDatabase();
		runPIRExe(path);
	}
	
	public void runPIRExe(String path) {
		Process process;
		try {
			System.out.println(path);
			ProcessBuilder pb = new ProcessBuilder(	path + "PolarityInferenceEngine.exe",runForSpeech.toString());
			pb.directory(new File(path + "\\"));
			process = pb.start();
			InputStream is = process.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;
			System.out.println("Output of running exe is:");
			while ((line = br.readLine()) != null && !line.equalsIgnoreCase("ENDPROGRAM")) {
				System.out.println(line);
			}

			process.destroy();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void writeDictionaryToSQLDatabase() {
		String url = "jdbc:sqlserver://localhost;databaseName=wordnet;integratedSecurity=true";
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection conn = DriverManager.getConnection(url);
			String query = "delete from dbo.InputFromJava";
			PreparedStatement st = conn.prepareStatement(query);
			st.executeUpdate();
			st.close();
			query = "insert into dbo.InputFromJava values (?,?,?)";
			PreparedStatement pStatement = conn.prepareStatement(query);
			int batchSize = 100;
			for (int count = 0; count < dictionary.size(); count++) {
				// System.out.println(dictionary.get(count).getWord().toString());
				pStatement.setString(1, dictionary.get(count).getWord().toString());
				pStatement.setString(2, dictionary.get(count).getSpeech().toString());
				pStatement.setString(3, dictionary.get(count).getPolarity().toString());
				pStatement.addBatch();

				if (count % batchSize == 0) {
					pStatement.executeBatch();
				}
			}

			pStatement.executeBatch(); // for remaining batch queries if total
										// record is odd no.

			// conn.commit();
			pStatement.close();
			conn.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

}
