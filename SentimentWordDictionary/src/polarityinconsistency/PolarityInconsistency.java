package polarityinconsistency;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.sat4j.specs.TimeoutException;

import edu.mit.jwi.item.POS;
import edu.uic.cs.cs582.data.Component;
import edu.uic.cs.cs582.data.Synset;
import edu.uic.cs.cs582.data.SynsetSet;
import edu.uic.cs.cs582.data.Word;
import edu.uic.cs.cs582.misc.ClassFactory;
import edu.uic.cs.cs582.misc.Config;
import edu.uic.cs.cs582.misc.GeneralException;
import edu.uic.cs.cs582.process.LogicFormulaProcessor;
import edu.uic.cs.cs582.process.SatProcessor;
import edu.uic.cs.cs582.process.WordsComponentBuilder;
import edu.uic.cs.cs582.process.impl.Sat4jProcessorImpl;
import edu.uic.cs.cs582.process.impl.WordsComponentBuilderImpl;

//import edu.uic.cs.cs582.misc.Config;

public class PolarityInconsistency {
	// private static final Logger LOGGER =
	// LogHelper.getLogger(PolarityInconsistency.class);

	private static final String INPUT_FOLDER_NAME = "input";
	private static final String INPUT_INFO_FILE = "inputFiles.properties";
	private static OutputStream OUTPUTSTREAM = null;
	protected static String wordNetPath = "";

	PolarityInconsistency(String inputFile, String outputFile, String speech) {
		try {
			preExecute(outputFile);

			// Properties inputFiles = readInputFilesInfomation();
			// List<Entry<Object, Object>> inputFilesList =
			// sortFileNames(inputFiles);

			POS pos = POS.valueOf(speech.toUpperCase(Locale.US));
			processOneFileOfCertainPos(inputFile, pos);
			
		} finally {
			postExecute();
		}
	}

	private static void preExecute(String outputFileName) {
		File outputFile = new File(outputFileName);
		try {
			OUTPUTSTREAM = FileUtils.openOutputStream(outputFile);
		} catch (IOException ioe) {
			throw new GeneralException(ioe);
		}
	}

	public PolarityInconsistency(String wordNetPath) {
		this.wordNetPath = wordNetPath;
		try {
			preExecute("res//PolarityInconsistencyProject//wontbeused.txt");
		} finally {
			postExecute();
		}
	}

	public Set<String> processFileToRemoveInconsistencies(String inputFileName,	String speech) {
		POS pos = POS.valueOf(speech.toString().toUpperCase(Locale.US));
		// System.out.println("input file: " + inputFileName);
		return processOneFileOfCertainPos(inputFileName, pos);
	}

	// PolarityInconsistency()
	// {
	// try
	// {
	// preExecute();
	//
	// Properties inputFiles = readInputFilesInfomation();
	// List<Entry<Object, Object>> inputFilesList = sortFileNames(inputFiles);
	//
	//
	// for (Entry<Object, Object> entry : inputFilesList)
	// {
	// String fileName = entry.getKey().toString();
	// POS pos = POS.valueOf(entry.getValue().toString()
	// .toUpperCase(Locale.US));
	//
	// processOneFileOfCertainPos(fileName, pos);
	// }
	// }
	// finally
	// {
	// postExecute();
	// }
	// }

	private static void preExecute() {
		File outputFile = new File(Config.OUTPUT_FILE_PATH);
		try {
			OUTPUTSTREAM = FileUtils.openOutputStream(outputFile);
		} catch (IOException ioe) {
			throw new GeneralException(ioe);
		}
	}

	private static List<Map.Entry<Object, Object>> sortFileNames(Properties inputFiles) {
		List inputFilesList = new ArrayList(inputFiles.entrySet());
		Collections.sort(inputFilesList,new Comparator<Entry<Object, Object>>() {

					public int compare(Map.Entry<Object, Object> one,Map.Entry<Object, Object> other) {
						String oneInString = one.getKey().toString() + one.getValue().toString().toUpperCase(Locale.US);
						String otherInString = other.getKey().toString() + other.getValue().toString().toUpperCase(Locale.US);
						return oneInString.compareTo(otherInString);
					}

				});
		return inputFilesList;
	}

	private static Properties readInputFilesInfomation() {
		Properties inputFiles = new Properties();
		InputStream inStream = ClassLoader.getSystemResourceAsStream("inputFiles.properties");

		try {
			inputFiles.load(inStream);
		} catch (IOException e) {
			throw new GeneralException(e);
		}

		return inputFiles;
	}

	private static void postExecute() {
		IOUtils.closeQuietly(OUTPUTSTREAM);
	}

	private static void println(String line) {
		System.out.println(line);

		try {
			IOUtils.write(line, OUTPUTSTREAM);
			IOUtils.write(IOUtils.LINE_SEPARATOR, OUTPUTSTREAM);
		} catch (IOException e) {
			// LOGGER.error("Catch IOException while write line to output file. ",
			// e);
		}
	}

	private static void println() {
		println("");
	}

	@SuppressWarnings("rawtypes")
	private static Set<String> processOneFileOfCertainPos(String fileName,POS pos) {
		println("Input file [" + fileName + "]. ");
		//String inputPath = PolarityInconsistency.class.getClassLoader().getResource(".").getFile() + "input" + "/" + fileName;
	
		String inputPath = fileName;
		System.out.println(inputPath);
		WordsComponentBuilder componentBuilder = new WordsComponentBuilderImpl(wordNetPath);

		// println("Begin to bulid components for [" + pos +
		// "], please stand by... ");

		List components = componentBuilder.buildComponents(inputPath, pos);
		
		// println("End to bulid components for [" + pos + "]. Total [" +
		// components.size() + "] components. ");
		// println("Begin to process each component, please stand by... ");
		// println();
		// println("=====================================");
		//
		Set<String> allConflictWords = new HashSet<String>();
		Map distribution = new HashMap();

		long beginTime = System.currentTimeMillis();

		Component component = null;
		Integer inconsistenciesSize;
		for (int componentCount = 1; componentCount <= components.size(); componentCount++) {
			component = (Component) components.get(componentCount - 1);
			int beginSize = allConflictWords.size();
			processEachComponent(pos, componentCount, component,allConflictWords, false);
			int endSize = allConflictWords.size();

			inconsistenciesSize = Integer.valueOf(endSize - beginSize);
			if (inconsistenciesSize.intValue() > 0) {
				Integer countForDistribution = (Integer) distribution.get(inconsistenciesSize);
				if (countForDistribution == null) {
					countForDistribution = Integer.valueOf(0);
				}

				distribution.put(inconsistenciesSize,countForDistribution = Integer.valueOf(countForDistribution.intValue() + 1));
			}
		}

		long endTime = System.currentTimeMillis();
		// println("End to process all components in [" + (endTime - beginTime)
		// /
		// 1000L + "] seconds. Total [" + allConflictWords.size() +
		// "] inconsistent words.");
		//
		// println("Distribution(cardinality=number): " + distribution + "\n");

		for (String wordString : allConflictWords) {
			println(wordString);
		}
		println("\n\n");

		// LOGGER.warn("##################################################################################################");
		System.out.println("method ending !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		return allConflictWords;
	}

	private static void processEachComponent(POS pos, int componentCount,Component component, Set<String> allConflictWords,boolean isRecursive) {
		LogicFormulaProcessor formulaProcessor = (LogicFormulaProcessor) ClassFactory.getInstance(Config.LOGIC_FORMULA_PROCESSOR_CLASS_NAME);
		SatProcessor satProcessor = new Sat4jProcessorImpl(component);

		try {
			Map indicesByCnfClause = formulaProcessor.generateCNF(component,pos);
			boolean result = satProcessor.processSat(indicesByCnfClause);
			if (!result) {
				if (!isRecursive) {
					// println(
					// "**** Inconsistency detected! - Component[" +
					// componentCount + "] | " + component);
				}

				printOutInconsistenciesForEachComponent(allConflictWords,satProcessor);
				Set certainlyInconsistentWords = getInconsistentWordsWhoseAllMajoritySynsetsAreInconsistent(component, satProcessor);
				if (!certainlyInconsistentWords.isEmpty()) {
					component = component.clone(certainlyInconsistentWords);
					processEachComponent(pos, componentCount, component,allConflictWords, true);
				}

				if (!isRecursive) {
					// println("=====================================");
				}
			}
		} catch (TimeoutException e) {
			println("[" + componentCount + "] Time out, we cannot solve this component - "	+ component);
			println("=====================================");
		}
	}

	private static Set<Word> getInconsistentWordsWhoseAllMajoritySynsetsAreInconsistent(Component component, SatProcessor satProcessor) {
		Set result = new TreeSet();
		Map conflictWordsBySynset = satProcessor.getConflictWordsBySynset();
		Map conflictWordsByMajoritySynsets = satProcessor.getConflictWordsBySynsetsInMajority();

		Set<Word> allConflictWords = new HashSet<Word>();
		Set<Synset> allConflictSynsets = new HashSet<Synset>();
		fetchAllConflictWordsAndSynsets(conflictWordsBySynset,conflictWordsByMajoritySynsets, allConflictWords,	allConflictSynsets);

		for (Word eachConflictWord : allConflictWords) {
			if (allConflictSynsets.containsAll(eachConflictWord.getAllSynsetsInMajoritySynsets())) {
				result.add(eachConflictWord);
			}
		}
		return result;
	}

	private static void fetchAllConflictWordsAndSynsets(
			Map<Synset, Set<Word>> conflictWordsBySynset,
			Map<SynsetSet, Set<Word>> conflictWordsByMajoritySynsets,
			Set<Word> allConflictWords, Set<Synset> allConflictSynsets) {
		for (Map.Entry entry : conflictWordsBySynset.entrySet()) {
			allConflictWords.addAll((Collection) entry.getValue());
			allConflictSynsets.add((Synset) entry.getKey());
		}

		for (Entry<SynsetSet, Set<Word>> entry : conflictWordsByMajoritySynsets.entrySet()) {
			allConflictWords.addAll(entry.getValue());
			allConflictSynsets.addAll(entry.getKey());
		}
	}

	private static void printOutInconsistenciesForEachComponent(Set<String> allConflictWords, SatProcessor satProcessor) {
		Map<SynsetSet, Set<Word>> conflictWordsByMajoritySynsets = satProcessor.getConflictWordsBySynsetsInMajority();
		for (Map.Entry<SynsetSet, Set<Word>> entry : conflictWordsByMajoritySynsets.entrySet()) {
			String each = ">>>>>>>>* ";
			for (Word word : entry.getValue()) {
				each += word.getWord() + "[" + word.getPolarity() + "] ";
				allConflictWords.add(word.getWord());
			}
			// println(each + " -- " + entry.getKey());
		}

		// /////////////////
		Map<Synset, Set<Word>> conflictWordsBySynset = satProcessor.getConflictWordsBySynset();
		for (Map.Entry<Synset, Set<Word>> entry : conflictWordsBySynset.entrySet()) {
			String each = ">>>>>>>>  ";
			boolean hasWordsToDisplay = false;
			for (Word word : entry.getValue()) {
				if (!allConflictWords.contains(word.getWord())) {
					each += word.getWord() + "[" + word.getPolarity() + "] ";
					hasWordsToDisplay = allConflictWords.add(word.getWord());
				}
			}

			if (hasWordsToDisplay) {
				println(each + " -- (" + entry.getKey() + ") ");
			}
		}
	}
}