package meronym;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import sentimentdictionaryapplication.SWDOperations;
import data.DictionaryEntry;
import data.SynsetEntry;
import data.Utils;
import data.Utils.Polarity;
import data.Utils.Speech;
import edu.smu.tspell.wordnet.NounSynset;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;

public class Meronym {
	ArrayList<String> nounWordsList;
	ArrayList<String> adjWordsList;
	ArrayList<String> advWordsList;
	ArrayList<String> verbWordsList;
	WordNetDatabase database;
	SWDOperations swd;
	Speech partOfSpeech;
	ArrayList<DictionaryEntry> allDictionary;
	
	public Meronym(SWDOperations swd, WordNetDatabase database, Speech partOfSpeech) throws IOException {
		this.database = database;
		this.swd = swd;
		this.allDictionary = swd.getEntireDictionaryForMeronymProject();


		ArrayList<String> previousSynsets = Utils.readFromFile("res/meronym/PreviouslyExecuted.txt");
		if(previousSynsets.size() != 0) {
			addToMainSynsets(previousSynsets);
		} else {
			this.partOfSpeech = Speech.NOUN;
			nounWordsList = Utils.readFromFile("res/wordnet-words/index.noun");
			findNounSynsetsWithMeronyms(nounWordsList, "res/meronym/nounWordsWithMeronyms.txt", false);			
		}
	}

	/**
	 * For each word in wordnet, get the noun synsets 
	 * and check if the noun synsets have member-meronyms.
	 * If true, then check if the definition of that 
	 * synset has atleast one word which is +ve or -ve.
	 * If none of the words are +ve or -ve, add the 
	 * synset to a list with neutral polarity.
	 * @param nounWordsList
	 * @param outFilename
	 * @return
	 * @throws IOException 
	 */
	@SuppressWarnings("rawtypes")
	public void findNounSynsetsWithMeronyms(
		ArrayList<String> nounWordsList, String outFilename, boolean writeToFile) throws IOException {
		Utils.print("Finding Noun Synsets with Meronyms having neutral polarity");
		NounSynset nounSynset;
		BufferedWriter out = outFile(outFilename);
		
		int numberMeronyms = 0;
		for (String wordForm : nounWordsList) {
			Synset[] synsets = database.getSynsets(wordForm, SynsetType.NOUN);
			
			for (int i = 0; i < synsets.length; i++) {
				nounSynset = (NounSynset) (synsets[i]);
//				for (NounSynset ns : nounSynset.getMemberMeronyms())
//					meronyms.add(ns);
//				for (NounSynset ns : nounSynset.getMemberHolonyms())
//					meronyms.add(ns);
				
				numberMeronyms = nounSynset.getMemberMeronyms().length;
//				numberMeronyms += nounSynset.getMemberHolonyms().length;
//				numberMeronyms += nounSynset.getInstanceHyponyms().length;
//				numberMeronyms += nounSynset.getInstanceHypernyms().length;
				
				if(writeToFile && numberMeronyms>0) {
					out.write(nounSynset.getWordForms()[0]);
					out.newLine();
				}
				
				if(numberMeronyms>0) {
					String wordDefinition = nounSynset.getDefinition();
					HashMap<String, Speech> speechMap = Utils.tagString(wordDefinition);
					boolean flag = false;
					if(speechMap.size()>0) {
						Iterator it = speechMap.entrySet().iterator();
					    while (it.hasNext()) {
					        Map.Entry et = (Map.Entry)it.next();
					        Polarity p = getPolarityOfWord((String) et.getKey(), (Speech) et.getValue());
					        if(p == Polarity.POSITIVE || p == Polarity.NEGATIVE)
					        	flag = true;
					    }
					    if(!flag) {
//					    	swd.addToSynsetsList(new MeronymSynsetsEntry(meronyms, wordDefinition, Polarity.NEUTRAL));
					    	swd.addToSynsetsList(new SynsetEntry(wordForm, i, partOfSpeech, Polarity.NEUTRAL, "M"));
//					    	System.out.println(swd.allSynsets.get(swd.allSynsets.size()-1).toString());
					    }
					}
				}
			}
		}

		closeFile(out);

	}

	public BufferedWriter outFile(String filename) {
		BufferedWriter out = null;
		try{
			// Create file 
			FileWriter fstream = new FileWriter(filename);
			out = new BufferedWriter(fstream);
		}catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
		return out;
	}
	
	public void closeFile(BufferedWriter out) {
		try {
			out.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> getNounWordsList() {
		return nounWordsList;
	}

	public void setNounWordsList(ArrayList<String> nounWordsList) {
		this.nounWordsList = nounWordsList;
	}

	public ArrayList<String> getAdjWordsList() {
		return adjWordsList;
	}

	public void setAdjWordsList(ArrayList<String> adjWordsList) {
		this.adjWordsList = adjWordsList;
	}

	public ArrayList<String> getAdvWordsList() {
		return advWordsList;
	}

	public void setAdvWordsList(ArrayList<String> advWordsList) {
		this.advWordsList = advWordsList;
	}

	public ArrayList<String> getVerbWordsList() {
		return verbWordsList;
	}

	public void setVerbWordsList(ArrayList<String> verbWordsList) {
		this.verbWordsList = verbWordsList;
	}
	
	public Polarity getPolarityOfWord(String word, Speech speech) {
		for (DictionaryEntry entry : allDictionary)
			if (entry.getWord().equalsIgnoreCase(word)
					&& entry.getSpeech().equals(speech))
				return entry.getPolarity();

		return null;
	}
	
	public void addToMainSynsets(ArrayList<String> synsetsList) {
		ArrayList<String[]> stringList = Utils.splitListItemsWith(synsetsList, ",");
		for(String[] s : stringList)
		swd.addToSynsetsList(new SynsetEntry(s[0].toLowerCase().replaceAll("\\s", ""), Integer.parseInt(s[1].replaceAll("\\s", "")), Utils.getSpeech(s[2].toLowerCase().replaceAll("\\s", "")), Utils.getPolarity(s[3].toLowerCase().replaceAll("\\s", "")), "MeronymProject"));		
	}
}
