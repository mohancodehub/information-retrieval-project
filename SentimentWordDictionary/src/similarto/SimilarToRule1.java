package similarto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.ISynsetID;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.item.Pointer;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;



/*Class 'RecordData' which stores a word, its part-of-speech tag,
 * its polarity in General Inquirer (GI) dictionary,
 * its polarity in the OpinionFinder (OP) dictionary,
 * its polarity in the AppraisalLexicon (AL) dictionary,
 * its overall polarity based on the GI and OP dictionaries,
 * the number of positive content words (of a given pos tag) in its WordNet senses,
 * the number of negative content words (of a given pos tag) in its WordNet senses
 * and its overall polarity based on its WordNet senses*/
public class SimilarToRule1 {
	int countoflines=0;
	String pathforwnhome = "";
	POS inputPos = null;
	private static final Logger DRIVER_LOG = Logger.getLogger("src");
	public class RecordData {
		String rdword;
		String rdpos;
		String rdpolOP;
		String rdpolGI;
		String rdpolAL;
		String rdoverpol;
		String rdwnoverpol;
		int rdposflag;
		int rdnegflag;
		RecordData() {
			this.rdword="";
			this.rdpos="";
			this.rdpolOP="";
			this.rdpolGI="";
			this.rdpolAL="";
			this.rdoverpol="";
			rdposflag=0;
			rdnegflag=0;
			this.rdwnoverpol="";
		}
		RecordData(String _rdword) {
			this.rdword=_rdword;
			this.rdpos="";
			this.rdpolOP="";
			this.rdpolGI="";
			this.rdpolAL="";
			this.rdoverpol="";
			rdposflag=0;
			rdnegflag=0;
			this.rdwnoverpol="";
		}
		void setWord(String _rdword) {
			this.rdword=_rdword;
		}
		void setPos(String _rdpos) {
			this.rdpos=_rdpos;
		}

		void setPosFlag(int _posflag) {
			this.rdposflag=_posflag;
			this.setWNOverPol();
		}

		void setNegFlag(int _negflag) {
			this.rdnegflag=_negflag;
			this.setWNOverPol();
		}

		void setWNOverPol(String _wnoverpol) {
			this.rdwnoverpol=_wnoverpol;
		}

		void setWNOverPol() {
			if(this.rdposflag>=1 && this.rdnegflag==0)
				this.rdwnoverpol="positive";
			else if (this.rdposflag==0 && this.rdnegflag>=1)
				this.rdwnoverpol="negative";
			else if (this.rdposflag==0 && this.rdnegflag==0)
				this.rdwnoverpol="neutral";
			else if (this.rdposflag>0 && this.rdnegflag>0)
				this.rdwnoverpol="ambiguous";
		}

		void setPolOP(String _rdpolOP) {
			this.rdpolOP=_rdpolOP;
			this.setOverPol();
		}
		void setPolGI(String _rdpolGI) {
			this.rdpolGI=_rdpolGI;
			this.setOverPol();
		}
		void setPolAL(String _rdpolAL) {
			this.rdpolAL=_rdpolAL;
			this.setOverPol();
		}
		/*This method decides the overall polarity of a word based on its polarities
		 * in the GI and OP dictionaries*/
		void setOverPol() {

			if(this.rdpolOP.equals("") || this.rdpolGI.equals("") || this.rdpolAL.equals("")) {
			}
			else {
				if(this.rdpolOP.equalsIgnoreCase(this.rdpolGI) && this.rdpolOP.equalsIgnoreCase(this.rdpolAL))
					this.rdoverpol=rdpolOP;
				else if(this.rdpolOP.equalsIgnoreCase("unknown"))
				{
					if(this.rdpolGI.equalsIgnoreCase("unknown") || this.rdpolGI.equalsIgnoreCase("neutral"))
					{
						this.rdoverpol=this.rdpolAL;
					}
					else if(this.rdpolGI.equalsIgnoreCase("positive"))
					{
						if(this.rdpolAL.equalsIgnoreCase("unknown") || this.rdpolAL.equalsIgnoreCase("positive") || this.rdpolAL.equalsIgnoreCase("neutral"))
						{
							this.rdoverpol=this.rdpolGI;
						}
						else if(this.rdpolAL.equalsIgnoreCase("negative"))
						{
							this.rdoverpol="ambiguous";
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("negative"))
					{
						if(this.rdpolAL.equalsIgnoreCase("unknown") || this.rdpolAL.equalsIgnoreCase("negative") || this.rdpolAL.equalsIgnoreCase("neutral"))
						{
							this.rdoverpol=this.rdpolGI;
						}
						else if(this.rdpolAL.equalsIgnoreCase("positive"))
						{
							this.rdoverpol="ambiguous";
						}
					}

				}
				else if(this.rdpolOP.equalsIgnoreCase("positive"))
				{
					if(this.rdpolGI.equalsIgnoreCase("unknown") || this.rdpolGI.equalsIgnoreCase("positive") || this.rdpolGI.equalsIgnoreCase("neutral"))
					{
						if(this.rdpolAL.equalsIgnoreCase("negative"))
						{
							this.rdoverpol="ambiguous";
						}
						else
						{
							this.rdoverpol=this.rdpolOP;
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("negative"))
					{
						this.rdoverpol="ambiguous";
					}
				}
				else if(this.rdpolOP.equalsIgnoreCase("negative"))
				{
					if(this.rdpolGI.equalsIgnoreCase("unknown") || this.rdpolGI.equalsIgnoreCase("negative") || this.rdpolGI.equalsIgnoreCase("neutral"))
					{
						if(this.rdpolAL.equalsIgnoreCase("positive"))
						{
							this.rdoverpol="ambiguous";
						}
						else
						{
							this.rdoverpol=this.rdpolOP;
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("positive"))
					{
						this.rdoverpol="ambiguous";
					}
				}
				else if(this.rdpolOP.equalsIgnoreCase("neutral"))
				{
					if(this.rdpolGI.equalsIgnoreCase("unknown") || this.rdpolGI.equalsIgnoreCase("neutral"))
					{
						if(this.rdpolAL.equalsIgnoreCase("unknown"))
						{
							this.rdoverpol=this.rdpolOP;
						}
						else
						{
							this.rdoverpol=this.rdpolAL;
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("positive"))
					{
						if(this.rdpolAL.equalsIgnoreCase("negative"))
						{
							this.rdoverpol="ambiguous";
						}
						else
						{
							this.rdoverpol=this.rdpolGI;
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("negative"))
					{
						if(this.rdpolAL.equalsIgnoreCase("positive"))
						{
							this.rdoverpol="ambiguous";
						}
						else
						{
							this.rdoverpol=this.rdpolGI;
						}
					}
				}
			}

		}
		String getWord() {
			return rdword;
		}
		String getPos() {
			return rdpos;
		}
		String getPolOP() {
			return rdpolOP;
		}
		String getPolGI() {
			return rdpolGI;
		}
		String getPolAL() {
			return rdpolAL;
		}
		String getOverPol() {
			return rdoverpol;
		}
		int getPosFlag() {
			return rdposflag;
		}
		int getNegFlag() {
			return rdnegflag;
		}
		String getWNOverPol() {
			return this.rdwnoverpol;
		}

	}
	public void testDictionary(String pathforwnhome, String inputFilePath, POS inputPos) throws IOException {
		try {
			this.pathforwnhome = pathforwnhome;
			this.inputPos = inputPos;
			/*RecordData rd;
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			Connection con = DriverManager.getConnection(
					"jdbc:odbc:SQL_SERVER;user=DEEPIKA-PC\\SQLEXPRESS;"
					+"database=wordnet");
			String pathforwnhome="C:\\WordNet_3.0_win32";
			String path = pathforwnhome + File.separator + "dict";
			URL url = new URL("file", null, path);
			IDictionary dict = new Dictionary(url);
			dict.open();*/
			RecordData rd;
			// Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con = DriverManager.getConnection(
					"jdbc:sqlserver://localhost;databaseName=wordnet;integratedSecurity=true");
			// Environment variable WNHOME consists of the base directory where
			// WordNet software resides
			// String wnhome = System.getenv("WNHOME");
			// System.out.println(pathforwnhome);

			String path = pathforwnhome + File.separator + "dict";
			URL url = new URL("file", null, path);
			IDictionary dict = new Dictionary(url);
			dict.open();
			// BufferedReader inp=new BufferedReader(new
			// InputStreamReader(System.in));
			// Accepting the input word and pos tag from the user

			//Initializing the POS tagger with model left3words-wsj-0-18.tagger
			MaxentTagger tagger = new MaxentTagger("libraries/left3words-wsj-0-18.tagger");

			//HashMap that maps 1,2,3,4 to pos tags
			HashMap<Integer,String> posMap=new HashMap<Integer,String>();
			posMap.put(1,"Adjective");
			posMap.put(2,"Adverb");
			posMap.put(3,"Noun");
			posMap.put(4,"Verb");

			//HashMap that maps the POS tag abbreviations from the tagger output to pos tags
			HashMap<String,String> posMap1=new HashMap<String,String>();
			posMap1.put("JJ","Adjective"); posMap1.put("JJR","Adjective"); posMap1.put("JJS","Adjective");
			posMap1.put("RB","Adverb"); posMap1.put("RBR","Adverb");	posMap1.put("RBS","Adverb");
			posMap1.put("NN","Noun"); posMap1.put("NNS","Noun"); posMap1.put("NNP","Noun");
			posMap1.put("NNPS","Noun"); posMap1.put("VB","Verb"); posMap1.put("VBD","Verb");
			posMap1.put("VBG","Verb"); posMap1.put("VBN","Verb"); posMap1.put("VBP","Verb");
			posMap1.put("VBZ","Verb"); posMap1.put("CC","coor_conj"); posMap1.put("CC","card_num");
			posMap1.put("DT","det"); posMap1.put("EX","exis"); posMap1.put("FW","for_word");
			posMap1.put("IN","prep"); posMap1.put("LS","list_item"); posMap1.put("MD","modal");
			posMap1.put("PDT","pre_det"); posMap1.put("POS","pos_ending"); posMap1.put("PRP","per_pronoun");
			posMap1.put("PRP$","pos_pronoun"); posMap1.put("RP","particle"); posMap1.put("SYM","symbol");
			posMap1.put("TO","to"); posMap1.put("UH","interjection"); posMap1.put("WDT","wh_det");
			posMap1.put("WP","wh_pnoun"); posMap1.put("WP$","pos_wh_pnoun"); posMap1.put("WRB","wh_adverb");
			posMap1.put("CD","constant");

			//Initializing stemmer


			BufferedReader br = new BufferedReader(
					new FileReader(
							inputFilePath));
			BufferedWriter pw = new BufferedWriter(
					new FileWriter(
							"res\\SimilarToProject\\output\\output.txt"));
			BufferedWriter wordbw = new BufferedWriter(
					new FileWriter(
							"res\\SimilarToProject\\output\\words.txt"));
			String  str;


			countoflines=0;
			DRIVER_LOG.log(Level.INFO, "Examine Process Started");
			ArrayList<String> isin=new ArrayList<String>();
			IIndexWord indexWord=null;
			IWord w=null;
			int synsetcount=0;
			int similartocount=0;
			while((str=br.readLine()) != null)
			{
				countoflines++;
				if (countoflines % 100 == 0) {
					DRIVER_LOG.log(Level.INFO, "Analyzed so far {0} words", countoflines);
				}

				String[] stra=str.split(",");
				String word = stra[0].trim();
				int position = Integer.parseInt(stra[1].trim());
				String polarity=stra[3].trim();
				/*if(!polarity.equalsIgnoreCase("NEUTRAL"))
				{
					continue;
				}*/
				////System.out.println("line : "+countoflines);
				////System.out.println(str);
				System.out.println(word);
				indexWord = dict.getIndexWord(word,inputPos);
				if(indexWord == null){
					continue;
				}
				//w=dict.getWord(indexWord);
				List<IWordID> wordID = indexWord.getWordIDs();
				Iterator<IWordID> i = wordID.iterator();
				int ki=0;
				/*for(int g=0;g<wordID.size();g++)
				{
					System.out.println("------->"+indexWord.getWordIDs().get(g));
				}*/
				ArrayList<String> ihyposin=new ArrayList<String>();
				//Displaying those senses of the word that have hyponyms
				//while(i.hasNext() && ki<wordID.size()) {
				if(position <= indexWord.getWordIDs().size()){

						IWordID wordID1 = indexWord.getWordIDs().get(position-1);
						////System.out.println(wordID1);
						ISynset s = dict.getSynset(wordID1.getSynsetID());
						////System.out.println((position-1)+" Synset : "+s.toString());
						String synsetstring=s.toString();


						String seq= "N-"+stra[1].trim()+"-"+word;
						/*if(!synsetstring.contains(seq))
						{
							ki++;
							continue;
						}*/
						//List<ISynsetID> sid=s.getRelatedSynsets(Pointer.HYPONYM);
						//Displaying the hyponyms of s
						//System.out.println("s.getRelatedSynsets(Pointer.HYPONYM)    "+sid.toString() +" \t size : "+sid.size());
						//if(sid.size()==0) {ki++;continue;}
						isin.add(wordID1.getSynsetID().toString());
						ISynsetID synsetID = s.getID();
						ihyposin=rule1(position-1, synsetID,s,polarity,tagger,dict,posMap,posMap1,pw,wordbw);
						ki=ki+1;
						HashSet<String> hs = new HashSet<String>();
						hs.addAll(ihyposin);
						ihyposin.clear();
						ihyposin.addAll(hs);
						similartocount+=ihyposin.size();
					//}//END OF WORD ITERATION

					hs = new HashSet<String>();
					hs.addAll(isin);
					isin.clear();
					isin.addAll(hs);
					synsetcount+=isin.size();
				}

			}

			HashSet<String> hs = new HashSet<String>();
			hs.addAll(isin);
			isin.clear();
			isin.addAll(hs);
			////System.out.println("Synset count : "+isin.size());
			////System.out.println("SimilarTo Synset count : "+similartocount);
			wordbw.flush();
			wordbw.close();
			pw.flush();
			pw.close();
			br.close();
			con.close();

		}
		catch(IOException e) {
			System.out.println("IO exception");
			System.out.println("line no : "+countoflines);
			System.out.println("Rule 1");
			e.printStackTrace();
		}
		catch(Exception e) {
			System.out.println();
			System.out.println();
			e.printStackTrace();
			System.out.println("line no- : "+countoflines);
			System.out.println("Rule 1");

		}
	}



	private ArrayList<String> rule1(int position, ISynsetID synsetID, ISynset s, String polarity,
			MaxentTagger tagger, IDictionary dict, HashMap<Integer, String> posMap, HashMap<String, String> posMap1, BufferedWriter pw, BufferedWriter wordbw) throws IOException {
		// TODO Auto-generated method stub
		Morphology morp=new Morphology();
		ArrayList<String> ihyposin=new ArrayList<String>();
		if(polarity.equalsIgnoreCase("Neutral"))
		{
			ihyposin = NeutralCase(position, synsetID,"Adjective",tagger, dict, posMap, posMap1,pw,wordbw);
			return ihyposin;
		}
		List<ISynsetID> sid=s.getRelatedSynsets(Pointer.SIMILAR_TO);
		//Displaying the hyponyms of s
		////System.out.println("s.getRelatedSynsets(Pointer.SIMILAR_TO)    "+sid.toString() +" \t size : "+sid.size());
		if(sid.size()==0) {return ihyposin;}

		int h=0;
		for(ISynsetID sid1 : sid){
			ISynset synset = dict.getSynset(sid1);

			for(IWord w : synset.getWords()) {
				////System.out.print("<"+w.getLemma()+"> ");
			}
			////System.out.print("- SimilarTo ID "+(h+1));
			////System.out.println("");
			h=h+1;
		}
		////System.out.println("After... ");

		OUTERLOOP:
		for(int ist=0;ist<sid.size();ist++)
		{
			String sprimesynsetidStr = sid.get(ist).toString();
			int sprimesynsetid;
			String[] split = sprimesynsetidStr.split("-");
			sprimesynsetid=Integer.parseInt(split[1]);
			////System.out.println("sprimesynsetid : "+sprimesynsetid);
			ISynset sprime = dict.getSynset(sid.get(ist));
			ihyposin.add(sprime.toString());

			////System.out.print("\nThe synset s is: ");
			//Displaying s, its polarity and its gloss
			boolean flag=false;
			INNERLOOP:
			for(IWord w : s.getWords())
			{
				////System.out.print("<"+w.getLemma()+"> ");
			}
			////System.out.println("Polarity : "+polarity);
			////System.out.println("\nGloss of s: "+s.getGloss());

			//Displaying s' and its gloss
			////System.out.print("\nThe synset s' is: ");
			for(IWord w : sprime.getWords())
			{
				////System.out.print("<"+w.getLemma()+"> ");
			}
			////System.out.print("- Polarity to be determined");
			////System.out.println("\nGloss of s': "+sprime.getGloss());
			////System.out.println("");

			String gloss=sprime.getGloss();
			////System.out.println("Gloss : "+gloss);
			gloss=gloss.replace("(","");
			gloss=gloss.replace(")","");
			//Extracting the definition(s) of s' from its gloss
			int sc=gloss.indexOf("\"");
			if(sc!=-1)
				gloss=gloss.substring(0,sc-2);
			//Splitting the definitions of s' if there are more than one
			String spgloss[]=gloss.split(";");
			Pattern pat1=Pattern.compile("\\W");
			int poswords=0, negwords=0, neuwords=0, ambwords=0, totk=0;
			//For each definition of s'
			for(int j=0;j<spgloss.length;j++)
			{
				List<RecordData> list=new ArrayList<RecordData>();
				Matcher mat1=pat1.matcher(spgloss[j]);
				mat1.replaceAll(" ");
				spgloss[j]=spgloss[j].replace("'s","");
				spgloss[j]=spgloss[j].replace("n't"," not");
				spgloss[j]=spgloss[j].trim();
				//System.out.println("spgloss[j] : "+spgloss[j]);
				//Tagging the definition of s'
				String taggedgloss = null;
				try {
					taggedgloss = tagger.tagTokenizedString(spgloss[j]);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println("taggedgloss : "+taggedgloss);
				//Calling a function to perform stop word check and polarity extraction on the tagged definition of s'
				List<RecordData> templist = null;
				try {
					templist = PolAnalysis(tagger, taggedgloss, posMap1);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				list.addAll(templist);

				/*If polarity of word is unknown, retrieve its wordnet senses
				 * and check polarities of all words in the definition of each sense
				 * having the same pos as the word*/
				int k=0;
				while(k<list.size()) {
					RecordData rd1=(RecordData) list.get(k);
					if (rd1.getOverPol().equalsIgnoreCase("unknown")) {
						String wnword=rd1.getWord();
						String wnpos=rd1.getPos();
						IIndexWord wnidxWord=null;
						if(wnpos.equalsIgnoreCase("adjective")) {
							wnidxWord = dict.getIndexWord(wnword,POS.ADJECTIVE);
							if(wnidxWord==null)
								wnidxWord = dict.getIndexWord(morp.stem(wnword),POS.ADJECTIVE);
						}
						else if (wnpos.equalsIgnoreCase("adverb")) {
							wnidxWord = dict.getIndexWord(wnword,POS.ADVERB);
							if(wnidxWord==null)
								wnidxWord = dict.getIndexWord(morp.stem(wnword),POS.ADVERB);
						}
						else if (wnpos.equalsIgnoreCase("noun")) {
							wnidxWord = dict.getIndexWord(wnword,POS.NOUN);
							if(wnidxWord==null)
								wnidxWord = dict.getIndexWord(morp.stem(wnword),POS.NOUN);
						}
						else if (wnpos.equalsIgnoreCase("verb")) {
							wnidxWord = dict.getIndexWord(wnword,POS.VERB);
							if(wnidxWord==null)
								wnidxWord = dict.getIndexWord(morp.stem(wnword),POS.VERB);
						}
						if(wnidxWord==null) {}
						else {
							List<IWordID> wnwordID = wnidxWord.getWordIDs();
							Iterator<IWordID> wni = wnwordID.iterator();
							int wnk=0;
							List<RecordData> wnlist=new ArrayList<RecordData>();
							while(wni.hasNext() && wnk<wnwordID.size()) {
								IWordID wordID11 = wnidxWord.getWordIDs().get(wnk);
								ISynsetID is=wordID11.getSynsetID();
								ISynset synset = dict.getSynset(is);
								String wngloss=synset.getGloss();
								wngloss=wngloss.replace("(","");
								wngloss=wngloss.replace(")","");
								int wnc=wngloss.indexOf("\"");
								if(wnc!=-1)
									wngloss=wngloss.substring(0,wnc-2);
								String spwngloss[]=wngloss.split(";");
								//System.out.println("spwngloss.length : "+spwngloss.length);
								for(int j1=0;j1<spwngloss.length;j1++) {
									Matcher mat2=pat1.matcher(spwngloss[j1]);
									//System.out.println("Matcher mat2 : "+mat2.toString());
									mat2.replaceAll(" ");
									spwngloss[j1]=spwngloss[j1].replace("'s","");
									spwngloss[j1]=spwngloss[j1].replace("n't"," not");
									spwngloss[j1]=spwngloss[j1].trim();
									String taggedwngloss;
									try {
										taggedwngloss = MaxentTagger.tagTokenizedString(spwngloss[j1]);
										List<RecordData> wntemplist=PolAnalysis(tagger, taggedwngloss, posMap1);
										//System.out.println("wntemplist.size() : "+wntemplist.size());
										wnlist.addAll(wntemplist);
										for(int i=0;i<wnlist.size();i++)
										{
											////System.out.println(wnlist.get(i).rdword.trim() + "   " +wnlist.get(i).rdwnoverpol);
										}
											wntemplist.clear();
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									//System.out.println("taggedwngloss : "+taggedwngloss);

									/*for(int y=0;y<wnlist.size();y++)
									{
										//System.out.println("wnlist["+y+"] : "+wnlist.get(y).getOverPol());
									}*/

								}
								wnk++;
							}
							//Count number of positive and negative words in the senses
							int posflag=0, negflag=0;
							for(int j1=0;j1<wnlist.size();j1++) {
								RecordData rd2=(RecordData) wnlist.get(j1);
								if(rd2.getPos().equalsIgnoreCase(rd1.getPos())) {
								if(rd2.getOverPol().equalsIgnoreCase("positive")) {
									//System.out.println(rd2.getWord()+" - positive");
									posflag++;
								}
								else if(rd2.getOverPol().equalsIgnoreCase("negative")) {
									//System.out.println(rd2.getWord()+" - negative");
									negflag++;
								}

								}
							}
							rd1.setPosFlag(posflag);
							rd1.setNegFlag(negflag);
							if(rd1.getPosFlag()>0 || rd1.getNegFlag()>0) {
								//System.out.println("In wordnet senses of '"+rd1.getWord()+"' the following words have specified polarities");
								for(int j1=0;j1<wnlist.size();j1++) {
									RecordData rd2=(RecordData) wnlist.get(j1);
									if(rd2.getPos().equalsIgnoreCase(rd1.getPos())) {
										if(rd2.getOverPol().equalsIgnoreCase("positive"))
										{
											////System.out.println(rd2.getWord()+" - positive");
										}
										else if(rd2.getOverPol().equalsIgnoreCase("negative"))
										{
											////System.out.println(rd2.getWord()+" - negative");
										}
									}
								}
							}
						}
					}
					k++;
				}
				//If any term is a negation term, flip polarities of all words after it
				k=0;
				while(k<list.size()) {
					RecordData rd1=(RecordData) list.get(k);
						if(rd1.getPos().equalsIgnoreCase("negation term")) {
							int k1=k+1;
							while((k1)<list.size()) {
								RecordData rd11=(RecordData) list.get(k1);
								if(rd11.getWNOverPol().equalsIgnoreCase("positive")) {rd11.setWNOverPol("negative");}
								else if(rd11.getWNOverPol().equalsIgnoreCase("negative")) {rd11.setWNOverPol("positive");}
								k1++;
							}
						}
						//Check for <Adjective,Noun> pairs and check if they meet the special rules
						if(rd1.getPos().equalsIgnoreCase("adjective")) {
							if((k+1)<list.size()) {
							RecordData rd11=(RecordData) list.get(k+1);
							if(rd11.getPos().equalsIgnoreCase("noun")) {
								if(rd1.getOverPol().equalsIgnoreCase("positive") && rd11.getOverPol().equalsIgnoreCase("negative")) {
								String temp="<"+rd1.getWord()+"> <"+rd11.getWord()+">";
								RecordData rd2=new RecordData(temp);
								rd2.setPos("<adjective> <noun>");
								rd2.setPolOP("negative");
								rd2.setPolGI("negative");
								list.remove(rd1);
								list.remove(rd11);
								list.add(k,rd2);
								}
								else if(rd1.getOverPol().equalsIgnoreCase("negative") && rd11.getOverPol().equalsIgnoreCase("positive")) {
									String temp="<"+rd1.getWord()+"> <"+rd11.getWord()+">";
									RecordData rd2=new RecordData(temp);
									rd2.setPos("<adjective> <noun>");
									rd2.setPolOP("negative");
									rd2.setPolGI("negative");
									list.remove(rd1);
									list.remove(rd11);
									list.add(k,rd2);
								}
							}
							}
						}
						//Check for <Adverb,Adjective> pairs and check if they meet the special rules
						if(rd1.getPos().equalsIgnoreCase("adverb")) {
							if((k+1)<list.size()) {
								RecordData rd11=(RecordData) list.get(k+1);
								if(rd11.getPos().equalsIgnoreCase("adjective")) {
									if(rd1.getOverPol().equalsIgnoreCase("positive") && rd11.getOverPol().equalsIgnoreCase("negative")) {
										String temp="<"+rd1.getWord()+"> <"+rd11.getWord()+">";
										RecordData rd2=new RecordData(temp);
										rd2.setPos("<adverb> <adjective>");
										rd2.setPolOP("negative");
										rd2.setPolGI("negative");
										list.remove(rd1);
										list.remove(rd11);
										list.add(k,rd2);
									}
									else if(rd1.getOverPol().equalsIgnoreCase("negative") && rd11.getOverPol().equalsIgnoreCase("positive")) {
										String temp="<"+rd1.getWord()+"> <"+rd11.getWord()+">";
										RecordData rd2=new RecordData(temp);
										rd2.setPos("<adverb> <adjective>");
										rd2.setPolOP("negative");
										rd2.setPolGI("negative");
										list.remove(rd1);
										list.remove(rd11);
										list.add(k,rd2);
									}
								}
							}
						}
					k++;
				}
				//Display the word and all its details
				k=0;
				////System.out.println("\n\nList of words in definition "+(j+1)+" of s' - RULE 1");
				while(k<list.size()) {
					RecordData rd1=(RecordData) list.get(k);
					////System.out.print("\n"+(k+1)+": ");
					////System.out.print("Word: "+rd1.getWord()+" - ");
					////System.out.print("POS: "+rd1.getPos()+" - ");
					////System.out.print("Polarity in GI: "+rd1.getPolGI()+" - ");
					////System.out.print("Polarity in OP: "+rd1.getPolOP()+" - ");
					////System.out.print("Polarity in AL: "+rd1.getPolAL()+" - ");
					////System.out.print("Overall Polarity: "+rd1.getOverPol());
					if(rd1.getOverPol().equalsIgnoreCase("unknown")) {
						////System.out.println("\nPositive content words in wordnet senses: "+rd1.getPosFlag());
						////System.out.print("Negative content words in wordnet senses: "+rd1.getNegFlag());
						////System.out.print("\nOverall polarity based on wordnet senses: "+rd1.getWNOverPol());
					}
					if(rd1.getOverPol().equalsIgnoreCase("positive"))
						poswords++;
					else if(rd1.getOverPol().equalsIgnoreCase("negative"))
						negwords++;
					else if(rd1.getOverPol().equalsIgnoreCase("unknown")) {
						if(rd1.getWNOverPol().equalsIgnoreCase("positive"))
							poswords++;
						else if(rd1.getWNOverPol().equalsIgnoreCase("negative"))
							negwords++;
						else if(rd1.getWNOverPol().equalsIgnoreCase("neutral"))
							neuwords++;
						else if(rd1.getWNOverPol().equalsIgnoreCase("ambiguous"))
							ambwords++;
					}
					else if(rd1.getOverPol().equalsIgnoreCase("neutral"))
						neuwords++;
					else if(rd1.getOverPol().equalsIgnoreCase("ambiguous"))
						ambwords++;
					k++;
				}
				totk=totk+k;
				//con.close();
				totk=totk-neuwords;
				//Check if hypothesis of Rule 1 is satisfied or not
				////System.out.println("\n\nAnalysis of Hypothesis of Rule 1:");
				////System.out.println("Positive Words: "+poswords+" - Negative Words: "+negwords);
				////System.out.println("Neutral Words: "+neuwords+" - Ambiguous polarity Words: "+ambwords);

				//*************///


				if(poswords>=1 && negwords==0 && polarity.equalsIgnoreCase("positive")&&flag==false)
				{
					flag=true;
					//System.out.println("1");
					pwprint(position, pw, s,sprime, polarity,"POSITIVE", "Rule 1");
					//pw.write(synsetID+":"+s.getWords()+":"+polarity+":"+sprime.getID()+":"+sprime.getWords()+":POSITIVE");
					pw.newLine();

				}
				else if(poswords==0 && negwords>=1 && polarity.equalsIgnoreCase("negative")&&flag==false)
				{
					flag=true;
					pwprint(position, pw, s,sprime, polarity,"NEGATIVE", "Rule 1");
					pw.newLine();
					//System.out.println("2");

				}

				else if(ambwords>0 && flag==false)
				{
					if(!polarity.equalsIgnoreCase("NEUTRAL"))

					{
						//System.out.println("3");
						rule2(position, synsetID,sprime, 3, polarity,s,pw,wordbw,countoflines,tagger,dict,posMap,posMap1);
					}
				}
				else if(poswords>0 && negwords>0 && flag==false)
				{
					if(!polarity.equalsIgnoreCase("NEUTRAL"))
					{
						//System.out.println("4");
						rule2(position, synsetID,sprime, 3, polarity,s,pw,wordbw,countoflines,tagger,dict,posMap,posMap1);
					}
				}

				else if (flag==false)
				{
					if(!polarity.equalsIgnoreCase("NEUTRAL"))
					{
						//System.out.println("5");
						rule2(position, synsetID,sprime, 3, polarity,s,pw,wordbw,countoflines,tagger,dict,posMap,posMap1);
					}
				}

				//*********************************/////

			}

		}// end of for each hyponym
		return ihyposin;
	}







	private void pwprint(int position, BufferedWriter pw, ISynset s, ISynset sprime,
			String polarity, String pol, String rule) throws IOException {

		int sSize=s.getWords().size(); //getwords size
//		pw.write("{");
		//if (sSize==1){
			pw.write(s.getWord(1).getLemma());
		/*}

		if (sSize>1){
			for(int i=1;i<sSize;i++){
				//System.out.println(s.getWords().size());
				pw.write(s.getWord(i).getLemma()+",");
			}
			pw.write(s.getWord(sSize).getLemma());
		}
		*/
		pw.write(","+position+","+inputPos.toString()+","+polarity);
		/*pw.write("\t"+sprime.getID()+"\t");
		int sprimeSize=sprime.getWords().size();
		pw.write("{");
		if (sprimeSize==1){
			pw.write(sprime.getWord(1).getLemma());
		}
		if (sprimeSize>1){
			for(int i=1;i<sprimeSize;i++){
				//System.out.println(s.getWords().size());
				pw.write(sprime.getWord(i).getLemma()+", ");
			}
			pw.write(sprime.getWord(sprimeSize).getLemma()+"}");
		}

		pw.write("\t"+pol+"\t"+rule);
		*/
	}



	public void rule2(int position, ISynsetID synsetID, ISynset sprime, int pos,String polarity,ISynset s, BufferedWriter pw, BufferedWriter wordbw,int countoflines,MaxentTagger tagger, IDictionary dict, HashMap<Integer, String> posMap, HashMap<String, String> posMap1){
		try {
			boolean flag=false;
			RecordData rd;
			List<RecordData> list=new ArrayList<RecordData>();

			IIndexWord idxWord=null;

			//Initializing stemmer
			Morphology morp=new Morphology();

			////System.out.println("Rule 2... entry point....");
				//Displaying s, its polarity and its gloss
				////System.out.print("\nThe synset s is: ");
				for(IWord w : s.getWords())
				{
					////System.out.print("<"+w.getLemma()+"> ");
				}

				////System.out.print("- Polarity: "+polarity);
				////System.out.println("\nGloss of s: "+s.getGloss());
				//Displaying s' and its gloss
				//System.out.print("\nThe synset s' is: ");
				/*for(IWord w : sprime.getWords())
				{
					System.out.print("<"+w.getLemma()+"> ");
				}*/
				//System.out.print("- Polarity to be determined");
				//System.out.println("\nGloss of s': "+sprime.getGloss());
				//Initializing connection to the SQl server database
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				Connection con = DriverManager.getConnection(
						"jdbc:sqlserver://localhost;databaseName=wordnet;integratedSecurity=true");

				//System.out.println("\n\nDb connected.. \n\n");
				for(IWord w : sprime.getWords()) {

					//PreparedStatement stmt2=con.prepareStatement("select DISTINCT word from dbo.stopword1 where word=?");
					PreparedStatement stmt2=con.prepareStatement("select DISTINCT word from dbo.StopWords where word=?");
					String sword=w.getLemma().replace("'s","");

					stmt2.setString(1,sword);
					ResultSet res2=stmt2.executeQuery();

					//Checking if the word is a stop word
					if(res2.next()==true) {

						while(res2.next()==true)
						{}
						res2.close();
						stmt2.close();
					}
					else {
						//System.out.println("Not a Stop word");
						res2.close();
						stmt2.close();
						//Getting polarities of the word from GI, AL and OF dictionaries
						rd=new RecordData(sword);
						PreparedStatement stmt=con.prepareStatement("select DISTINCT polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
						PreparedStatement stmt1=con.prepareStatement("select DISTINCT Polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
						PreparedStatement stmt_applex=con.prepareStatement("select DISTINCT Polarity from dbo.AppLexiconClean where Word=? and (Pos=? or Pos=?)");
						stmt.setString(1,sword);
						stmt_applex.setString(1,sword);
						stmt1.setString(1,sword);
						stmt.setString(2,posMap.get(pos));
						stmt_applex.setString(2,posMap.get(pos));

						stmt1.setString(2,posMap.get(pos));
						if(posMap.get(pos).equals("adjective") || posMap.get(pos).equals("adverb")) {
							stmt.setString(3,"anypos");
							stmt1.setString(3,"anypos");
							stmt_applex.setString(3,"anypos");
						}
						else {
							stmt.setString(3,"anypos");
							stmt1.setString(3,"");
							stmt_applex.setString(3,"");
						}
						rd.setPos(posMap.get(pos));
						ResultSet res=stmt.executeQuery();
						if (res.next())
							rd.setPolOP(res.getString("polarity"));
						else
							rd.setPolOP("unknown");
						res.close();
						stmt.close();
						ResultSet res1=stmt1.executeQuery();
						if (res1.next())
							rd.setPolGI(res1.getString("Polarity"));
						else
							rd.setPolGI("unknown");
						res1.close();
						stmt1.close();

						ResultSet tem1_applex=stmt_applex.executeQuery();
						if(tem1_applex.next())
							rd.setPolAL(tem1_applex.getString("Polarity"));
						else
							rd.setPolAL("unknown");
						tem1_applex.close();
						stmt_applex.close();
						list.add(rd);


						//If the word is an n-gram of unknown polarity then split it
						//and get polarities from GI, AL and OF dictionaries for each split
						Pattern pat=Pattern.compile("_");
						if((rd.getWord().indexOf("_")!=-1) || (rd.getWord().indexOf("-")!=-1)) {
							String tword=rd.getWord();
							list.remove(rd);
							Matcher m=pat.matcher(tword);
							tword=m.replaceAll(" ");
							tword=tword.replace("-"," ");
							tword=tword.replace("'s","");
							//Tagging the n-gram word using POS tagger
							String taggedtword = tagger.tagTokenizedString(tword);
							String temp1[]=taggedtword.split(" ");
							for(int j=0;j<temp1.length;j++) {
								String temp2[]=temp1[j].split("_");
								PreparedStatement stmt3=con.prepareStatement("select DISTINCT word from dbo.StopWords where word=?");
								//PreparedStatement stmt3=con.prepareStatement("select DISTINCT word from dbo.stopword1 where word=?");
								stmt3.setString(1,temp2[0]);
								ResultSet res3=stmt3.executeQuery();
								if(res3.next()==true) {
									while(res3.next()==true)
									{}
									res3.close();
									stmt3.close();
								}
								else {
									res3.close();
									stmt3.close();
									RecordData temp=new RecordData(temp2[0]);
									temp.setPos(posMap1.get(temp2[1]));
									stmt=con.prepareStatement("select DISTINCT polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
									stmt1=con.prepareStatement("select DISTINCT Polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
									stmt_applex=con.prepareStatement("select DISTINCT Polarity from dbo.AppLexiconClean where Word=? and (Pos=? or Pos=?)");
									stmt.setString(1,temp2[0]);
									stmt_applex.setString(1,temp2[0]);
									stmt.setString(2,posMap1.get(temp2[1]));
									stmt_applex.setString(2,posMap1.get(temp2[1]));
									if(posMap1.get(temp2[1]).equals("adjective") || posMap1.get(temp2[1]).equals("adverb")) {
										stmt.setString(3,"anypos");
										stmt1.setString(3,"anypos");
										stmt_applex.setString(3,"anypos");
									}
									else {
										stmt.setString(3,"anypos");
										stmt1.setString(3,"");
										stmt_applex.setString(3,"");
									}
									ResultSet tem=stmt.executeQuery();
									if(tem.next())
										temp.setPolOP(tem.getString("polarity"));
									else
										temp.setPolOP("unknown");
									tem.close();
									stmt.close();
									stmt1.setString(1,temp2[0]);
									stmt1.setString(2,posMap1.get(temp2[1]));
									ResultSet tem1=stmt1.executeQuery();
									if(tem1.next())
										temp.setPolGI(tem1.getString("Polarity"));
									else
										temp.setPolGI("unknown");
									tem1.close();
									stmt1.close();
									ResultSet pres1=stmt_applex.executeQuery();
									if (pres1.next())
										temp.setPolGI(pres1.getString("Polarity"));
									else
										temp.setPolGI("unknown");
									pres1.close();
									stmt_applex.close();
									list.add(temp);
								}
							}
						}
					}
				}
				int k=0;
				while(k<list.size()) {
					RecordData rd1=(RecordData) list.get(k);
					PreparedStatement stmt2=con.prepareStatement("select DISTINCT word from dbo.StopWords where word=?");
					//PreparedStatement stmt2=con.prepareStatement("select DISTINCT word from dbo.stopword1 where word=?");
					//Stem the word and check if it is stop word; if so eliminate it
					stmt2.setString(1,morp.stem(rd1.getWord()));
					ResultSet res2=stmt2.executeQuery();
					if(res2.next()) {
						while(res2.next()) {}
						list.remove(rd1);
						if(k<list.size())
						rd1=(RecordData) list.get(k);
						else
							rd1=new RecordData();
						res2.close();
						stmt2.close();
					}
					res2.close();
					stmt2.close();
					//If polarity is unknown, Stem the word find its polarity in the OF dictionary
					if(rd1.getPolOP().equals("unknown")) {
						String sword=morp.stem(rd1.getWord());
						PreparedStatement stmt=con.prepareStatement("select DISTINCT polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
						stmt.setString(1,sword);
						stmt.setString(2,rd1.getPos());
						if(rd1.getPos().equals("adjective") || rd1.getPos().equals("adverb"))
							stmt.setString(3,"anypos");
						else
							stmt.setString(3,"anypos");
						ResultSet res=stmt.executeQuery();
						if (res.next())
							rd1.setPolOP(res.getString("Polarity"));
						else
							rd1.setPolOP("unknown");
						res.close();
						stmt.close();
					}
					//If polarity is unknown, Stem the word find its polarity in the GI dictionary
					if(rd1.getPolGI().equals("unknown")) {
						String sword=morp.stem(rd1.getWord());
						PreparedStatement stmt1=con.prepareStatement("select DISTINCT polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
						stmt1.setString(1,sword);
						stmt1.setString(2,rd1.getPos());
						if(rd1.getPos().equals("adjective") || rd1.getPos().equals("adverb"))
							stmt1.setString(3,"anypos");
						else
							stmt1.setString(3,"");
						ResultSet res=stmt1.executeQuery();
						if (res.next())
							rd1.setPolGI(res.getString("Polarity"));
						else
							rd1.setPolGI("unknown");
						res.close();
						stmt1.close();
					}
					//If polarity is unknown, Stem the word find its polarity in the APPLEX dictionary
					if(rd1.getPolGI().equalsIgnoreCase("unknown")) {
						String sword=morp.stem(rd1.getWord());
						PreparedStatement stmt1=con.prepareStatement("select Polarity from dbo.AppLexiconClean where Word=? and (Pos=? or Pos=?)");
						stmt1.setString(1,sword);
						stmt1.setString(2,rd1.getPos());
						if(rd1.getPos().equalsIgnoreCase("adjective") || rd1.getPos().equalsIgnoreCase("adverb"))
							stmt1.setString(3,"anypos");
						else
							stmt1.setString(3,"");
						ResultSet res=stmt1.executeQuery();
						if (res.next())
							rd1.setPolAL(res.getString("Polarity"));
						else
							rd1.setPolAL("unknown");
						res.close();
						stmt1.close();
					}
					k++;
				}
				con.close();
				//Displaying the word and its details
				int poswords=0, negwords=0, neuwords=0, unkwords=0, ambwords=0;
				k=0;

				////System.out.println("\nList of words in s' - RULE 2");
				while(k<list.size()) {
					RecordData rd1=(RecordData) list.get(k);
					////System.out.print("\n"+(k+1)+": ");
					////System.out.print("Word: "+rd1.getWord()+" - ");
					////System.out.print("POS: "+rd1.getPos()+" - ");
					////System.out.print("Polarity in GI: "+rd1.getPolGI()+" - ");
					////System.out.print("Polarity in OF: "+rd1.getPolOP()+" - ");
					////System.out.print("Polarity in AL: "+rd1.getPolAL()+" - ");
					////System.out.print("Overall Polarity: "+rd1.getOverPol());

					if(rd1.getOverPol().equalsIgnoreCase("positive"))
						poswords++;
					else if(rd1.getOverPol().equalsIgnoreCase("negative"))
						negwords++;
					else if(rd1.getOverPol().equalsIgnoreCase("unknown"))
						unkwords++;
					else if(rd1.getOverPol().equalsIgnoreCase("neutral"))
						neuwords++;
					else if(rd1.getOverPol().equalsIgnoreCase("ambiguous"))
						ambwords++;
					k++;
				}
				k=k-neuwords;
				//Check whether the hypothesis of Rule 2 is satisfied or not
				////System.out.println("\n\nAnalysis of Hypothesis of Rule 2:");
				////System.out.println("Positive Words: "+poswords+" - Negative Words: "+negwords);
				////System.out.println("Neutral Words: "+neuwords+" - Unknown polarity Words: "+unkwords+" - Ambiguous polarity Words: "+ambwords);

				////////////**************************************/////////////

				if(flag==false&&poswords>0 && negwords==0 && poswords>(0.5*k) && (polarity.equalsIgnoreCase("positive") || polarity.equalsIgnoreCase("p")))
				{
					flag=true;
					pwprint(position, pw, s,sprime, polarity,"POSITIVE", "Rule 2");
					//System.out.println("a");
					pw.newLine();
					return;

				}
				else if(flag==false&&poswords==0 && negwords>0 && negwords>(0.5*k) && (polarity.equalsIgnoreCase("negative") || polarity.equalsIgnoreCase("n")))
				{
					flag=true;
					pwprint(position, pw, s,sprime, polarity,"NEGATIVE", "Rule 2");
					pw.newLine();
					//System.out.println("b");
				}
				else if(flag==false&&ambwords>0)
				{

					SimilarToRule3 t3=new SimilarToRule3(pathforwnhome, position, inputPos);
					if(!polarity.equalsIgnoreCase("NEUTRAL"))
					{
						//System.out.println("c");
						t3.rule3(synsetID,sprime, 3, polarity, s, pw,wordbw,countoflines,tagger,dict,posMap,posMap1);
					}
				}
				else if(flag==false&&poswords>0 && negwords>0)
				{

					SimilarToRule3 t3=new SimilarToRule3(pathforwnhome, position, inputPos);
					if(!polarity.equalsIgnoreCase("NEUTRAL"))
					{
						//System.out.println("d");
						t3.rule3(synsetID,sprime, 3, polarity, s, pw,wordbw,countoflines,tagger,dict,posMap,posMap1);
					}


				}

				else if (flag==false)
				{
					//////ADDED NOV2012
					SimilarToRule3 t3=new SimilarToRule3(pathforwnhome, position, inputPos);

					if(!polarity.equalsIgnoreCase("NEUTRAL"))
					{
						//System.out.println("e");
						t3.rule3(synsetID,sprime, 3, polarity, s, pw,wordbw,countoflines,tagger,dict,posMap,posMap1);
					}
				}

				////////////****************************////////////////////////////
			}

		catch(IOException e) {
			System.out.println("IO exception");
			System.out.println("line no : "+countoflines);
			System.out.println("Rule 2");
		}
		catch(Exception e) {
			System.out.println(e);
			System.out.println("line no : "+countoflines);
			System.out.println("Rule 2");
			System.out.println(e.getMessage());
		}
	}



	private ArrayList<String> NeutralCase(int position, ISynsetID synsetID, String stra2, MaxentTagger tagger, IDictionary dict, HashMap<Integer, String> posMap, HashMap<String, String> posMap1, BufferedWriter pw, BufferedWriter wordbw) {
		// TODO Auto-generated method stub

		ArrayList<String> ihyposin=new ArrayList<String>();
		ISynset s = dict.getSynset(synsetID);
		List<ISynsetID> sid=s.getRelatedSynsets(Pointer.SIMILAR_TO);
		//Displaying the hyponyms of s
		////System.out.println("s.getRelatedSynsets(Pointer.SIMILAR_TO)    "+sid.toString() +" \t size : "+sid.size());

		List<IWord> words1;	int k=0;
		for(ISynsetID sid1 : sid){
			ISynset synset = dict.getSynset(sid1);
			words1 = dict.getSynset(sid1).getWords();
			for(IWord w : synset.getWords()) {
				////System.out.print("<"+w.getLemma()+"> ");
			}
			////System.out.print("- SIMILARTO ID "+(k+1));
			////System.out.println("");
			k=k+1;
		}
		for(int ist=0;ist<sid.size();ist++)
		{
			String sprimesynsetidStr = sid.get(ist).toString();
			int sprimesynsetid;
			String[] split = sprimesynsetidStr.split("-");
			sprimesynsetid=Integer.parseInt(split[1]);
			ISynset sprime = dict.getSynset(sid.get(ist));
			ihyposin.add(sprime.toString());
			////System.out.print("\nThe synset s is: ");
			//Displaying s, its polarity and its gloss
			for(IWord w : s.getWords())
			{
				////System.out.print("<"+w.getLemma()+"> ");
			}
			////System.out.println("Polarity : "+stra2);
			////System.out.println("\nGloss of s: "+s.getGloss());

			//Displaying s' and its gloss
			////System.out.print("\nThe synset s' is: ");
			for(IWord w : sprime.getWords())
			{
				////System.out.print("<"+w.getLemma()+"> ");
			}
			////System.out.print("- Polarity to be determined");
			////System.out.println("\nGloss of s': "+sprime.getGloss());
			////System.out.println("");

			String gloss=sprime.getGloss();
			////System.out.println("Gloss : "+gloss);
			gloss=gloss.replace("(","");
			gloss=gloss.replace(")","");
			//Extracting the definition(s) of s' from its gloss
			int sc=gloss.indexOf("\"");
			if(sc!=-1)
				gloss=gloss.substring(0,sc-2);
			//Splitting the definitions of s' if there are more than one
			String spgloss[]=gloss.split(";");
			Pattern pat1=Pattern.compile("\\W");
			int poswords=0, negwords=0, neuwords=0, ambwords=0, totk=0;
			//For each definition of s'

			for(int j=0;j<spgloss.length;j++)
			{
				List<RecordData> list=new ArrayList<RecordData>();
				Matcher mat1=pat1.matcher(spgloss[j]);
				mat1.replaceAll(" ");
				spgloss[j]=spgloss[j].replace("'s","");
				spgloss[j]=spgloss[j].replace("n't"," not");
				spgloss[j]=spgloss[j].trim();
				//System.out.println("spgloss[j] : "+spgloss[j]);
				//Tagging the definition of s'
				String taggedgloss;
				try {
					taggedgloss = tagger.tagTokenizedString(spgloss[j]);
					List<RecordData> templist=PolAnalysis(tagger, taggedgloss, posMap1);
					list.addAll(templist);
					poswords=0;
					negwords=0;
					k=0;
					while(k<list.size()) {
						RecordData rd1=(RecordData) list.get(k);
						if(rd1.getOverPol().equalsIgnoreCase("positive"))
							poswords++;
						else if(rd1.getOverPol().equalsIgnoreCase("negative"))
							negwords++;
						k++;
					}
					if(poswords==0 && negwords==0)
					{
						////System.out.println("Definition of s' has words of neutral polarity. Hence, hypothesis of Rule 1 is satisfied.");
						pwprint(position, pw, s,sprime, "NEUTRAL","NEUTRAL", "Rule 1");


						//pw.write(synsetID+":"+s.getWords()+":NEUTRAL:"+sprime.getID()+":"+sprime.getWords()+":NEUTRAL");
						pw.newLine();
						List<IWord> l=sprime.getWords();
						//wordbw.write(sprime.toString()+"\t:NEGATIVE");
						//wordbw.newLine();
						if(l.size()>1)
						{
							for(int jq=0;jq<l.size();jq++)
							{
								//findpolarity(l.get(jq).getLemma(),wordbw,sprime.getPOS().toString());
								wordbw.write(l.get(jq).getLemma().toUpperCase()+":NEUTRAL");
								wordbw.newLine();
							}
							//wordbw.newLine();
						}
						if(l.size()==1)
						{
							wordbw.write(l.get(0).getLemma().toUpperCase()+":NEUTRAL");
							wordbw.newLine();
						}
					}
					else
					{
						////System.out.println("Rule 1 is not satisfied for Synset polarity Neutral");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//System.out.println("taggedgloss : "+taggedgloss);
				//Calling a function to perform stop word check and polarity extraction on the tagged definition of s'

			}
		}
		return ihyposin;
	}



	private void findpolarity(String lemma, BufferedWriter wordbw, String pos) {
		// TODO Auto-generated method stub
		RecordData temp=new RecordData(lemma);
		temp.setPos(pos);
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con = DriverManager.getConnection(
					"jdbc:sqlserver://localhost;databaseName=wordnet;integratedSecurity=true");

			PreparedStatement stmt=con.prepareStatement("select DISTINCT polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
			PreparedStatement stmt1=con.prepareStatement("select DISTINCT Polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
			PreparedStatement stmt_applex=con.prepareStatement("select DISTINCT Polarity from dbo.AppLexiconClean where Word=? and (Pos=? or Pos=?)");
			stmt.setString(1,lemma);
			stmt_applex.setString(1,lemma);
			stmt.setString(2,pos);
			stmt_applex.setString(2,pos);
			if(pos.equals("adjective") || pos.equals("adverb")) {
				stmt.setString(3,"anypos");
				stmt1.setString(3,"modif");
				stmt_applex.setString(3,"modif");
			}
			else {
				stmt.setString(3,"anypos");
				stmt1.setString(3,"");
				stmt_applex.setString(3,"");
			}
			ResultSet tem=stmt.executeQuery();
			if(tem.next())
				temp.setPolOP(tem.getString("polarity"));
			else
				temp.setPolOP("unknown");
			tem.close();
			stmt.close();
			ResultSet tem1_applex=stmt_applex.executeQuery();
			if(tem1_applex.next())
				temp.setPolAL(tem1_applex.getString("Polarity"));
			else
				temp.setPolAL("unknown");
			tem1_applex.close();
			stmt_applex.close();
			Morphology morp=new Morphology();
			if(temp.getPolOP().equalsIgnoreCase("unknown")) {
				String sword=morp.stem(temp.getWord());
				PreparedStatement pstmt=con.prepareStatement("select polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
				pstmt.setString(1,sword);
				pstmt.setString(2,temp.getPos());
				if(temp.getPos().equalsIgnoreCase("adjective") || temp.getPos().equalsIgnoreCase("adverb"))
					pstmt.setString(3,"anypos");
				else
					pstmt.setString(3,"anypos");
				ResultSet pres=pstmt.executeQuery();
				if (pres.next())
					temp.setPolOP(pres.getString("polarity"));
				else
					temp.setPolOP("unknown");
				pres.close();
				pstmt.close();
			}
			stmt1.setString(1,lemma);
			stmt1.setString(2,pos);
			ResultSet tem1=stmt1.executeQuery();
			if(tem1.next())
				temp.setPolGI(tem1.getString("Polarity"));
			else
				temp.setPolGI("unknown");
			tem1.close();
			stmt1.close();
			if(temp.getPolGI().equalsIgnoreCase("unknown")) {
				String sword=morp.stem(temp.getWord());
				PreparedStatement pstmt1=con.prepareStatement("select Polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
				pstmt1.setString(1,sword);
				pstmt1.setString(2,temp.getPos());
				if(temp.getPos().equalsIgnoreCase("adjective") || temp.getPos().equalsIgnoreCase("adverb"))
					pstmt1.setString(3,"modif");
				else
					pstmt1.setString(3,"");
				ResultSet pres1=pstmt1.executeQuery();
				if (pres1.next())
					temp.setPolGI(pres1.getString("Polarity"));
				else
					temp.setPolGI("unknown");
				pres1.close();
				pstmt1.close();
			}
			//If polarity is unknown, Stem the word find its polarity in the APPLEX dictionary
			if(temp.getPolGI().equalsIgnoreCase("unknown")) {
				String sword=morp.stem(temp.getWord());
				stmt1=con.prepareStatement("select Polarity from dbo.AppLexiconClean where Word=? and (Pos=? or Pos=?)");
				stmt1.setString(1,sword);
				stmt1.setString(2,temp.getPos());
				if(temp.getPos().equalsIgnoreCase("adjective") || temp.getPos().equalsIgnoreCase("adverb"))
					stmt1.setString(3,"modif");
				else
					stmt1.setString(3,"");
				ResultSet res=stmt1.executeQuery();
				if (res.next())
					temp.setPolAL(res.getString("Polarity"));
				else
					temp.setPolAL("unknown");
				res.close();
				stmt1.close();
			}
			if(temp.getOverPol().equalsIgnoreCase("unknown"))
			{
				if(!temp.getWNOverPol().isEmpty() && (temp.getWNOverPol().toUpperCase()!="AMBIGUOUS"))
				{
					wordbw.write(lemma.toUpperCase() + ":" +temp.getWNOverPol().toUpperCase());
					wordbw.newLine();
				}
			}
			else
			{
				wordbw.write(lemma.toUpperCase() + ":" +temp.getOverPol().toUpperCase());
				wordbw.newLine();
			}

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public List<RecordData> PolAnalysis(MaxentTagger tagger, String taggedString, HashMap<String,String> posMap1) throws Exception {
		List<RecordData> palist=new ArrayList<RecordData>();
		palist.clear();
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		Connection pacon = DriverManager.getConnection(
				"jdbc:sqlserver://localhost;databaseName=wordnet;integratedSecurity=true");
		Morphology morp=new Morphology();
//		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//		String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=wordnet;";
		String sptgloss[]=taggedString.split(" ");

		RecordData rd;
		for(int j1=0;j1<sptgloss.length;j1++) {
			//Extracting the parameters from the form "word_postag"
			String spsptgloss[]=sptgloss[j1].split("_");
			/*for(int g=0;g<spsptgloss.length;g++)
			{
				System.out.println("spsptgloss["+g+"] : "+spsptgloss[g]);
			}*/

			if(spsptgloss[0].startsWith("-") || spsptgloss[0].startsWith(".") || spsptgloss[0].startsWith(":")|| spsptgloss.length>2 || spsptgloss[0].startsWith("$") || spsptgloss[0].startsWith("?"))
			{

				if(spsptgloss.length>2)
				{
					////System.out.println("trueee.. :)");
					for(int i=0;i<spsptgloss.length;i++)
					{
						////System.out.println(spsptgloss[i]);
					}
				}
				continue;
			}
			PreparedStatement pastmt2=pacon.prepareStatement("select word from dbo.StopWords where word=?");
			//PreparedStatement pastmt2=pacon.prepareStatement("select word from dbo.stopword1 where word=?");
			pastmt2.setString(1,spsptgloss[0]);
			ResultSet res2=pastmt2.executeQuery();

			//Checking if the word is a stop word
			if(res2.next()==true) {
				//System.out.println("res2.next is true");
				res2.close();
				pastmt2.close();
			}
			else {
				res2.close();
				pastmt2.close();
				//Extracting the GI and OP dictionary polarities for the word
				PreparedStatement pastmt=pacon.prepareStatement("select polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
				PreparedStatement pastmt1=pacon.prepareStatement("select Polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
				PreparedStatement stmt_applex=pacon.prepareStatement("select Polarity from dbo.AppLexiconClean where Word=? and (Pos=? or Pos=?)");
				rd=new RecordData(spsptgloss[0]);
				pastmt.setString(1,spsptgloss[0]);
				pastmt1.setString(1,spsptgloss[0]);
				stmt_applex.setString(1,spsptgloss[0]);
				pastmt.setString(2,posMap1.get(spsptgloss[1]));
				pastmt1.setString(2,posMap1.get(spsptgloss[1]));
				stmt_applex.setString(2,posMap1.get(spsptgloss[1]));
				if(posMap1.get(spsptgloss[1]).equalsIgnoreCase("adjective") || posMap1.get(spsptgloss[1]).equalsIgnoreCase("adverb")) {
					pastmt.setString(3,"anypos");
					pastmt1.setString(3,"modif");
					stmt_applex.setString(3,"modif");
				}
				else {
					pastmt.setString(3,"anypos");
					pastmt1.setString(3,"");
					stmt_applex.setString(3,"");
				}
				rd.setPos(posMap1.get(spsptgloss[1]));
				ResultSet res=pastmt.executeQuery();
				if (res.next())
					rd.setPolOP(res.getString("polarity"));
				else
					rd.setPolOP("unknown");
				res.close();
				pastmt.close();
				ResultSet res1=pastmt1.executeQuery();
				if (res1.next())
					rd.setPolGI(res1.getString("Polarity"));
				else
					rd.setPolGI("unknown");
				res1.close();
				pastmt1.close();
				ResultSet res_applex=stmt_applex.executeQuery();
				if (res_applex.next())
					rd.setPolAL(res_applex.getString("Polarity"));
				else
					rd.setPolAL("unknown");
				res_applex.close();
				stmt_applex.close();
				palist.add(rd);

				//If the word is an n-gram of unknown polarity then split it
				//and get polarities from GI, AL and OP dictionaries for each split
				Pattern pat=Pattern.compile("_");
				if((rd.getWord().indexOf("_")!=-1) || (rd.getWord().indexOf("-")!=-1)) {
					String tword=rd.getWord();
					palist.remove(rd);
					Matcher m=pat.matcher(tword);
					tword=m.replaceAll(" ");
					tword=tword.replace("-"," ");
					String taggedtword = tagger.tagTokenizedString(tword);
					String temp1[]=taggedtword.split(" ");
					for(int j2=0;j2<temp1.length;j2++) {
						String temp2[]=temp1[j2].split("_");
						PreparedStatement pastmt3=pacon.prepareStatement("select word from dbo.StopWords where word=?");

						//PreparedStatement pastmt3=pacon.prepareStatement("select word from dbo.stopword1 where word=?");
						pastmt3.setString(1,temp2[0]);
						ResultSet res3=pastmt3.executeQuery();
						if(res3.next()==true) {
							res3.close();
							pastmt3.close();
						}
						else {
							res3.close();
							pastmt3.close();
							pastmt=pacon.prepareStatement("select polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
							pastmt1=pacon.prepareStatement("select Polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
							stmt_applex=pacon.prepareStatement("select Polarity from dbo.AppLexiconClean where Word=? and (Pos=? or Pos=?)");
							RecordData temp=new RecordData(temp2[0]);
							temp.setPos(posMap1.get(temp2[1]));
							pastmt.setString(1,temp2[0]);
							stmt_applex.setString(1,temp2[0]);
							pastmt.setString(2,posMap1.get(temp2[1]));
							stmt_applex.setString(2,posMap1.get(temp2[1]));
							if(posMap1.get(temp2[1]).equalsIgnoreCase("adjective") || posMap1.get(temp2[1]).equalsIgnoreCase("adverb")) {
								pastmt.setString(3,"anypos");
								pastmt1.setString(3,"modif");
								stmt_applex.setString(3,"modif");
							}
							else {
								pastmt.setString(3,"anypos");
								pastmt1.setString(3,"");
								stmt_applex.setString(3,"");
							}
							ResultSet tem=pastmt.executeQuery();
							if(tem.next())
								temp.setPolOP(tem.getString("polarity"));
							else
								temp.setPolOP("unknown");
							tem.close();
							pastmt.close();
							pastmt1.setString(1,temp2[0]);
							pastmt1.setString(2,posMap1.get(temp2[1]));
							ResultSet tem1=pastmt1.executeQuery();
							if(tem1.next())
								temp.setPolGI(tem1.getString("Polarity"));
							else
								temp.setPolGI("unknown");
							tem1.close();
							pastmt1.close();
							ResultSet tem1_applex=stmt_applex.executeQuery();
							if(tem1_applex.next())
								temp.setPolAL(tem1_applex.getString("Polarity"));
							else
								temp.setPolAL("unknown");
							tem1_applex.close();
							stmt_applex.close();
							palist.add(temp);
						}
					}
				}
			}
		}
		int k=0;
		while(k<palist.size()) {
			//Stem the word and check if it is stop word; if so eliminate it
			PreparedStatement pastmt2=pacon.prepareStatement("select word from dbo.StopWords where word=?");
			//PreparedStatement pastmt2=pacon.prepareStatement("select word from dbo.stopword1 where word=?");
			RecordData rd1=(RecordData) palist.get(k);
			pastmt2.setString(1,morp.stem(rd1.getWord()));
			ResultSet res2=pastmt2.executeQuery();
			if(res2.next()) {
				palist.remove(rd1);
				if(k<palist.size())
					rd1=(RecordData) palist.get(k);
				else
					rd1=new RecordData();
			}
			res2.close();
			pastmt2.close();
			//If polarity is unknown, Stem the word find its polarity in the OP dictionary
			if(rd1.getPolOP().equalsIgnoreCase("unknown")) {
				PreparedStatement pastmt=pacon.prepareStatement("select polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
				String sword=morp.stem(rd1.getWord());
				pastmt.setString(1,sword);
				pastmt.setString(2,rd1.getPos());
				pastmt.setString(3,"anypos");
				ResultSet res=pastmt.executeQuery();
				if (res.next())
					rd1.setPolOP(res.getString("polarity"));
				else
					rd1.setPolOP("unknown");
				res.close();
				pastmt.close();
			}
			//If polarity is unknown, Stem the word find its polarity in the GI dictionary
			if(rd1.getPolGI().equalsIgnoreCase("unknown")) {
				PreparedStatement pastmt1=pacon.prepareStatement("select Polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
				String sword=morp.stem(rd1.getWord());
				pastmt1.setString(1,sword);
				pastmt1.setString(2,rd1.getPos());
				if(rd1.getPos().equalsIgnoreCase("adjective") || rd1.getPos().equalsIgnoreCase("adverb"))
					pastmt1.setString(3,"modif");
				else
					pastmt1.setString(3,"");
				ResultSet res=pastmt1.executeQuery();
				if (res.next())
					rd1.setPolGI(res.getString("Polarity"));
				else
					rd1.setPolGI("unknown");
				res.close();
				pastmt1.close();
			}


			//If polarity is unknown, Stem the word find its polarity in the APPLEX dictionary
			if(rd1.getPolGI().equalsIgnoreCase("unknown")) {
				String sword=morp.stem(rd1.getWord());
				PreparedStatement stmt1=pacon.prepareStatement("select Polarity from dbo.AppLexiconClean where Word=? and (Pos=? or Pos=?)");
				stmt1.setString(1,sword);
				stmt1.setString(2,rd1.getPos());
				if(rd1.getPos().equalsIgnoreCase("adjective") || rd1.getPos().equalsIgnoreCase("adverb"))
					stmt1.setString(3,"modif");
				else
					stmt1.setString(3,"");
				ResultSet res=stmt1.executeQuery();
				if (res.next())
					rd1.setPolAL(res.getString("Polarity"));
				else
					rd1.setPolAL("unknown");
				res.close();
				stmt1.close();
			}
			k++;
		}
		//Check for negation terms and flip polarities accordingly
		k=0;
		String negterms[]={"lack","no","not","loss","neither"};
		while(k<palist.size()) {
			RecordData rd1=(RecordData) palist.get(k);
			for(String neg : negterms) {
				if(neg.equalsIgnoreCase(rd1.getWord())) {
					rd1.setPos("negation term");
					int k1=k+1;
					while((k1)<palist.size()) {
						RecordData rd11=(RecordData) palist.get(k1);
						if(rd11.getPolOP().equalsIgnoreCase("unknown")) {}
						else if(rd11.getPolOP().equalsIgnoreCase("positive")) {rd11.setPolOP("negative");}
						else if(rd11.getPolOP().equalsIgnoreCase("negative")) {rd11.setPolOP("positive");}
						if(rd11.getPolGI().equalsIgnoreCase("unknown")) {}
						else if(rd11.getPolGI().equalsIgnoreCase("positive")) {rd11.setPolGI("negative");}
						else if(rd11.getPolGI().equalsIgnoreCase("negative")) {rd11.setPolGI("positive");}
						if(rd11.getWNOverPol().equalsIgnoreCase("positive")) {rd11.setWNOverPol("negative");}
						else if(rd11.getWNOverPol().equalsIgnoreCase("negative")) {rd11.setWNOverPol("positive");}
						k1++;
					}
				}
			}
			k++;
		}
		pacon.close();
		return palist;
	}
}
