package similarto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.ISynsetID;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.item.Pointer;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

/*Class 'RecordData' which stores a word, its part-of-speech tag,
 * its polarity in General Inquirer (GI) dictionary,
 * its polarity in the OpinionFinder (OP) dictionary,
 * its overall polarity based on the GI and OP dictionaries*/
public class SimilarToRule2 {
	public class RecordData {
		String rdword;
		String rdpos;
		String rdpolOP;
		String rdpolGI;
		String rdpolAL;
		String rdoverpol;
		String rdwnoverpol;
		int rdposflag;
		int rdnegflag;
		RecordData() {
			this.rdword="";
			this.rdpos="";
			this.rdpolOP="";
			this.rdpolGI="";
			this.rdpolAL="";
			this.rdoverpol="";
			rdposflag=0;
			rdnegflag=0;
			this.rdwnoverpol="";
		}
		RecordData(String _rdword) {
			this.rdword=_rdword;
			this.rdpos="";
			this.rdpolOP="";
			this.rdpolGI="";
			this.rdpolAL="";
			this.rdoverpol="";
			rdposflag=0;
			rdnegflag=0;
			this.rdwnoverpol="";
		}
		void setWord(String _rdword) {
			this.rdword=_rdword;
		}
		void setPos(String _rdpos) {
			this.rdpos=_rdpos;
		}

		void setPosFlag(int _posflag) {
			this.rdposflag=_posflag;
			this.setWNOverPol();
		}

		void setNegFlag(int _negflag) {
			this.rdnegflag=_negflag;
			this.setWNOverPol();
		}

		void setWNOverPol(String _wnoverpol) {
			this.rdwnoverpol=_wnoverpol;
		}

		void setWNOverPol() {
			if(this.rdposflag>=1 && this.rdnegflag==0)
				this.rdwnoverpol="positive";
			else if (this.rdposflag==0 && this.rdnegflag>=1)
				this.rdwnoverpol="negative";
			else if (this.rdposflag==0 && this.rdnegflag==0)
				this.rdwnoverpol="neutral";
			else if (this.rdposflag>0 && this.rdnegflag>0)
				this.rdwnoverpol="ambiguous";
		}

		void setPolOP(String _rdpolOP) {
			this.rdpolOP=_rdpolOP;
			this.setOverPol();
		}
		void setPolGI(String _rdpolGI) {
			this.rdpolGI=_rdpolGI;
			this.setOverPol();
		}
		void setPolAL(String _rdpolAL) {
			this.rdpolAL=_rdpolAL;
			this.setOverPol();
		}
		/*This method decides the overall polarity of a word based on its polarities
		 * in the GI and OP dictionaries*/
		void setOverPol() {

			if(this.rdpolOP.equals("") || this.rdpolGI.equals("") || this.rdpolAL.equals("")) {
			}
			else {
				if(this.rdpolOP.equalsIgnoreCase(this.rdpolGI) && this.rdpolOP.equalsIgnoreCase(this.rdpolAL))
					this.rdoverpol=rdpolOP;
				else if(this.rdpolOP.equalsIgnoreCase("unknown"))
				{
					if(this.rdpolGI.equalsIgnoreCase("unknown") || this.rdpolGI.equalsIgnoreCase("neutral"))
					{
						this.rdoverpol=this.rdpolAL;
					}
					else if(this.rdpolGI.equalsIgnoreCase("positive"))
					{
						if(this.rdpolAL.equalsIgnoreCase("unknown") || this.rdpolAL.equalsIgnoreCase("positive") || this.rdpolAL.equalsIgnoreCase("neutral"))
						{
							this.rdoverpol=this.rdpolGI;
						}
						else if(this.rdpolAL.equalsIgnoreCase("negative"))
						{
							this.rdoverpol="ambiguous";
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("negative"))
					{
						if(this.rdpolAL.equalsIgnoreCase("unknown") || this.rdpolAL.equalsIgnoreCase("negative") || this.rdpolAL.equalsIgnoreCase("neutral"))
						{
							this.rdoverpol=this.rdpolGI;
						}
						else if(this.rdpolAL.equalsIgnoreCase("positive"))
						{
							this.rdoverpol="ambiguous";
						}
					}

				}
				else if(this.rdpolOP.equalsIgnoreCase("positive"))
				{
					if(this.rdpolGI.equalsIgnoreCase("unknown") || this.rdpolGI.equalsIgnoreCase("positive") || this.rdpolGI.equalsIgnoreCase("neutral"))
					{
						if(this.rdpolAL.equalsIgnoreCase("negative"))
						{
							this.rdoverpol="ambiguous";
						}
						else
						{
							this.rdoverpol=this.rdpolOP;
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("negative"))
					{
						this.rdoverpol="ambiguous";
					}
				}
				else if(this.rdpolOP.equalsIgnoreCase("negative"))
				{
					if(this.rdpolGI.equalsIgnoreCase("unknown") || this.rdpolGI.equalsIgnoreCase("negative") || this.rdpolGI.equalsIgnoreCase("neutral"))
					{
						if(this.rdpolAL.equalsIgnoreCase("positive"))
						{
							this.rdoverpol="ambiguous";
						}
						else
						{
							this.rdoverpol=this.rdpolOP;
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("positive"))
					{
						this.rdoverpol="ambiguous";
					}
				}
				else if(this.rdpolOP.equalsIgnoreCase("neutral"))
				{
					if(this.rdpolGI.equalsIgnoreCase("unknown") || this.rdpolGI.equalsIgnoreCase("neutral"))
					{
						if(this.rdpolAL.equalsIgnoreCase("unknown"))
						{
							this.rdoverpol=this.rdpolOP;
						}
						else
						{
							this.rdoverpol=this.rdpolAL;
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("positive"))
					{
						if(this.rdpolAL.equalsIgnoreCase("negative"))
						{
							this.rdoverpol="ambiguous";
						}
						else
						{
							this.rdoverpol=this.rdpolGI;
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("negative"))
					{
						if(this.rdpolAL.equalsIgnoreCase("positive"))
						{
							this.rdoverpol="ambiguous";
						}
						else
						{
							this.rdoverpol=this.rdpolGI;
						}
					}
				}
			}

		}
		String getWord() {
			return rdword;
		}
		String getPos() {
			return rdpos;
		}
		String getPolOP() {
			return rdpolOP;
		}
		String getPolGI() {
			return rdpolGI;
		}
		String getPolAL() {
			return rdpolAL;
		}
		String getOverPol() {
			return rdoverpol;
		}
		int getPosFlag() {
			return rdposflag;
		}
		int getNegFlag() {
			return rdnegflag;
		}
		String getWNOverPol() {
			return this.rdwnoverpol;
		}

	}
	public void testDictionary() throws IOException {
		try {
			RecordData rd;
			List<RecordData> list=new ArrayList<RecordData>();
			//Environment variable WNHOME consists of the base directory where WordNet software resides
			//String wnhome = System.getenv("WNHOME");
			//String path = wnhome + File.separator + "dict";
			String pathforwnhome="WordNet_3.0";
			String path = pathforwnhome + File.separator + "dict";
			URL url = new URL("file", null, path);
			IDictionary dict = new Dictionary(url);
			dict.open();
			BufferedReader inp=new BufferedReader(new InputStreamReader(System.in));
			//Accepting the input word and pos tag from the user
			System.out.println("Enter the word: ");
			String main_word=inp.readLine();
			System.out.println("Enter the pos of "+main_word+": 1. ADJECTIVE 2. ADVERB 3. NOUN 4. VERB");
			int main_pos=Integer.parseInt(inp.readLine());
			//Initializing the POS tagger with model left3words-wsj-0-18.tagger
			MaxentTagger tagger = new MaxentTagger("left3words-wsj-0-18.tagger");
			//HashMap that maps 1,2,3,4 to pos tags
			HashMap<Integer,String> posMap=new HashMap<Integer,String>();
			posMap.put(1,"Adjective");
			posMap.put(2,"Adverb");
			posMap.put(3,"Noun");
			posMap.put(4,"Verb");
			//HashMap that maps the POS tag abbreviations from the tagger output to pos tags
			HashMap<String,String> posMap1=new HashMap<String,String>();
			posMap1.put("JJ","Adjective"); posMap1.put("JJR","Adjective"); posMap1.put("JJS","Adjective");
			posMap1.put("RB","Adverb"); posMap1.put("RBR","Adverb");	posMap1.put("RBS","Adverb");
			posMap1.put("NN","Noun"); posMap1.put("NNS","Noun"); posMap1.put("NNP","Noun");
			posMap1.put("NNPS","Noun"); posMap1.put("VB","Verb"); posMap1.put("VBD","Verb");
			posMap1.put("VBG","Verb"); posMap1.put("VBN","Verb"); posMap1.put("VBP","Verb");
			posMap1.put("VBZ","Verb"); posMap1.put("CC","coor_conj"); posMap1.put("CC","card_num");
			posMap1.put("DT","det"); posMap1.put("EX","exis"); posMap1.put("FW","for_word");
			posMap1.put("IN","prep"); posMap1.put("LS","list_item"); posMap1.put("MD","modal");
			posMap1.put("PDT","pre_det"); posMap1.put("POS","pos_ending"); posMap1.put("PRP","per_pronoun");
			posMap1.put("PRP$","pos_pronoun"); posMap1.put("RP","particle"); posMap1.put("SYM","symbol");
			posMap1.put("TO","to"); posMap1.put("UH","interjection"); posMap1.put("WDT","wh_det");
			posMap1.put("WP","wh_pnoun"); posMap1.put("WP$","pos_wh_pnoun"); posMap1.put("WRB","wh_adverb");
			posMap1.put("CD","constant");
			IIndexWord idxWord=null;
			//Initializing stemmer
			Morphology morp=new Morphology();
			switch (main_pos) {
			case 1:
				idxWord = dict.getIndexWord(main_word,POS.ADJECTIVE);
				if(idxWord==null)
					idxWord = dict.getIndexWord(morp.stem(main_word),POS.ADJECTIVE);
				////System.out.println("The senses of "+main_word+" with pos ADJECTIVE having SIMILARTOS:");
				break;
			case 2:
				idxWord = dict.getIndexWord(main_word,POS.ADVERB);
				if(idxWord==null)
					idxWord = dict.getIndexWord(morp.stem(main_word),POS.ADVERB);
				////System.out.println("The senses of "+main_word+" with pos ADVERB having SIMILARTOS:");
				break;
			case 3:
				idxWord = dict.getIndexWord(main_word,POS.NOUN);
				if(idxWord==null)
					idxWord = dict.getIndexWord(morp.stem(main_word),POS.NOUN);
				////System.out.println("The senses of "+main_word+" with pos NOUN having SIMILARTOS:");
				break;
			case 4:
				idxWord = dict.getIndexWord(main_word,POS.VERB);
				if(idxWord==null)
					idxWord = dict.getIndexWord(morp.stem(main_word),POS.VERB);
				////System.out.println("The senses of "+main_word+" with pos VERB having SIMILARTOS:");
				break;
			default: throw new Exception("Pick one of the given choices.");
			}
			if(idxWord==null)
				throw new Exception("The given word and pos tag does not have any senses in wordnet");
			List<IWordID> wordID = idxWord.getWordIDs();
			Iterator<IWordID> i = wordID.iterator();
			int k=0;
			List<ISynsetID> isin=new ArrayList<ISynsetID>();
			//Displaying those senses of the word that have hyponyms
			while(i.hasNext() && k<wordID.size()) {
				IWordID wordID1 = idxWord.getWordIDs().get(k);
				isin.add(k,wordID1.getSynsetID());
				ISynset synset = dict.getSynset(isin.get(k));
				List<ISynsetID> temp=synset.getRelatedSynsets(Pointer.SIMILAR_TO);
				if(!(temp.isEmpty())) {
					for(IWord w : synset.getWords()) {
						////System.out.print("<"+w.getLemma()+"> ");
					}
					////System.out.print("- SENSE ID "+(k+1));
					////System.out.println("");
				}
				k=k+1;
			}
			////System.out.print("Enter the SENSE ID for s:");
			String ch=inp.readLine();
			int cho=Integer.parseInt(ch);
			if(cho>k||cho<1)
				throw new Exception("Please select ID from given range.");
			cho=cho-1;
			ISynset s = dict.getSynset(isin.get(cho));
			//Getting polarity of s from user
			System.out.println("Enter polarity of s (positive/negative): ");
			String s_polarity=inp.readLine();
			System.out.println("");
			List<ISynsetID> sid=s.getRelatedSynsets(Pointer.SIMILAR_TO);
			//Displaying hyponyms of s
			////System.out.println("SIMILARTOS of s are:");
			List<IWord> words1;	k=0;
			for(ISynsetID sid1 : sid){
				ISynset synset = dict.getSynset(sid1);
				words1 = dict.getSynset(sid1).getWords();
				for(IWord w : synset.getWords()) {
					////System.out.print("<"+w.getLemma()+"> ");
				}
				////System.out.print("- SimilarTo ID "+(k+1));
				////System.out.println("");
				k=k+1;
			}
			////System.out.print("Enter the SimilarTo ID for s':");
			String ch1=inp.readLine();
			int cho1=Integer.parseInt(ch1);
			if(cho1>k||cho1<1)
				throw new Exception("Please select ID from given range.");
			cho1=cho1-1;
			ISynset sprime = dict.getSynset(sid.get(cho1));
			//Displaying s, its polarity and its gloss
			////System.out.print("\nThe synset s is: ");
			for(IWord w : s.getWords()) {}
				////System.out.print("<"+w.getLemma()+"> ");
			////System.out.print("- Polarity: "+s_polarity);
			////System.out.println("\nGloss of s: "+s.getGloss());
			//Displaying s' and its gloss
			////System.out.print("\nThe synset s' is: ");
			for(IWord w : sprime.getWords()) {
				////System.out.print("<"+w.getLemma()+"> ");
				}
			////System.out.print("- Polarity to be determined");
			////System.out.println("\nGloss of s': "+sprime.getGloss());
			//Initializing connection to the SQl server database
			Connection con = null;
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			// String connectionUrl =
			// "jdbc:odbc:SQL_SERVER;user=PETITQUATRE\\Jiali;database=wordnet";
			String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=wordnet;";
			Properties prop = new Properties();
			prop.put("user", "user");
			prop.put("password", "password");
			con = DriverManager.getConnection(connectionUrl, prop);
			for(IWord w : sprime.getWords()) {
				PreparedStatement stmt2=con.prepareStatement("select DISTINCT word from dbo.StopWords where word=?");
				String sword=w.getLemma().replace("'s","");
				stmt2.setString(1,sword);
				ResultSet res2=stmt2.executeQuery();
				//Checking if the word is a stop word
				if(res2.next()==true) {
					while(res2.next()==true)
					{}
					res2.close();
					stmt2.close();
				}
				else {
					res2.close();
					stmt2.close();
					//Getting polarities of the word from GI and OP dictionaries
					rd=new RecordData(sword);
					PreparedStatement stmt=con.prepareStatement("select DISTINCT polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
					PreparedStatement stmt1=con.prepareStatement("select DISTINCT Polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
					stmt.setString(1,sword);
					stmt1.setString(1,sword);
					stmt.setString(2,posMap.get(main_pos));
					stmt1.setString(2,posMap.get(main_pos));
					if(posMap.get(main_pos).equals("adj") || posMap.get(main_pos).equals("adverb")) {
						stmt.setString(3,"anypos");
						stmt1.setString(3,"modif");
					}
					else {
						stmt.setString(3,"anypos");
						stmt1.setString(3,"");
					}
					rd.setPos(posMap.get(main_pos));
					ResultSet res=stmt.executeQuery();
					if (res.next())
						rd.setPolOP(res.getString("Polarity"));
					else
						rd.setPolOP("unknown");
					res.close();
					stmt.close();
					ResultSet res1=stmt1.executeQuery();
					if (res1.next())
						rd.setPolGI(res1.getString("Polarity"));
					else
						rd.setPolGI("unknown");
					res1.close();
					stmt1.close();
					list.add(rd);

					//If the word is an n-gram of unknown polarity then split it
					//and get polarities from GI and OP dictionaries for each split
					Pattern pat=Pattern.compile("_");
					if((rd.getWord().indexOf("_")!=-1) || (rd.getWord().indexOf("-")!=-1)) {
						String tword=rd.getWord();
						list.remove(rd);
						Matcher m=pat.matcher(tword);
						tword=m.replaceAll(" ");
						tword=tword.replace("-"," ");
						tword=tword.replace("'s","");
						//Tagging the n-gram word using POS tagger
						String taggedtword = tagger.tagTokenizedString(tword);
						String temp1[]=taggedtword.split(" ");
						for(int j=0;j<temp1.length;j++) {
							String temp2[]=temp1[j].split("_");
							PreparedStatement stmt3=con.prepareStatement("select DISTINCT word from dbo.StopWords where word=?");
							stmt3.setString(1,temp2[0]);
							ResultSet res3=stmt3.executeQuery();
							if(res3.next()==true) {
								while(res3.next()==true)
								{}
								res3.close();
								stmt3.close();
							}
							else {
								res3.close();
								stmt3.close();
								RecordData temp=new RecordData(temp2[0]);
								temp.setPos(posMap1.get(temp2[1]));
								stmt=con.prepareStatement("select DISTINCT polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
								stmt1=con.prepareStatement("select DISTINCT polarity from dbo.GeneralInquirerBackup where word=? and (pos=? or pos=?)");
								stmt.setString(1,temp2[0]);
								stmt.setString(2,posMap1.get(temp2[1]));
								if(posMap1.get(temp2[1]).equals("adj") || posMap1.get(temp2[1]).equals("adverb")) {
									stmt.setString(3,"anypos");
									stmt1.setString(3,"modif");
								}
								else {
									stmt.setString(3,"anypos");
									stmt1.setString(3,"");
								}
								ResultSet tem=stmt.executeQuery();
								if(tem.next())
									temp.setPolOP(tem.getString("Polarity"));
								else
									temp.setPolOP("unknown");
								tem.close();
								stmt.close();
								stmt1.setString(1,temp2[0]);
								stmt1.setString(2,posMap1.get(temp2[1]));
								ResultSet tem1=stmt1.executeQuery();
								if(tem1.next())
									temp.setPolGI(tem1.getString("Polarity"));
								else
									temp.setPolGI("unknown");
								tem1.close();
								stmt1.close();
								list.add(temp);
							}
						}
					}
				}
			}
			k=0;
			while(k<list.size()) {
				RecordData rd1=(RecordData) list.get(k);
				PreparedStatement stmt2=con.prepareStatement("select DISTINCT word from dbo.StopWords where word=?");
				//Stem the word and check if it is stop word; if so eliminate it
				stmt2.setString(1,morp.stem(rd1.getWord()));
				ResultSet res2=stmt2.executeQuery();
				if(res2.next()) {
					while(res2.next()) {}
					list.remove(rd1);
					if(k<list.size())
					rd1=(RecordData) list.get(k);
					else
						rd1=new RecordData();
					res2.close();
					stmt2.close();
				}
				res2.close();
				stmt2.close();
				//If polarity is unknown, Stem the word find its polarity in the OP dictionary
				if(rd1.getPolOP().equals("unknown")) {
					String sword=morp.stem(rd1.getWord());
					PreparedStatement stmt=con.prepareStatement("select DISTINCT polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
					stmt.setString(1,sword);
					stmt.setString(2,rd1.getPos());
					if(rd1.getPos().equals("adj") || rd1.getPos().equals("adverb"))
						stmt.setString(3,"anypos");
					else
						stmt.setString(3,"anypos");
					ResultSet res=stmt.executeQuery();
					if (res.next())
						rd1.setPolOP(res.getString("Polarity"));
					else
						rd1.setPolOP("unknown");
					res.close();
					stmt.close();
				}
				//If polarity is unknown, Stem the word find its polarity in the GI dictionary
				if(rd1.getPolGI().equals("unknown")) {
					String sword=morp.stem(rd1.getWord());
					PreparedStatement stmt1=con.prepareStatement("select DISTINCT polarity from dbo.GeneralInquirerBackup where word=? and (pos=? or pos=?)");
					stmt1.setString(1,sword);
					stmt1.setString(2,rd1.getPos());
					if(rd1.getPos().equals("adj") || rd1.getPos().equals("adverb"))
						stmt1.setString(3,"modif");
					else
						stmt1.setString(3,"");
					ResultSet res=stmt1.executeQuery();
					if (res.next())
						rd1.setPolGI(res.getString("Polarity"));
					else
						rd1.setPolGI("unknown");
					res.close();
					stmt1.close();
				}
				k++;
			}
			con.close();
			//Displaying the word and its details
			int poswords=0, negwords=0, neuwords=0, unkwords=0, ambwords=0;
			k=0;
			System.out.println("\nList of words in s' - RULE 2");
			while(k<list.size()) {
				RecordData rd1=(RecordData) list.get(k);
				System.out.print("\n"+(k+1)+": ");
				System.out.print("Word: "+rd1.getWord()+" - ");
				System.out.print("POS: "+rd1.getPos()+" - ");
				System.out.print("Polarity in GI: "+rd1.getPolGI()+" - ");
				System.out.print("Polarity in OP: "+rd1.getPolOP()+" - ");
				System.out.print("Overall Polarity: "+rd1.getOverPol());
				if(rd1.getOverPol().equalsIgnoreCase("positive"))
					poswords++;
				else if(rd1.getOverPol().equalsIgnoreCase("negative"))
					negwords++;
				else if(rd1.getOverPol().equalsIgnoreCase("unknown"))
					unkwords++;
				else if(rd1.getOverPol().equalsIgnoreCase("neutral"))
					neuwords++;
				else if(rd1.getOverPol().equalsIgnoreCase("ambiguous"))
					ambwords++;
				k++;
			}
			k=k-neuwords;
			//Check whether the hypothesis of Rule 2 is satisfied or not
			System.out.println("\n\nAnalysis of Hypothesis of Rule 2:");
			System.out.println("Positive Words: "+poswords+" - Negative Words: "+negwords);
			System.out.println("Neutral Words: "+neuwords+" - Unknown polarity Words: "+unkwords+" - Ambiguous polarity Words: "+ambwords);
			if(ambwords>0)
				System.out.println("Synset s' has words whose polarity may be positive or negative. Hence, hypothesis of Rule 2 is violated.");
			else if(poswords>0 && negwords>0)
				System.out.println("Synset s' has words of both positive "+"and negative polarities. Hence, hypothesis of Rule 2 is violated.");
			else if(poswords>0 && negwords==0 && poswords>=(0.5*k) && (s_polarity.equalsIgnoreCase("positive") || s_polarity.equalsIgnoreCase("p")))
				System.out.println("Synset s' has 50% or more words(excluding neutral words) of positive polarity. Hence, hypothesis of Rule 2 is satisfied.");
			else if(poswords==0 && negwords>0 && negwords>=(0.5*k) && (s_polarity.equalsIgnoreCase("negative") || s_polarity.equalsIgnoreCase("n")))
				System.out.println("Synset s' has 50% or more words(excluding neutral words) of negative polarity. Hence, hypothesis of Rule 2 is satisfied.");
			else
				System.out.println("Synset s' does not have 50% or more words(excluding neutral words) of "+s_polarity+" polarity. Hence, hypothesis of Rule 2 is violated.");
		}
		catch(IOException e) {
			System.out.println("IO exception");
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}


	public void rule2(ISynsetID synsetID, ISynset sprime, int pos,String polarity,ISynset s, BufferedWriter pw, BufferedWriter wordbw,int countoflines,MaxentTagger tagger, IDictionary dict, HashMap<Integer, String> posMap, HashMap<String, String> posMap1){
		try {
			RecordData rd;
			List<RecordData> list=new ArrayList<RecordData>();
			//Environment variable WNHOME consists of the base directory where WordNet software resides
			//String wnhome = System.getenv("WNHOME");
			//String path = wnhome + File.separator + "dict";


			//Initializing the POS tagger with model left3words-wsj-0-18.tagger
			//MaxentTagger tagger = new MaxentTagger("left3words-wsj-0-18.tagger");

			//HashMap that maps 1,2,3,4 to pos tags

			IIndexWord idxWord=null;

			//Initializing stemmer
			Morphology morp=new Morphology();

			System.out.println("Rule 2... entry point....");
				//Displaying s, its polarity and its gloss
				//System.out.print("\nThe synset s is: ");
				for(IWord w : s.getWords())
				{
					System.out.print("<"+w.getLemma()+"> ");
				}

				//System.out.print("- Polarity: "+polarity);
				//System.out.println("\nGloss of s: "+s.getGloss());
				//Displaying s' and its gloss
				//System.out.print("\nThe synset s' is: ");
				/*for(IWord w : sprime.getWords())
				{
					System.out.print("<"+w.getLemma()+"> ");
				}*/
				//System.out.print("- Polarity to be determined");
				//System.out.println("\nGloss of s': "+sprime.getGloss());
				//Initializing connection to the SQl server database
				Connection con = null;
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				// String connectionUrl =
				// "jdbc:odbc:SQL_SERVER;user=PETITQUATRE\\Jiali;database=wordnet";
				String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=wordnet;";
				Properties prop = new Properties();
				prop.put("user", "user");
				prop.put("password", "password");
				 con = DriverManager.getConnection(connectionUrl, prop);
				//System.out.println("\n\nDb connected.. \n\n");
				for(IWord w : sprime.getWords()) {
					PreparedStatement stmt2=con.prepareStatement("select DISTINCT word from dbo.StopWords where word=?");
					String sword=w.getLemma().replace("'s","");
					//System.out.println("sword : "+sword);
					stmt2.setString(1,sword);
					ResultSet res2=stmt2.executeQuery();
					//System.out.println("Checkpoint 1");
					//Checking if the word is a stop word
					if(res2.next()==true) {
						//System.out.println("Stop word");
						while(res2.next()==true)
						{}
						res2.close();
						stmt2.close();
					}
					else {
						//System.out.println("Not a Stop word");
						res2.close();
						stmt2.close();
						//Getting polarities of the word from GI and OP dictionaries
						rd=new RecordData(sword);
						PreparedStatement stmt=con.prepareStatement("select DISTINCT polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
						PreparedStatement stmt1=con.prepareStatement("select DISTINCT Polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
						PreparedStatement stmt_applex=con.prepareStatement("select DISTINCT Polarity from dbo.AppLexiconClean where Word=? and (Pos=? or Pos=?)");
						stmt.setString(1,sword);
						stmt_applex.setString(1,sword);
						stmt1.setString(1,sword);
						stmt.setString(2,posMap.get(pos));
						stmt_applex.setString(2,posMap.get(pos));
						//System.out.println("posMap.get(pos) : "+posMap.get(pos));
						stmt1.setString(2,posMap.get(pos));
						if(posMap.get(pos).equals("adjective") || posMap.get(pos).equals("adverb")) {
							stmt.setString(3,"anypos");
							stmt1.setString(3,"modif");
							stmt_applex.setString(3,"modif");
						}
						else {
							stmt.setString(3,"anypos");
							stmt1.setString(3,"");
							stmt_applex.setString(3,"");
						}
						rd.setPos(posMap.get(pos));
						ResultSet res=stmt.executeQuery();
						if (res.next())
							rd.setPolOP(res.getString("polarity"));
						else
							rd.setPolOP("unknown");
						res.close();
						stmt.close();
						ResultSet res1=stmt1.executeQuery();
						if (res1.next())
							rd.setPolGI(res1.getString("Polarity"));
						else
							rd.setPolGI("unknown");
						res1.close();
						stmt1.close();

						ResultSet tem1_applex=stmt_applex.executeQuery();
						if(tem1_applex.next())
							rd.setPolAL(tem1_applex.getString("Polarity"));
						else
							rd.setPolAL("unknown");
						tem1_applex.close();
						stmt_applex.close();
						list.add(rd);

						/*for(int b=0;b<list.size();b++)
						{
							//System.out.println("list ["+b+"] : "+list.get(b));
						}*/
						//If the word is an n-gram of unknown polarity then split it
						//and get polarities from GI and OP dictionaries for each split
						Pattern pat=Pattern.compile("_");
						if((rd.getWord().indexOf("_")!=-1) || (rd.getWord().indexOf("-")!=-1)) {
							String tword=rd.getWord();
							list.remove(rd);
							Matcher m=pat.matcher(tword);
							tword=m.replaceAll(" ");
							tword=tword.replace("-"," ");
							tword=tword.replace("'s","");
							//Tagging the n-gram word using POS tagger
							String taggedtword = tagger.tagTokenizedString(tword);
							String temp1[]=taggedtword.split(" ");
							for(int j=0;j<temp1.length;j++) {
								String temp2[]=temp1[j].split("_");
								PreparedStatement stmt3=con.prepareStatement("select DISTINCT word from dbo.StopWords where word=?");
								stmt3.setString(1,temp2[0]);
								ResultSet res3=stmt3.executeQuery();
								if(res3.next()==true) {
									while(res3.next()==true)
									{}
									res3.close();
									stmt3.close();
								}
								else {
									res3.close();
									stmt3.close();
									RecordData temp=new RecordData(temp2[0]);
									temp.setPos(posMap1.get(temp2[1]));
									stmt=con.prepareStatement("select DISTINCT polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
									stmt1=con.prepareStatement("select DISTINCT Polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
									stmt_applex=con.prepareStatement("select DISTINCT Polarity from dbo.AppLexiconClean where Word=? and (Pos=? or Pos=?)");
									stmt.setString(1,temp2[0]);
									stmt_applex.setString(1,temp2[0]);
									stmt.setString(2,posMap1.get(temp2[1]));
									stmt_applex.setString(2,posMap1.get(temp2[1]));
									if(posMap1.get(temp2[1]).equals("adjective") || posMap1.get(temp2[1]).equals("adverb")) {
										stmt.setString(3,"anypos");
										stmt1.setString(3,"modif");
										stmt_applex.setString(3,"modif");
									}
									else {
										stmt.setString(3,"anypos");
										stmt1.setString(3,"");
										stmt_applex.setString(3,"");
									}
									ResultSet tem=stmt.executeQuery();
									if(tem.next())
										temp.setPolOP(tem.getString("polarity"));
									else
										temp.setPolOP("unknown");
									tem.close();
									stmt.close();
									stmt1.setString(1,temp2[0]);
									stmt1.setString(2,posMap1.get(temp2[1]));
									ResultSet tem1=stmt1.executeQuery();
									if(tem1.next())
										temp.setPolGI(tem1.getString("Polarity"));
									else
										temp.setPolGI("unknown");
									tem1.close();
									stmt1.close();
									ResultSet pres1=stmt_applex.executeQuery();
									if (pres1.next())
										temp.setPolGI(pres1.getString("Polarity"));
									else
										temp.setPolGI("unknown");
									pres1.close();
									stmt_applex.close();
									list.add(temp);
								}
							}
						}
					}
				}
				int k=0;
				while(k<list.size()) {
					RecordData rd1=(RecordData) list.get(k);
					PreparedStatement stmt2=con.prepareStatement("select DISTINCT word from dbo.StopWords where word=?");
					//Stem the word and check if it is stop word; if so eliminate it
					stmt2.setString(1,morp.stem(rd1.getWord()));
					ResultSet res2=stmt2.executeQuery();
					if(res2.next()) {
						while(res2.next()) {}
						list.remove(rd1);
						if(k<list.size())
						rd1=(RecordData) list.get(k);
						else
							rd1=new RecordData();
						res2.close();
						stmt2.close();
					}
					res2.close();
					stmt2.close();
					//If polarity is unknown, Stem the word find its polarity in the OP dictionary
					if(rd1.getPolOP().equals("unknown")) {
						String sword=morp.stem(rd1.getWord());
						PreparedStatement stmt=con.prepareStatement("select DISTINCT polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
						stmt.setString(1,sword);
						stmt.setString(2,rd1.getPos());
						if(rd1.getPos().equals("adjective") || rd1.getPos().equals("adverb"))
							stmt.setString(3,"anypos");
						else
							stmt.setString(3,"anypos");
						ResultSet res=stmt.executeQuery();
						if (res.next())
							rd1.setPolOP(res.getString("Polarity"));
						else
							rd1.setPolOP("unknown");
						res.close();
						stmt.close();
					}
					//If polarity is unknown, Stem the word find its polarity in the GI dictionary
					if(rd1.getPolGI().equals("unknown")) {
						String sword=morp.stem(rd1.getWord());
						PreparedStatement stmt1=con.prepareStatement("select DISTINCT polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
						stmt1.setString(1,sword);
						stmt1.setString(2,rd1.getPos());
						if(rd1.getPos().equals("adjective") || rd1.getPos().equals("adverb"))
							stmt1.setString(3,"modif");
						else
							stmt1.setString(3,"");
						ResultSet res=stmt1.executeQuery();
						if (res.next())
							rd1.setPolGI(res.getString("Polarity"));
						else
							rd1.setPolGI("unknown");
						res.close();
						stmt1.close();
					}
					//If polarity is unknown, Stem the word find its polarity in the APPLEX dictionary
					if(rd1.getPolGI().equalsIgnoreCase("unknown")) {
						String sword=morp.stem(rd1.getWord());
						PreparedStatement stmt1=con.prepareStatement("select Polarity from dbo.AppLexiconClean where Word=? and (Pos=? or Pos=?)");
						stmt1.setString(1,sword);
						stmt1.setString(2,rd1.getPos());
						if(rd1.getPos().equalsIgnoreCase("adjective") || rd1.getPos().equalsIgnoreCase("adverb"))
							stmt1.setString(3,"modif");
						else
							stmt1.setString(3,"");
						ResultSet res=stmt1.executeQuery();
						if (res.next())
							rd1.setPolAL(res.getString("Polarity"));
						else
							rd1.setPolAL("unknown");
						res.close();
						stmt1.close();
					}
					k++;
				}
				con.close();
				//Displaying the word and its details
				int poswords=0, negwords=0, neuwords=0, unkwords=0, ambwords=0;
				k=0;
				System.out.println("\nList of words in s' - RULE 2");
				while(k<list.size()) {
					RecordData rd1=(RecordData) list.get(k);
					System.out.print("\n"+(k+1)+": ");
					System.out.print("Word: "+rd1.getWord()+" - ");
					System.out.print("POS: "+rd1.getPos()+" - ");
					System.out.print("Polarity in GI: "+rd1.getPolGI()+" - ");
					System.out.print("Polarity in OP: "+rd1.getPolOP()+" - ");
					System.out.print("Polarity in AL: "+rd1.getPolAL()+" - ");
					System.out.print("Overall Polarity: "+rd1.getOverPol());

					if(rd1.getOverPol().equalsIgnoreCase("positive"))
						poswords++;
					else if(rd1.getOverPol().equalsIgnoreCase("negative"))
						negwords++;
					else if(rd1.getOverPol().equalsIgnoreCase("unknown"))
						unkwords++;
					else if(rd1.getOverPol().equalsIgnoreCase("neutral"))
						neuwords++;
					else if(rd1.getOverPol().equalsIgnoreCase("ambiguous"))
						ambwords++;
					k++;
				}
				k=k-neuwords;
				//Check whether the hypothesis of Rule 2 is satisfied or not
				System.out.println("\n\nAnalysis of Hypothesis of Rule 2:");
				System.out.println("Positive Words: "+poswords+" - Negative Words: "+negwords);
				System.out.println("Neutral Words: "+neuwords+" - Unknown polarity Words: "+unkwords+" - Ambiguous polarity Words: "+ambwords);

				if(ambwords>0)
				{
					//System.out.println("Synset s' has words whose polarity may be positive or negative. Hence, hypothesis of Rule 2 is violated.");
					//call rule 3
					SimilarToRule3 t3=new SimilarToRule3("", 0, POS.ADVERB);
					if(!polarity.equalsIgnoreCase("NEUTRAL"))
					{
						t3.rule3(synsetID,sprime, 3, polarity, s, pw,wordbw,countoflines,tagger,dict,posMap,posMap1);
					}
				}
				else if(poswords>0 && negwords>0)
				{
					System.out.println("Synset s' has words of both positive "+"and negative polarities. Hence, hypothesis of Rule 2 is violated.");
					//call rule 3
					SimilarToRule3 t3=new SimilarToRule3("", 0, POS.ADVERB);
					if(!polarity.equalsIgnoreCase("NEUTRAL"))
					{
						t3.rule3(synsetID,sprime, 3, polarity, s, pw,wordbw,countoflines,tagger,dict,posMap,posMap1);
					}
				}
				else if(poswords>0 && negwords==0 && poswords>=(0.5*k) && (polarity.equalsIgnoreCase("positive") || polarity.equalsIgnoreCase("p")))
				{
					////System.out.println("Synset s' has 50% or more words(excluding neutral words) of positive polarity. Hence, hypothesis of Rule 2 is satisfied.");

					pwprint(pw, s,sprime, polarity,"POSITIVE", "Rule-2");

					//pw.write(synsetID+":"+s.getWords()+":"+polarity+":"+sprime.getID()+":"+sprime.getWords()+":POSITIVE");
					pw.newLine();

					List<IWord> l=sprime.getWords();
					//wordbw.write(sprime.toString()+"\t:POSITIVE");
					//wordbw.newLine();
					//if(l.size()>1)
					//{
						for(int jq=0;jq<list.size();jq++)
						{
							RecordData temp1=(RecordData)list.get(jq);
							if(!temp1.getOverPol().isEmpty() && !temp1.getOverPol().equalsIgnoreCase("unknown"))
							{
								wordbw.write(temp1.rdword.toUpperCase() + ":" +temp1.getOverPol().toUpperCase());
								wordbw.newLine();
							}
							else
							{
								wordbw.write(temp1.rdword.toUpperCase() + ":POSITIVE");
								wordbw.newLine();
							}

						}
						//wordbw.newLine();
					//}
					//if(l.size()==1)
					//{
					//	wordbw.write(l.get(0).getLemma()+":Positive");
					//}
					//wordbw.newLine();

				}
				else if(poswords==0 && negwords>0 && negwords>=(0.5*k) && (polarity.equalsIgnoreCase("negative") || polarity.equalsIgnoreCase("n")))
				{
					////System.out.println("Synset s' has 50% or more words(excluding neutral words) of negative polarity. Hence, hypothesis of Rule 2 is satisfied.");

					pwprint(pw, s,sprime, polarity,"NEGATIVE", "Rule-2");

					//pw.write(synsetID+":"+s.getWords()+":"+polarity+":"+sprime.getID()+":"+sprime.getWords()+":NEGATIVE");
					pw.newLine();

					List<IWord> l=sprime.getWords();
					//wordbw.write(sprime.toString()+"\t:NEGATIVE");
					//wordbw.newLine();
					//if(l.size()>1)
					//{
						for(int jq=0;jq<list.size();jq++)
						{
							RecordData temp1=(RecordData)list.get(jq);
							if(!temp1.getOverPol().isEmpty() && !temp1.getOverPol().equalsIgnoreCase("unknown"))
							{
								wordbw.write(temp1.rdword.toUpperCase() + ":" +temp1.getOverPol().toUpperCase());
								wordbw.newLine();
							}
							else
							{
								wordbw.write(temp1.rdword.toUpperCase() + ":NEGATIVE");
								wordbw.newLine();
							}

						}
						//wordbw.newLine();
				}
				else
				{
					////System.out.println("Synset s' does not have 50% or more words(excluding neutral words) of "+polarity+" polarity. Hence, hypothesis of Rule 2 is violated.");
					SimilarToRule3 t3=new SimilarToRule3("", 0, POS.ADVERB);
					if(!polarity.equalsIgnoreCase("NEUTRAL"))
					{
						t3.rule3(synsetID,sprime, 3, polarity, s, pw,wordbw,countoflines,tagger,dict,posMap,posMap1);
					}
				}
			}

		catch(IOException e) {
			System.out.println("IO exception");
			System.out.println("line no : "+countoflines);
			System.out.println("Rule 2");
		}
		catch(Exception e) {
			System.out.println(e);
			System.out.println("line no : "+countoflines);
			System.out.println("Rule 2");
			System.out.println(e.getMessage());
		}
	}

	private void pwprint(BufferedWriter pw, ISynset s, ISynset sprime,
			String polarity, String pol, String rule) throws IOException {
		// TODO Auto-generated method stub
		int sSize=s.getWords().size(); //getwords size
		pw.write("{");
		if (sSize==1){
			pw.write(s.getWord(1).getLemma()+"}");
		}
		if (sSize>1){
			for(int i=1;i<sSize;i++){
				//System.out.println(s.getWords().size());
				pw.write(s.getWord(i).getLemma()+", ");
			}
			pw.write(s.getWord(sSize).getLemma()+"}");
		}
		pw.write("\t"+polarity+"\t");
		int sprimeSize=sprime.getWords().size();
		pw.write("{");
		if (sprimeSize==1){
			pw.write(sprime.getWord(1).getLemma()+"}");
		}
		if (sprimeSize>1){
			for(int i=1;i<sprimeSize;i++){
				//System.out.println(s.getWords().size());
				pw.write(sprime.getWord(i).getLemma()+", ");
			}
			pw.write(sprime.getWord(sprimeSize).getLemma()+"}");
		}

		pw.write("\t"+pol+"\t"+rule);

	}
//	public static void main(String args[]) throws IOException {
//		SimilarToRule2 t2=new SimilarToRule2();
//		t2.testDictionary();
//		//t2.rule2(synid, pos, polarity)
//	}

}