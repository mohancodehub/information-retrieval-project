package simpleInconsistencyCheckWord;

import java.util.ArrayList;

import data.DictionaryEntry;

public class SimpleInconsistencyCheckWordProject {
	
	ArrayList<DictionaryEntry> dictionary;
	
	public SimpleInconsistencyCheckWordProject(ArrayList<DictionaryEntry> dictionary){
		this.dictionary = dictionary;
	}
	
	public ArrayList<DictionaryEntry> removeInconsistenciesManually() {
		//	Utils.print("Manually removing inconsistencies in polarity");

			ArrayList<DictionaryEntry> removeList = new ArrayList<DictionaryEntry>(
					dictionary.size());
			for (int i = 0; i < dictionary.size(); i++) {
				int flag = 0;
				for (int j = i + 1; j < dictionary.size(); j++) {
					if (!dictionary.get(i).getComment()
							.equalsIgnoreCase("MarkedWithInconsistency")
							&& dictionary.get(i).getWord()
									.equalsIgnoreCase(dictionary.get(j).getWord()))
						if (dictionary.get(i).getSpeech() == dictionary.get(j)
								.getSpeech()) {
							if (dictionary.get(i).getPolarity() != dictionary
									.get(j).getPolarity()) {
								flag = 1;
								removeList.add(dictionary.get(j));
								dictionary.get(j)
										.setComment("MarkedWithInconsistency");
							}
						}
				}
				if (flag == 1) {
					removeList.add(dictionary.get(i));
					dictionary.get(i).setComment("MarkedWithInconsistency");
				}
			}
			dictionary.removeAll(removeList);
			return removeList;
		}

}
