package hyponym;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.ISynsetID;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.item.Pointer;
import edu.stanford.nlp.process.Morphology;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

/*Class 'RecordData' which stores a word, its part-of-speech tag,
 * its polarity in General Inquirer (GI) dictionary,
 * its polarity in the OpinionFinder (OP) dictionary,
 * its overall polarity based on the GI and OP dictionaries*/
public class HyponymRule3 {
	public class RecordData {
		String rdword;
		String rdpos;
		String rdpolOP;
		String rdpolGI;
		String rdpolAL;
		String rdoverpol;
		String rdwnoverpol;
		int rdposflag;
		int rdnegflag;
		RecordData() {
			this.rdword="";
			this.rdpos="";
			this.rdpolOP="";
			this.rdpolGI="";
			this.rdpolAL="";
			this.rdoverpol="";
			rdposflag=0;
			rdnegflag=0;
			this.rdwnoverpol="";
		}
		RecordData(String _rdword) {
			this.rdword=_rdword;
			this.rdpos="";
			this.rdpolOP="";
			this.rdpolGI="";
			this.rdpolAL="";
			this.rdoverpol="";
			rdposflag=0;
			rdnegflag=0;
			this.rdwnoverpol="";
		}
		void setWord(String _rdword) {
			this.rdword=_rdword;
		}
		void setPos(String _rdpos) {
			this.rdpos=_rdpos;
		}

		void setPosFlag(int _posflag) {
			this.rdposflag=_posflag;
			this.setWNOverPol();
		}

		void setNegFlag(int _negflag) {
			this.rdnegflag=_negflag;
			this.setWNOverPol();
		}

		void setWNOverPol(String _wnoverpol) {
			this.rdwnoverpol=_wnoverpol;
		}

		void setWNOverPol() {
			if(this.rdposflag>=1 && this.rdnegflag==0)
				this.rdwnoverpol="positive";
			else if (this.rdposflag==0 && this.rdnegflag>=1)
				this.rdwnoverpol="negative";
			else if (this.rdposflag==0 && this.rdnegflag==0)
				this.rdwnoverpol="neutral";
			else if (this.rdposflag>0 && this.rdnegflag>0)
				this.rdwnoverpol="ambiguous";
		}

		void setPolOP(String _rdpolOP) {
			this.rdpolOP=_rdpolOP;
			this.setOverPol();
		}
		void setPolGI(String _rdpolGI) {
			this.rdpolGI=_rdpolGI;
			this.setOverPol();
		}
		void setPolAL(String _rdpolAL) {
			this.rdpolAL=_rdpolAL;
			this.setOverPol();
		}
		/*This method decides the overall polarity of a word based on its polarities
		 * in the GI and OP dictionaries*/
		void setOverPol() {

			if(this.rdpolOP.equals("") || this.rdpolGI.equals("") || this.rdpolAL.equals("")) {
			}
			else {
				if(this.rdpolOP.equalsIgnoreCase(this.rdpolGI) && this.rdpolOP.equalsIgnoreCase(this.rdpolAL))
					this.rdoverpol=rdpolOP;
				else if(this.rdpolOP.equalsIgnoreCase("unknown"))
				{
					if(this.rdpolGI.equalsIgnoreCase("unknown") || this.rdpolGI.equalsIgnoreCase("neutral"))
					{
						this.rdoverpol=this.rdpolAL;
					}
					else if(this.rdpolGI.equalsIgnoreCase("positive"))
					{
						if(this.rdpolAL.equalsIgnoreCase("unknown") || this.rdpolAL.equalsIgnoreCase("positive") || this.rdpolAL.equalsIgnoreCase("neutral"))
						{
							this.rdoverpol=this.rdpolGI;
						}
						else if(this.rdpolAL.equalsIgnoreCase("negative"))
						{
							this.rdoverpol="ambiguous";
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("negative"))
					{
						if(this.rdpolAL.equalsIgnoreCase("unknown") || this.rdpolAL.equalsIgnoreCase("negative") || this.rdpolAL.equalsIgnoreCase("neutral"))
						{
							this.rdoverpol=this.rdpolGI;
						}
						else if(this.rdpolAL.equalsIgnoreCase("positive"))
						{
							this.rdoverpol="ambiguous";
						}
					}

				}
				else if(this.rdpolOP.equalsIgnoreCase("positive"))
				{
					if(this.rdpolGI.equalsIgnoreCase("unknown") || this.rdpolGI.equalsIgnoreCase("positive") || this.rdpolGI.equalsIgnoreCase("neutral"))
					{
						if(this.rdpolAL.equalsIgnoreCase("negative"))
						{
							this.rdoverpol="ambiguous";
						}
						else
						{
							this.rdoverpol=this.rdpolOP;
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("negative"))
					{
						this.rdoverpol="ambiguous";
					}
				}
				else if(this.rdpolOP.equalsIgnoreCase("negative"))
				{
					if(this.rdpolGI.equalsIgnoreCase("unknown") || this.rdpolGI.equalsIgnoreCase("negative") || this.rdpolGI.equalsIgnoreCase("neutral"))
					{
						if(this.rdpolAL.equalsIgnoreCase("positive"))
						{
							this.rdoverpol="ambiguous";
						}
						else
						{
							this.rdoverpol=this.rdpolOP;
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("positive"))
					{
						this.rdoverpol="ambiguous";
					}
				}
				else if(this.rdpolOP.equalsIgnoreCase("neutral"))
				{
					if(this.rdpolGI.equalsIgnoreCase("unknown") || this.rdpolGI.equalsIgnoreCase("neutral"))
					{
						if(this.rdpolAL.equalsIgnoreCase("unknown"))
						{
							this.rdoverpol=this.rdpolOP;
						}
						else
						{
							this.rdoverpol=this.rdpolAL;
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("positive"))
					{
						if(this.rdpolAL.equalsIgnoreCase("negative"))
						{
							this.rdoverpol="ambiguous";
						}
						else
						{
							this.rdoverpol=this.rdpolGI;
						}
					}
					else if(this.rdpolGI.equalsIgnoreCase("negative"))
					{
						if(this.rdpolAL.equalsIgnoreCase("positive"))
						{
							this.rdoverpol="ambiguous";
						}
						else
						{
							this.rdoverpol=this.rdpolGI;
						}
					}
				}
			}

		}
		String getWord() {
			return rdword;
		}
		String getPos() {
			return rdpos;
		}
		String getPolOP() {
			return rdpolOP;
		}
		String getPolGI() {
			return rdpolGI;
		}
		String getPolAL() {
			return rdpolAL;
		}
		String getOverPol() {
			return rdoverpol;
		}
		int getPosFlag() {
			return rdposflag;
		}
		int getNegFlag() {
			return rdnegflag;
		}
		String getWNOverPol() {
			return this.rdwnoverpol;
		}

	}

	public void testDictionary() throws IOException {
		try {
			RecordData rd;
			//Environment variable WNHOME consists of the base directory where WordNet software resides
			//String wnhome = System.getenv("WNHOME");
			//String path = wnhome + File.separator + "dict";
			String pathforwnhome="C:\\WordNet_3.0_win32";
			String path = pathforwnhome + File.separator + "dict";
			URL url = new URL("file", null, path);
			IDictionary dict = new Dictionary(url);
			dict.open();
			BufferedReader inp=new BufferedReader(new InputStreamReader(System.in));
			//Accepting the input word and pos tag from the user
			System.out.println("Enter the word: ");
			String main_word=inp.readLine();
			System.out.println("Enter the pos of "+main_word+": 1. ADJECTIVE 2. ADVERB 3. NOUN 4. VERB");
			int main_pos=Integer.parseInt(inp.readLine());
			//Initializing the POS tagger with model left3words-wsj-0-18.tagger
			MaxentTagger tagger = new MaxentTagger("left3words-wsj-0-18.tagger");
			//HashMap that maps 1,2,3,4 to pos tags
			HashMap<Integer,String> posMap=new HashMap<Integer,String>();
			posMap.put(1,"adj");
			posMap.put(2,"adverb");
			posMap.put(3,"noun");
			posMap.put(4,"verb");
			//HashMap that maps the POS tag abbreviations from the tagger output to pos tags
			HashMap<String,String> posMap1=new HashMap<String,String>();
			posMap1.put("JJ","Adjective"); posMap1.put("JJR","Adjective"); posMap1.put("JJS","Adjective");
			posMap1.put("RB","Adverb"); posMap1.put("RBR","Adverb");	posMap1.put("RBS","Adverb");
			posMap1.put("NN","Noun"); posMap1.put("NNS","Noun"); posMap1.put("NNP","Noun");
			posMap1.put("NNPS","Noun"); posMap1.put("VB","Verb"); posMap1.put("VBD","Verb");
			posMap1.put("VBG","Verb"); posMap1.put("VBN","Verb"); posMap1.put("VBP","Verb");
			posMap1.put("VBZ","Verb"); posMap1.put("CC","coor_conj"); posMap1.put("CC","card_num");
			posMap1.put("DT","det"); posMap1.put("EX","exis"); posMap1.put("FW","for_word");
			posMap1.put("IN","prep"); posMap1.put("LS","list_item"); posMap1.put("MD","modal");
			posMap1.put("PDT","pre_det"); posMap1.put("POS","pos_ending"); posMap1.put("PRP","per_pronoun");
			posMap1.put("PRP$","pos_pronoun"); posMap1.put("RP","particle"); posMap1.put("SYM","symbol");
			posMap1.put("TO","to"); posMap1.put("UH","interjection"); posMap1.put("WDT","wh_det");
			posMap1.put("WP","wh_pnoun"); posMap1.put("WP$","pos_wh_pnoun"); posMap1.put("WRB","wh_adverb");
			posMap1.put("CD","constant");
			IIndexWord idxWord=null;
			//Initializing stemmer
			Morphology morp=new Morphology();
			switch (main_pos) {
			case 1:
				idxWord = dict.getIndexWord(main_word,POS.ADJECTIVE);
				if(idxWord==null)
					idxWord = dict.getIndexWord(morp.stem(main_word),POS.ADJECTIVE);
				System.out.println("The senses of "+main_word+" with pos ADJECTIVE having HYPONYMS:");
				break;
			case 2:
				idxWord = dict.getIndexWord(main_word,POS.ADVERB);
				if(idxWord==null)
					idxWord = dict.getIndexWord(morp.stem(main_word),POS.ADVERB);
				System.out.println("The senses of "+main_word+" with pos ADVERB having HYPONYMS:");
				break;
			case 3:
				idxWord = dict.getIndexWord(main_word,POS.NOUN);
				if(idxWord==null)
					idxWord = dict.getIndexWord(morp.stem(main_word),POS.NOUN);
				System.out.println("The senses of "+main_word+" with pos NOUN having HYPONYMS:");
				break;
			case 4:
				idxWord = dict.getIndexWord(main_word,POS.VERB);
				if(idxWord==null)
					idxWord = dict.getIndexWord(morp.stem(main_word),POS.VERB);
				System.out.println("The senses of "+main_word+" with pos VERB having HYPONYMS:");
				break;
			default: throw new Exception("Pick one of the given choices.");
			}
			if(idxWord==null)
				throw new Exception("The given word and pos tag does not have any senses in wordnet");
			List<IWordID> wordID = idxWord.getWordIDs();
			Iterator<IWordID> i = wordID.iterator();
			int k=0;
			List<ISynsetID> isin=new ArrayList<ISynsetID>();
			//Displaying those senses of the word that have hyponyms
			while(i.hasNext() && k<wordID.size()) {
				IWordID wordID1 = idxWord.getWordIDs().get(k);
				isin.add(k,wordID1.getSynsetID());
				ISynset synset = dict.getSynset(isin.get(k));
				List<ISynsetID> temp=synset.getRelatedSynsets(Pointer.SIMILAR_TO);
				if(!(temp.isEmpty())) {
					for(IWord w : synset.getWords()) {
						System.out.print("<"+w.getLemma()+"> ");
					}
					System.out.print("- SENSE ID "+(k+1));
					System.out.println("");
				}
				k=k+1;
			}
			System.out.print("Enter the SENSE ID for s:");
			String ch=inp.readLine();
			int cho=Integer.parseInt(ch);
			if(cho>k||cho<1)
				throw new Exception("Please select ID from given range.");
			cho=cho-1;
			ISynset s = dict.getSynset(isin.get(cho));
			//Getting polarity of s from user
			System.out.println("Enter polarity of s (positive/negative) :");
			String s_polarity=inp.readLine();
			System.out.println("");
			List<ISynsetID> sid=s.getRelatedSynsets(Pointer.SIMILAR_TO);
			//Displaying hyponyms of s
			System.out.println("Hyponyms of s are:");
			List<IWord> words1;	k=0;
			for(ISynsetID sid1 : sid){
				ISynset synset = dict.getSynset(sid1);
				words1 = dict.getSynset(sid1).getWords();
				for(IWord w : synset.getWords()) {
					System.out.print("<"+w.getLemma()+"> ");
				}
				System.out.print("- HYPONYM ID "+(k+1));
				System.out.println("");
				k=k+1;
			}
			System.out.print("Enter the HYPONYM ID for s':");
			String ch1=inp.readLine();
			int cho1=Integer.parseInt(ch1);
			if(cho1>k||cho1<1)
				throw new Exception("Please select ID from given range.");
			cho1=cho1-1;
			ISynset sprime = dict.getSynset(sid.get(cho1));
			//Displaying s, its polarity and its gloss
			System.out.print("\nThe synset s is: ");
			for(IWord w : s.getWords())
				System.out.print("<"+w.getLemma()+"> ");
			System.out.print("- Polarity: "+s_polarity);
			System.out.println("\nGloss of s: "+s.getGloss());
			//Displaying s' and its gloss
			System.out.print("\nThe synset s' is: ");
			for(IWord w : sprime.getWords())
				System.out.print("<"+w.getLemma()+"> ");
			System.out.print("- Polarity to be determined");
			System.out.println("\nGloss of s': "+sprime.getGloss());
			//Initializing connection to the SQl server database
			Connection con = null;
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			con = DriverManager.getConnection(
					"jdbc:odbc:SQL_SERVER;user=DEEPIKA-PC\\SQLEXPRESS;"
					+"database=wordnet");
			String gloss=sprime.getGloss();
			gloss=gloss.replace("(","");
			gloss=gloss.replace(")","");
			//Extracting the definition(s) of s' from its gloss
			int sc=gloss.indexOf("\"");
			if(sc!=-1)
				gloss=gloss.substring(0,sc-2);
			//Splitting the definitions of s' if there are more than one
			String spgloss[]=gloss.split(";");
			Pattern pat1=Pattern.compile("\\W");
			int poswords=0, negwords=0, neuwords=0, unkwords=0, ambwords=0, totk=0;
			//For each definition of s'
			for(int j=0;j<spgloss.length;j++) {
				List<RecordData> list=new ArrayList<RecordData>();
				Matcher mat1=pat1.matcher(spgloss[j]);
				mat1.replaceAll(" ");
				spgloss[j]=spgloss[j].replace("'s","");
				spgloss[j]=spgloss[j].replace("n't"," not");
				spgloss[j]=spgloss[j].trim();
				//Tagging the definition of s'
				String taggedgloss = tagger.tagTokenizedString(spgloss[j]);
				String sptgloss[]=taggedgloss.split(" ");
				for(int j1=0;j1<sptgloss.length;j1++) {
					//Extracting the parameters from the form "word_postag"
					String spsptgloss[]=sptgloss[j1].split("_");
					PreparedStatement stmt2=con.prepareStatement("select word from dbo.StopWords where word=?");
					stmt2.setString(1,spsptgloss[0]);
					ResultSet res2=stmt2.executeQuery();
					//Checking if the word is a stop word
					if(res2.next()==true) {
						while(res2.next()==true)
						{}
						res2.close();
						stmt2.close();
					}
					else {
						res2.close();
						stmt2.close();
						//Extracting the GI and OP dictionary polarities for the word
						rd=new RecordData(spsptgloss[0]);
						PreparedStatement stmt=con.prepareStatement("select polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
						PreparedStatement stmt1=con.prepareStatement("select polarity from dbo.GeneralInquirerBackup where word=? and (pos=? or pos=?)");
						stmt.setString(1,spsptgloss[0]);
						stmt1.setString(1,spsptgloss[0]);
						stmt.setString(2,posMap1.get(spsptgloss[1]));
						stmt1.setString(2,posMap1.get(spsptgloss[1]));
						if(posMap1.get(spsptgloss[1]).equalsIgnoreCase("adj") || posMap1.get(spsptgloss[1]).equalsIgnoreCase("adverb")) {
							stmt.setString(3,"anypos");
							stmt1.setString(3,"modif");
						}
						else {
							stmt.setString(3,"anypos");
							stmt1.setString(3,"");
						}
						rd.setPos(posMap1.get(spsptgloss[1]));
						ResultSet res=stmt.executeQuery();
						if (res.next())
							rd.setPolOP(res.getString("Polarity"));
						else
							rd.setPolOP("unknown");
						res.close();
						stmt.close();
						ResultSet res1=stmt1.executeQuery();
						if (res1.next())
							rd.setPolGI(res1.getString("Polarity"));
						else
							rd.setPolGI("unknown");
						res1.close();
						stmt1.close();
						list.add(rd);

						//If the word is an n-gram of unknown polarity then split it
						//and get polarities from GI and OP dictionaries for each split
						Pattern pat=Pattern.compile("_");
						if((rd.getWord().indexOf("_")!=-1) || (rd.getWord().indexOf("-")!=-1)) {
							String tword=rd.getWord();
							list.remove(rd);
							Matcher m=pat.matcher(tword);
							tword=m.replaceAll(" ");
							tword=tword.replace("-"," ");
							String taggedtword = tagger.tagTokenizedString(tword);
							String temp1[]=taggedtword.split(" ");
							for(int j2=0;j2<temp1.length;j2++) {
								String temp2[]=temp1[j2].split("_");
								PreparedStatement stmt3=con.prepareStatement("select word from dbo.StopWords where word=?");
								stmt3.setString(1,temp2[0]);
								ResultSet res3=stmt3.executeQuery();
								if(res3.next()==true) {
									while(res3.next()==true)
									{}
									res3.close();
									stmt3.close();
								}
								else {
									res3.close();
									stmt3.close();
									RecordData temp=new RecordData(temp2[0]);
									temp.setPos(posMap1.get(temp2[1]));
									stmt=con.prepareStatement("select polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
									stmt1=con.prepareStatement("select polarity from dbo.GeneralInquirerBackup where word=? and (pos=? or pos=?)");
									stmt.setString(1,temp2[0]);
									stmt.setString(2,posMap1.get(temp2[1]));
									if(posMap1.get(temp2[1]).equalsIgnoreCase("adj") || posMap1.get(temp2[1]).equalsIgnoreCase("adverb")) {
										stmt.setString(3,"anypos");
										stmt1.setString(3,"modif");
									}
									else {
										stmt.setString(3,"anypos");
										stmt1.setString(3,"");
									}
									ResultSet tem=stmt.executeQuery();
									if(tem.next())
										temp.setPolOP(tem.getString("Polarity"));
									else
										temp.setPolOP("unknown");
									tem.close();
									stmt.close();
									stmt1.setString(1,temp2[0]);
									stmt1.setString(2,posMap1.get(temp2[1]));
									ResultSet tem1=stmt1.executeQuery();
									if(tem1.next())
										temp.setPolGI(tem1.getString("Polarity"));
									else
										temp.setPolGI("unknown");
									tem1.close();
									stmt1.close();
									list.add(temp);
								}
							}
						}
					}
				}

			k=0;
			while(k<list.size()) {
				RecordData rd1=(RecordData) list.get(k);
				PreparedStatement stmt2=con.prepareStatement("select word from dbo.StopWords where word=?");
				//Stem the word and check if it is stop word; if so eliminate it
				stmt2.setString(1,morp.stem(rd1.getWord()));
				ResultSet res2=stmt2.executeQuery();
				if(res2.next()) {
					while(res2.next()) {}
					list.remove(rd1);
					if(k<list.size())
					rd1=(RecordData) list.get(k);
					else
						rd1=new RecordData();
				}
				res2.close();
				stmt2.close();
				//If polarity is unknown, Stem the word find its polarity in the OP dictionary
				if(rd1.getPolOP().equalsIgnoreCase("unknown")) {
					String sword=morp.stem(rd1.getWord());
					PreparedStatement stmt=con.prepareStatement("select polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
					stmt.setString(1,sword);
					stmt.setString(2,rd1.getPos());
					if(rd1.getPos().equalsIgnoreCase("adj") || rd1.getPos().equalsIgnoreCase("adverb"))
						stmt.setString(3,"anypos");
					else
						stmt.setString(3,"anypos");
					ResultSet res=stmt.executeQuery();
					if (res.next())
						rd1.setPolOP(res.getString("Polarity"));
					else
						rd1.setPolOP("unknown");
					res.close();
					stmt.close();
				}
				//If polarity is unknown, Stem the word find its polarity in the GI dictionary
				if(rd1.getPolGI().equalsIgnoreCase("unknown")) {
					String sword=morp.stem(rd1.getWord());
					PreparedStatement stmt1=con.prepareStatement("select polarity from dbo.GeneralInquirerBackup where word=? and (pos=? or pos=?)");
					stmt1.setString(1,sword);
					stmt1.setString(2,rd1.getPos());
					if(rd1.getPos().equalsIgnoreCase("adj") || rd1.getPos().equalsIgnoreCase("adverb"))
						stmt1.setString(3,"modif");
					else
						stmt1.setString(3,"");
					ResultSet res=stmt1.executeQuery();
					if (res.next())
						rd1.setPolGI(res.getString("Polarity"));
					else
						rd1.setPolGI("unknown");
					res.close();
					stmt1.close();
				}
				//Check for <Adjective,Noun> pairs and check if they meet the special rules
				if(rd1.getPos().equalsIgnoreCase("adj")) {
					if((k+1)<list.size()) {
						RecordData rd11=(RecordData) list.get(k+1);
						if(rd11.getPos().equalsIgnoreCase("noun")) {
							if(rd1.getOverPol().equalsIgnoreCase("positive") && rd11.getOverPol().equalsIgnoreCase("negative")) {
								String temp="<"+rd1.getWord()+"> <"+rd11.getWord()+">";
								RecordData rd2=new RecordData(temp);
								rd2.setPos("<adj> <noun>");
								rd2.setPolOP("negative");
								rd2.setPolGI("negative");
								list.remove(rd1);
								list.remove(rd11);
								list.add(k,rd2);
							}
							else if(rd1.getOverPol().equalsIgnoreCase("negative") && rd11.getOverPol().equalsIgnoreCase("positive")) {
								String temp="<"+rd1.getWord()+"> <"+rd11.getWord()+">";
								RecordData rd2=new RecordData(temp);
								rd2.setPos("<adj> <noun>");
								rd2.setPolOP("negative");
								rd2.setPolGI("negative");
								list.remove(rd1);
								list.remove(rd11);
								list.add(k,rd2);
							}
						}
					}
				}
				//Check for <Adverb,Adjective> pairs and check if they meet the special rules
				if(rd1.getPos().equalsIgnoreCase("adverb")) {
					if((k+1)<list.size()) {
						RecordData rd11=(RecordData) list.get(k+1);
						if(rd11.getPos().equalsIgnoreCase("adj")) {
							if(rd1.getOverPol().equalsIgnoreCase("positive") && rd11.getOverPol().equalsIgnoreCase("negative")) {
								String temp="<"+rd1.getWord()+"> <"+rd11.getWord()+">";
								RecordData rd2=new RecordData(temp);
								rd2.setPos("<adverb> <adj>");
								rd2.setPolOP("negative");
								rd2.setPolGI("negative");
								list.remove(rd1);
								list.remove(rd11);
								list.add(k,rd2);
							}
							else if(rd1.getOverPol().equalsIgnoreCase("negative") && rd11.getOverPol().equalsIgnoreCase("positive")) {
								String temp="<"+rd1.getWord()+"> <"+rd11.getWord()+">";
								RecordData rd2=new RecordData(temp);
								rd2.setPos("<adverb> <adj>");
								rd2.setPolOP("negative");
								rd2.setPolGI("negative");
								list.remove(rd1);
								list.remove(rd11);
								list.add(k,rd2);
							}
						}
					}
				}
				k++;
			}
			//Check for negation terms and flip polarities accordingly
			k=0;
			String negterms[]={"lack","no","not","loss","neither"};
			while(k<list.size()) {
				RecordData rd1=(RecordData) list.get(k);
				for(String neg : negterms) {
					if(neg.equalsIgnoreCase(rd1.getWord())) {
						rd1.setPos("negation term");
						int k1=k+1;
						while((k1)<list.size()) {
							RecordData rd11=(RecordData) list.get(k1);
							if(rd11.getPolOP().equalsIgnoreCase("unknown")) {}
							else if(rd11.getPolOP().equalsIgnoreCase("positive")) {rd11.setPolOP("negative");}
							else if(rd11.getPolOP().equalsIgnoreCase("negative")) {rd11.setPolOP("positive");}
							if(rd11.getPolGI().equalsIgnoreCase("unknown")) {}
							else if(rd11.getPolGI().equalsIgnoreCase("positive")) {rd11.setPolGI("negative");}
							else if(rd11.getPolGI().equalsIgnoreCase("negative")) {rd11.setPolGI("positive");}
							k1++;
						}
					}
				}
				k++;
			}
			//Display the word and its details
			k=0;
			System.out.println("\n\nList of words in definition "+(j+1)+" of s' with consideration to special rules - RULE 3");
			while(k<list.size()) {
				RecordData rd1=(RecordData) list.get(k);
				System.out.print("\n"+(k+1)+": ");
				System.out.print("Word: "+rd1.getWord()+" - ");
				System.out.print("POS: "+rd1.getPos()+" - ");
				System.out.print("Polarity in GI: "+rd1.getPolGI()+" - ");
				System.out.print("Polarity in OP: "+rd1.getPolOP()+" - ");
				System.out.print("Overall Polarity: "+rd1.getOverPol());
				if(rd1.getOverPol().equalsIgnoreCase("positive"))
					poswords++;
				else if(rd1.getOverPol().equalsIgnoreCase("negative"))
					negwords++;
				else if(rd1.getOverPol().equalsIgnoreCase("unknown"))
					unkwords++;
				else if(rd1.getOverPol().equalsIgnoreCase("neutral"))
					neuwords++;
				else if(rd1.getOverPol().equalsIgnoreCase("ambiguous"))
					ambwords++;
				k++;
			}
			totk=totk+k;
			}
			con.close();
			totk=totk-neuwords;
			//Check whether the hypothesis of Rule 3 is satisfied or not
			System.out.println("\n\nAnalysis of Hypothesis of Rule 3:");
			System.out.println("Positive Words: "+poswords+" - Negative Words: "+negwords);
			System.out.println("Neutral Words: "+neuwords+" - Unknown polarity Words: "+unkwords+" - Ambiguous polarity Words: "+ambwords);
			if(ambwords>0)
			{
				System.out.println("Definition of s' has words whose polarity may be positive or negative. Hence, hypothesis of Rule 3 is violated.");
			}
			else if(poswords>0 && negwords>0)
			{
				System.out.println("Definition of s' has words of both positive "+"and negative polarities. Hence, hypothesis of Rule 3 is violated.");
			}
			else if(poswords>0 && negwords==0 && poswords>=(0.5*totk) && (s_polarity.equalsIgnoreCase("positive") || s_polarity.equalsIgnoreCase("p")))
			{
				System.out.println("Definition of s' has 50% or more words/pairs(excluding neutral words) of positive polarity. Hence, hypothesis of Rule 3 is satisfied.");
			}
			else if(poswords==0 && negwords>0 && negwords>=(0.5*totk) && (s_polarity.equalsIgnoreCase("negative") || s_polarity.equalsIgnoreCase("n")))
			{
				System.out.println("Synset s' has 50% or more words/pairs(excluding neutral words) of negative polarity. Hence, hypothesis of Rule 3 is satisfied.");
			}
			else
			{
				System.out.println("Synset s' does not have 50% or more words/pairs(excluding neutral words) of "+s_polarity+" polarity. Hence, hypothesis of Rule 3 is violated.");
			}
		}
		catch(IOException e) {
			System.out.println("IO exception");
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}

//	public static void main(String args[]) throws IOException {
//		HyponymRule3 t3=new HyponymRule3();
//		t3.testDictionary();
//	}

	public void rule3(ISynsetID synsetID, ISynset sprime,  int pos, String polarity, ISynset s, BufferedWriter pw,BufferedWriter wordbw,int countoflines,MaxentTagger tagger, IDictionary dict, HashMap<Integer, String> posMap, HashMap<String, String> posMap1) {
		int k;
		// TODO Auto-generated method stub
		try {
			List<RecordData> list = null;
			RecordData rd;
			//Environment variable WNHOME consists of the base directory where WordNet software resides
			//String wnhome = System.getenv("WNHOME");
			//String path = wnhome + File.separator + "dict";

			System.out.println("Rule 3.. entry point....");
			//Initializing the POS tagger with model left3words-wsj-0-18.tagger
			//MaxentTagger tagger = new MaxentTagger("left3words-wsj-0-18.tagger");
			//HashMap that maps 1,2,3,4 to pos tags

			IIndexWord idxWord=null;
			//Initializing stemmer
			Morphology morp=new Morphology();

			//Displaying s, its polarity and its gloss
			System.out.print("\nThe synset s is: ");
			for(IWord w : s.getWords())
			{
				System.out.print("<"+w.getLemma()+"> ");
			}
				//System.out.print("<"+w.getLemma()+"> ");
			System.out.print("- Polarity: "+polarity);
			System.out.println("\nGloss of s: "+s.getGloss());
			//Displaying s' and its gloss
			System.out.print("\nThe synset s' is: ");
			for(IWord w : sprime.getWords())
			{
				System.out.print("<"+w.getLemma()+"> ");
			}
			System.out.print("- Polarity to be determined");
			System.out.println("\nGloss of s': "+sprime.getGloss());
			//Initializing connection to the SQl server database
			Connection con = null;
			Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			con = DriverManager.getConnection(
					"jdbc:odbc:SQL_SERVER;user=DEEPIKA-PC\\SQLEXPRESS;"
					+"database=wordnet");
			String gloss=sprime.getGloss();
			gloss=gloss.replace("(","");
			gloss=gloss.replace(")","");
			//Extracting the definition(s) of s' from its gloss
			int sc=gloss.indexOf("\"");
			if(sc!=-1)
				gloss=gloss.substring(0,sc-2);
			//Splitting the definitions of s' if there are more than one
			String spgloss[]=gloss.split(";");
			Pattern pat1=Pattern.compile("\\W");
			int poswords=0, negwords=0, neuwords=0, unkwords=0, ambwords=0, totk=0;
			//For each definition of s'
			for(int j=0;j<spgloss.length;j++) {
				list=new ArrayList<RecordData>();
				Matcher mat1=pat1.matcher(spgloss[j]);
				mat1.replaceAll(" ");
				spgloss[j]=spgloss[j].replace("'s","");
				spgloss[j]=spgloss[j].replace("n't"," not");
				spgloss[j]=spgloss[j].trim();
				//Tagging the definition of s'
				String taggedgloss = tagger.tagTokenizedString(spgloss[j]);
				String sptgloss[]=taggedgloss.split(" ");
				for(int j1=0;j1<sptgloss.length;j1++) {
					//Extracting the parameters from the form "word_postag"
					String spsptgloss[]=sptgloss[j1].split("_");
					PreparedStatement stmt2=con.prepareStatement("select word from dbo.StopWords where word=?");
					stmt2.setString(1,spsptgloss[0]);
					ResultSet res2=stmt2.executeQuery();
					//Checking if the word is a stop word
					if(res2.next()==true) {
						while(res2.next()==true)
						{}
						res2.close();
						stmt2.close();
					}
					else {
						res2.close();
						stmt2.close();
						//Extracting the GI, AL and OP dictionary polarities for the word
						rd=new RecordData(spsptgloss[0]);
						PreparedStatement stmt=con.prepareStatement("select polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
						PreparedStatement stmt1=con.prepareStatement("select Polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
						PreparedStatement stmt_applex=con.prepareStatement("select Polarity from dbo.AppLexiconClean where Word=? and (Pos=? or Pos=?)");
						stmt.setString(1,spsptgloss[0]);
						stmt_applex.setString(1,spsptgloss[0]);
						stmt1.setString(1,spsptgloss[0]);
						stmt.setString(2,posMap1.get(spsptgloss[1]));
						stmt_applex.setString(2,posMap1.get(spsptgloss[1]));
						stmt1.setString(2,posMap1.get(spsptgloss[1]));
						if(posMap1.get(spsptgloss[1]).equalsIgnoreCase("adjective") || posMap1.get(spsptgloss[1]).equalsIgnoreCase("adverb")) {
							stmt.setString(3,"anypos");
							stmt1.setString(3,"modif");
							stmt_applex.setString(3,"modif");
						}
						else {
							stmt.setString(3,"anypos");
							stmt1.setString(3,"");
							stmt_applex.setString(3,"");
						}
						rd.setPos(posMap1.get(spsptgloss[1]));
						ResultSet res=stmt.executeQuery();
						if (res.next())
							rd.setPolOP(res.getString("polarity"));
						else
							rd.setPolOP("unknown");
						res.close();
						stmt.close();
						ResultSet res1=stmt1.executeQuery();
						if (res1.next())
							rd.setPolGI(res1.getString("Polarity"));
						else
							rd.setPolGI("unknown");
						res1.close();
						stmt1.close();
						ResultSet tem1_applex=stmt_applex.executeQuery();
						if(tem1_applex.next())
							rd.setPolAL(tem1_applex.getString("Polarity"));
						else
							rd.setPolAL("unknown");
						tem1_applex.close();
						stmt_applex.close();

						list.add(rd);

						//If the word is an n-gram of unknown polarity then split it
						//and get polarities from GI and OP dictionaries for each split
						Pattern pat=Pattern.compile("_");
						if((rd.getWord().indexOf("_")!=-1) || (rd.getWord().indexOf("-")!=-1)) {
							String tword=rd.getWord();
							list.remove(rd);
							Matcher m=pat.matcher(tword);
							tword=m.replaceAll(" ");
							tword=tword.replace("-"," ");
							String taggedtword = tagger.tagTokenizedString(tword);
							String temp1[]=taggedtword.split(" ");
							for(int j2=0;j2<temp1.length;j2++) {
								String temp2[]=temp1[j2].split("_");
								PreparedStatement stmt3=con.prepareStatement("select word from dbo.StopWords where word=?");
								stmt3.setString(1,temp2[0]);
								ResultSet res3=stmt3.executeQuery();
								if(res3.next()==true) {
									while(res3.next()==true)
									{}
									res3.close();
									stmt3.close();
								}
								else {
									res3.close();
									stmt3.close();
									RecordData temp=new RecordData(temp2[0]);
									temp.setPos(posMap1.get(temp2[1]));
									stmt=con.prepareStatement("select polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
									stmt1=con.prepareStatement("select polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
									stmt_applex=con.prepareStatement("select Polarity from dbo.AppLexiconClean where Word=? and (Pos=? or Pos=?)");
									stmt.setString(1,temp2[0]);
									stmt_applex.setString(1,temp2[0]);
									stmt.setString(2,posMap1.get(temp2[1]));
									stmt_applex.setString(2,posMap1.get(temp2[1]));
									if(posMap1.get(temp2[1]).equalsIgnoreCase("adjective") || posMap1.get(temp2[1]).equalsIgnoreCase("adverb")) {
										stmt.setString(3,"anypos");
										stmt1.setString(3,"modif");
										stmt_applex.setString(3,"modif");
									}
									else {
										stmt.setString(3,"anypos");
										stmt1.setString(3,"");
										stmt_applex.setString(3,"");
									}
									ResultSet tem=stmt.executeQuery();
									if(tem.next())
										temp.setPolOP(tem.getString("Polarity"));
									else
										temp.setPolOP("unknown");
									tem.close();
									stmt.close();
									stmt1.setString(1,temp2[0]);
									stmt1.setString(2,posMap1.get(temp2[1]));
									ResultSet tem1=stmt1.executeQuery();
									if(tem1.next())
										temp.setPolGI(tem1.getString("Polarity"));
									else
										temp.setPolGI("unknown");
									tem1.close();
									stmt1.close();
									ResultSet pres1=stmt_applex.executeQuery();
									if (pres1.next())
										temp.setPolGI(pres1.getString("Polarity"));
									else
										temp.setPolGI("unknown");
									pres1.close();
									stmt_applex.close();
									list.add(temp);
								}
							}
						}
					}
				}

			k=0;
			while(k<list.size()) {
				RecordData rd1=(RecordData) list.get(k);
				PreparedStatement stmt2=con.prepareStatement("select word from dbo.StopWords where word=?");
				//Stem the word and check if it is stop word; if so eliminate it
				stmt2.setString(1,morp.stem(rd1.getWord()));
				ResultSet res2=stmt2.executeQuery();
				if(res2.next()) {
					while(res2.next()) {}
					list.remove(rd1);
					if(k<list.size())
					rd1=(RecordData) list.get(k);
					else
						rd1=new RecordData();
				}
				res2.close();
				stmt2.close();
				//If polarity is unknown, Stem the word find its polarity in the OP dictionary
				if(rd1.getPolOP().equalsIgnoreCase("unknown")) {
					String sword=morp.stem(rd1.getWord());
					PreparedStatement stmt=con.prepareStatement("select polarity from dbo.OpinionFinder where word=? and (pos=? or pos=?)");
					stmt.setString(1,sword);
					stmt.setString(2,rd1.getPos());
					if(rd1.getPos().equalsIgnoreCase("adjective") || rd1.getPos().equalsIgnoreCase("adverb"))
						stmt.setString(3,"anypos");
					else
						stmt.setString(3,"anypos");
					ResultSet res=stmt.executeQuery();
					if (res.next())
						rd1.setPolOP(res.getString("Polarity"));
					else
						rd1.setPolOP("unknown");
					res.close();
					stmt.close();
				}
				//If polarity is unknown, Stem the word find its polarity in the GI dictionary
				if(rd1.getPolGI().equalsIgnoreCase("unknown")) {
					String sword=morp.stem(rd1.getWord());
					PreparedStatement stmt1=con.prepareStatement("select polarity from dbo.GeneralInquirerBackup where Word=? and (Pos=? or Pos=?)");
					stmt1.setString(1,sword);
					stmt1.setString(2,rd1.getPos());
					if(rd1.getPos().equalsIgnoreCase("adjective") || rd1.getPos().equalsIgnoreCase("adverb"))
						stmt1.setString(3,"modif");
					else
						stmt1.setString(3,"");
					ResultSet res=stmt1.executeQuery();
					if (res.next())
						rd1.setPolGI(res.getString("Polarity"));
					else
						rd1.setPolGI("unknown");
					res.close();
					stmt1.close();
				}
				//If polarity is unknown, Stem the word find its polarity in the APPLEX dictionary
				if(rd1.getPolGI().equalsIgnoreCase("unknown")) {
					String sword=morp.stem(rd1.getWord());
					PreparedStatement stmt1=con.prepareStatement("select Polarity from dbo.AppLexiconClean where Word=? and (Pos=? or Pos=?)");
					stmt1.setString(1,sword);
					stmt1.setString(2,rd1.getPos());
					if(rd1.getPos().equalsIgnoreCase("adjective") || rd1.getPos().equalsIgnoreCase("adverb"))
						stmt1.setString(3,"modif");
					else
						stmt1.setString(3,"");
					ResultSet res=stmt1.executeQuery();
					if (res.next())
						rd1.setPolAL(res.getString("Polarity"));
					else
						rd1.setPolAL("unknown");
					res.close();
					stmt1.close();
				}
				//Check for <Adjective,Noun> pairs and check if they meet the special rules
				if(rd1.getPos().equalsIgnoreCase("adjective")) {
					if((k+1)<list.size()) {
						RecordData rd11=(RecordData) list.get(k+1);
						if(rd11.getPos().equalsIgnoreCase("noun")) {
							if(rd1.getOverPol().equalsIgnoreCase("positive") && rd11.getOverPol().equalsIgnoreCase("negative")) {
								String temp="<"+rd1.getWord()+"> <"+rd11.getWord()+">";
								RecordData rd2=new RecordData(temp);
								rd2.setPos("<adjective> <noun>");
								rd2.setPolOP("negative");
								rd2.setPolGI("negative");
								list.remove(rd1);
								list.remove(rd11);
								list.add(k,rd2);
							}
							else if(rd1.getOverPol().equalsIgnoreCase("negative") && rd11.getOverPol().equalsIgnoreCase("positive")) {
								String temp="<"+rd1.getWord()+"> <"+rd11.getWord()+">";
								RecordData rd2=new RecordData(temp);
								rd2.setPos("<adjective> <noun>");
								rd2.setPolOP("negative");
								rd2.setPolGI("negative");
								list.remove(rd1);
								list.remove(rd11);
								list.add(k,rd2);
							}
						}
					}
				}
				//Check for <Adverb,Adjective> pairs and check if they meet the special rules
				if(rd1.getPos().equalsIgnoreCase("adverb")) {
					if((k+1)<list.size()) {
						RecordData rd11=(RecordData) list.get(k+1);
						if(rd11.getPos().equalsIgnoreCase("adjective")) {
							if(rd1.getOverPol().equalsIgnoreCase("positive") && rd11.getOverPol().equalsIgnoreCase("negative")) {
								String temp="<"+rd1.getWord()+"> <"+rd11.getWord()+">";
								RecordData rd2=new RecordData(temp);
								rd2.setPos("<adverb> <adjective>");
								rd2.setPolOP("negative");
								rd2.setPolGI("negative");
								list.remove(rd1);
								list.remove(rd11);
								list.add(k,rd2);
							}
							else if(rd1.getOverPol().equalsIgnoreCase("negative") && rd11.getOverPol().equalsIgnoreCase("positive")) {
								String temp="<"+rd1.getWord()+"> <"+rd11.getWord()+">";
								RecordData rd2=new RecordData(temp);
								rd2.setPos("<adverb> <adjective>");
								rd2.setPolOP("negative");
								rd2.setPolGI("negative");
								list.remove(rd1);
								list.remove(rd11);
								list.add(k,rd2);
							}
						}
					}
				}
				k++;
			}
			//Check for negation terms and flip polarities accordingly
			k=0;
			String negterms[]={"lack","no","not","loss","neither"};
			while(k<list.size()) {
				RecordData rd1=(RecordData) list.get(k);
				for(String neg : negterms) {
					if(neg.equalsIgnoreCase(rd1.getWord())) {
						rd1.setPos("negation term");
						int k1=k+1;
						while((k1)<list.size()) {
							RecordData rd11=(RecordData) list.get(k1);
							if(rd11.getPolOP().equalsIgnoreCase("unknown")) {}
							else if(rd11.getPolOP().equalsIgnoreCase("positive")) {rd11.setPolOP("negative");}
							else if(rd11.getPolOP().equalsIgnoreCase("negative")) {rd11.setPolOP("positive");}
							if(rd11.getPolGI().equalsIgnoreCase("unknown")) {}
							else if(rd11.getPolGI().equalsIgnoreCase("positive")) {rd11.setPolGI("negative");}
							else if(rd11.getPolGI().equalsIgnoreCase("negative")) {rd11.setPolGI("positive");}
							k1++;
						}
					}
				}
				k++;
			}
			//Display the word and its details
			k=0;
			System.out.println("\n\nList of words in definition "+(j+1)+" of s' with consideration to special rules - RULE 3");
			while(k<list.size()) {
				RecordData rd1=(RecordData) list.get(k);
				System.out.print("\n"+(k+1)+": ");
				System.out.print("Word: "+rd1.getWord()+" - ");
				System.out.print("POS: "+rd1.getPos()+" - ");
				System.out.print("Polarity in GI: "+rd1.getPolGI()+" - ");
				System.out.print("Polarity in OP: "+rd1.getPolOP()+" - ");
				System.out.print("Polarity in AL: "+rd1.getPolAL()+" - ");
				System.out.print("Overall Polarity: "+rd1.getOverPol());
				if(rd1.getOverPol().equalsIgnoreCase("positive"))
					poswords++;
				else if(rd1.getOverPol().equalsIgnoreCase("negative"))
					negwords++;
				else if(rd1.getOverPol().equalsIgnoreCase("unknown"))
					unkwords++;
				else if(rd1.getOverPol().equalsIgnoreCase("neutral"))
					neuwords++;
				else if(rd1.getOverPol().equalsIgnoreCase("ambiguous"))
					ambwords++;
				k++;
				//System.out.println();
			}
			totk=totk+k;
			}
			con.close();
			totk=totk-neuwords;
			//Check whether the hypothesis of Rule 3 is satisfied or not
			System.out.println("\n\nAnalysis of Hypothesis of Rule 3:");
			System.out.println("Positive Words: "+poswords+" - Negative Words: "+negwords);
			System.out.println("Neutral Words: "+neuwords+" - Unknown polarity Words: "+unkwords+" - Ambiguous polarity Words: "+ambwords);
			if(ambwords>0)
			{
				System.out.println("Definition of s' has words whose polarity may be positive or negative. Hence, hypothesis of Rule 3 is violated.");
			}
			else if(poswords>0 && negwords>0)
			{
				System.out.println("Definition of s' has words of both positive "+"and negative polarities. Hence, hypothesis of Rule 3 is violated.");
			}
			else if(poswords>0 && negwords==0 && poswords>=(0.5*totk) && (polarity.equalsIgnoreCase("positive") || polarity.equalsIgnoreCase("p")))
			{
				System.out.println("Definition of s' has 50% or more words/pairs(excluding neutral words) of positive polarity. Hence, hypothesis of Rule 3 is satisfied.");
				pw.write(synsetID+":"+s.getWords()+":"+polarity+":"+sprime.getID()+":"+sprime.getWords()+":POSITIVE");
				pw.newLine();
				List<IWord> l=sprime.getWords();
				//wordbw.write(sprime.toString()+"\t:POSITIVE");
				//wordbw.newLine();
				//if(l.size()>1)
				//{
					for(int jq=0;jq<list.size();jq++)
					{
						RecordData temp=(RecordData)list.get(jq);
						if(!temp.getOverPol().isEmpty() && !temp.getOverPol().equalsIgnoreCase("unknown"))
						{
							wordbw.write(temp.rdword.toUpperCase() + ":" +temp.getOverPol().toUpperCase());
							wordbw.newLine();
						}
						else
						{
							wordbw.write(temp.rdword.toUpperCase() + ":POSITIVE");
							wordbw.newLine();
						}
					}
					//wordbw.newLine();
					//System.exit(0);
			}
			else if(poswords==0 && negwords>0 && negwords>=(0.5*totk) && (polarity.equalsIgnoreCase("negative") || polarity.equalsIgnoreCase("n")))
			{
				System.out.println("Synset s' has 50% or more words/pairs(excluding neutral words) of negative polarity. Hence, hypothesis of Rule 3 is satisfied.");
				pw.write(synsetID+":"+s.getWords()+":"+polarity+":"+sprime.getID()+":"+sprime.getWords()+":NEGATIVE");
				pw.newLine();

				List<IWord> l=sprime.getWords();
				//wordbw.write(sprime.toString()+"\t:POSITIVE");
				//wordbw.newLine();
				//if(l.size()>1)
				//{
					for(int jq=0;jq<list.size();jq++)
					{
						RecordData temp=(RecordData)list.get(jq);
						if(!temp.getOverPol().isEmpty() && !temp.getOverPol().equalsIgnoreCase("unknown"))
						{
							wordbw.write(temp.rdword.toUpperCase() + ":" +temp.getOverPol().toUpperCase());
							wordbw.newLine();
						}
						else
						{
							wordbw.write(temp.rdword.toUpperCase() + ":NEGATIVE" );
							wordbw.newLine();
						}
					}
					//wordbw.newLine();
					//System.exit(0);
			}
			else
			{
				System.out.println("Synset s' does not have 50% or more words/pairs(excluding neutral words) of "+polarity+" polarity. Hence, hypothesis of Rule 3 is violated.");
			}
		}
		catch(IOException e) {
			System.out.println("IO exception");
			System.out.println("line no : "+countoflines);
			System.out.println("Rule 3");
		}
		catch(Exception e) {
			System.out.println(e);
			System.out.println("line no : "+countoflines);
			System.out.println("Rule 3");
		}

	}
}