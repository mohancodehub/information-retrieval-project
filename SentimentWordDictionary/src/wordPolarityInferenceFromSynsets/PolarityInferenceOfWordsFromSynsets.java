package wordPolarityInferenceFromSynsets;

import java.net.MalformedURLException;
import java.util.ArrayList;

import data.DictionaryEntry;
import data.SynsetEntry;
import data.Utils.Polarity;
import data.Utils.Speech;
import edu.mit.jwi.item.POS;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;

public class PolarityInferenceOfWordsFromSynsets {
	
	public Speech getRunForSpeech() {
		return runForSpeech;
	}

	public void setRunForSpeech(Speech runForSpeech) {
		this.runForSpeech = runForSpeech;
	}

	public ArrayList<SynsetEntry> getAllSynsets() {
		return allSynsets;
	}

	public void setAllSynsets(ArrayList<SynsetEntry> allSynsets) {
		this.allSynsets = allSynsets;
	}

	public ArrayList<DictionaryEntry> getDictionary() {
		return dictionary;
	}

	public void setDictionary(ArrayList<DictionaryEntry> dictionary) {
		this.dictionary = dictionary;
	}

	Speech runForSpeech;
	ArrayList<SynsetEntry> allSynsets;
	ArrayList<DictionaryEntry> dictionary;
	
	public PolarityInferenceOfWordsFromSynsets(ArrayList<SynsetEntry> all,Speech runForSpeech, ArrayList<DictionaryEntry> dic) {
		this.runForSpeech = runForSpeech;
		this.allSynsets = all;
		this.dictionary = dic;
	}
	
	public int inferPolarityOfWordsFromObtainedSynsets(String pathforwnhome,
			WordNetDatabase database, String pirOutputPath, POS pos, String tempPath, int count, int stepCount)
			throws MalformedURLException {
		System.out.println("Inferring Polarity of Words from Synsets");
		ArrayList<DictionaryEntry> newWords = new ArrayList<DictionaryEntry>();
		ArrayList<String> processedWords = new ArrayList<String>(allSynsets.size());
		// SynsetType synsetType = new SynsetType(0);
		float positive = 0;
		float negative = 0;
		float neutral = 0;
		float unknown = 0;

		for (SynsetEntry synsetEntry : allSynsets) {
			if(!synsetEntry.getSpeech().equals(runForSpeech))
				continue;
			String word = synsetEntry.getWord();
			if(processedWords.contains(word))
				continue;
			else
				processedWords.add(word);

			positive = 0;
			negative = 0;
			neutral = 0;
			unknown = 0;

			for (SynsetEntry se : allSynsets) {
				if(!se.getSpeech().equals(runForSpeech))
					continue;
				if (se.getWord().equalsIgnoreCase(word)	&& database.getSynsets(word, getSynsetType()).length > se.getSynsetIndex()) {
					Synset[] synsets = database.getSynsets(word,getSynsetType());
					
					if(!(synsets[se.getSynsetIndex()].toString().contains(word)))
						continue;
					else{
						System.out.println(synsets[se.getSynsetIndex()].toString());
					}
					se.setFrequency(synsets[se.getSynsetIndex()].getTagCount(word));

					switch (se.getPolarity()) {
					case POSITIVE:
						positive += se.getFrequency();
						break;

					case NEGATIVE:
						negative += se.getFrequency();
						break;

					case NEUTRAL:
						neutral += se.getFrequency();
						break;

					case UNKNOWN:
						unknown += se.getFrequency();
						break;
					default:
						break;
					}
				}
			}
			
//
//			System.out.println("word positive total: " + positive);
//			System.out.println("word negative total: " + negative);

			float total = 0;
			if (positive != 0)
				total += positive;
			if (negative != 0)
				total += negative;
			if (negative != 0)
				total += neutral;
			if (neutral != 0)
				total += unknown;

			float wordPositive = 0;
			wordPositive = positive / total;

			float wordNegative = 0;
			wordNegative = negative / total;

			float wordNeutral = 0;
			wordNeutral = neutral / total;

			float wordUnknown = 0;
			wordUnknown = unknown / total;

			Polarity wordPolarity = Polarity.UNKNOWN;

			if (wordPositive > 0.5)
				wordPolarity = Polarity.POSITIVE;

			if (wordNegative > 0.5)
				wordPolarity = Polarity.NEGATIVE;
			
			if (wordNeutral > 0.5 || ((positive+unknown)/total <= 0.5 && (negative+unknown)/total <= 0.5))
				wordPolarity = Polarity.NEUTRAL;

			if (!wordPolarity.equals(Polarity.UNKNOWN))
				newWords.add(new DictionaryEntry(word, runForSpeech,
						wordPolarity, "ObtainedFromInferredSynsets"));
		}
		dictionary.addAll(newWords);

		System.out.println("New words added: " + newWords.size());
		for (DictionaryEntry de : newWords)
			System.out.println(de.printString());
		
		return newWords.size();
	}
	
	public SynsetType getSynsetType() {
		switch(runForSpeech) {
		case ADJECTIVE:
			return SynsetType.ADJECTIVE;
			
		case ADVERB:
			return SynsetType.ADVERB;
			
		case NOUN:
			return SynsetType.NOUN;
			
		case VERB:
			return SynsetType.VERB;
			
		default:
			return SynsetType.ADVERB;
		}
	}

	
}
