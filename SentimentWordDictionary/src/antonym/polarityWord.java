package antonym;

import edu.mit.jwi.item.IWord;

public class polarityWord {
	IWord word;
	String polarity;
	
	polarityWord(IWord aWord,String aPolarity){
		word=aWord;
		polarity=aPolarity;
	}
	
	public String getPolarity(){
		return polarity;
	}
	
	public String toString (){
		return word.toString()+" "+polarity;

	}
	
	
	
}
