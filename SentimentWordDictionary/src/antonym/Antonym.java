package antonym;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.ISynsetID;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.item.Pointer;

public class Antonym {
	// TODO: modify pos, and file to output.
	POS pos; // = POS.NOUN;
	static String outPutFile;
	String wordNetPath;
	static HashMap<String, String> listALL = new HashMap<String, String>();
	static HashMap<String, String> listLocal = new HashMap<String, String>();
	private static IDictionary dict = null;

	public void executeAntonym(String wordNetPath, String inputFilePath, POS pos) throws IOException {
		this.wordNetPath = wordNetPath;
		this.pos = pos;
		
		ArrayList<InferredWord> geAdj = new ArrayList<InferredWord>();
		
		/*
		 * Read from the file with the following details
		 * inferred word, position, inferred position, inferred polarity, conflict
		 * Read all the lines into an ArrayList geAdj
		 */
		geAdj = readPolarities(inputFilePath);
		outPutFile = "res//AntonymProject//output//output.txt";
		getList(inputFilePath);
		reset();
		process(geAdj);
	}

	private static void reset() throws IOException {
		File afile = new File(outPutFile);
		afile.delete();
		File newfile = new File(outPutFile);
		newfile.createNewFile();
		getALL("res//AntonymProject//all.txt");

	}

	public static Scanner fileScanner(String fName)
			throws FileNotFoundException {
		return new Scanner(new FileInputStream(fName));
	}

	public static PrintStream printStream(String fName)
			throws FileNotFoundException {
		return new PrintStream(new FileOutputStream(fName));
	}

	public static ArrayList<InferredWord> readPolarities(String fileName) {
		// ArrayList<String> originWords= new ArrayList<String>();
		ArrayList<InferredWord> originWords = new ArrayList<InferredWord>();
		Scanner sc = null;
		try {
			sc = fileScanner(fileName);
		} catch (FileNotFoundException e) {
			System.out.println("Error with input or output file");
			System.exit(0);
		}
		// System.out.print(sc.next());
		while (sc.hasNextLine()) {
			String geString[] = sc.nextLine().split(", ");
			// System.out.println(geString[0]);
			if (geString.length == 5) {
				InferredWord temp = new InferredWord(geString[0], geString[1],
						geString[2], geString[3], geString[4]);
				originWords.add(temp);
				// System.out.println(geString[0]+"\t"+geString[1]+"\t"+geString[2]+"\t"+
				// geString[3]+"\t"+geString[4]);
			}
			// sc.next(); // consume non-int
		}
		return originWords;

	}

	public void process(ArrayList<InferredWord> wordList)
			throws IOException {
		ArrayList<InferredWord> indirectList = new ArrayList<InferredWord>();

		for (int i = 0; i < wordList.size(); i++) {
			if (wordList.get(i).conflict == true)
				continue;
			String wordString = wordList.get(i).infWord;
			String polarity = wordList.get(i).infpolarity;
			int position = wordList.get(i).position;
			IIndexWord idxWord = dict.getIndexWord(wordString, pos);
			if (idxWord == null)
				continue;
			boolean flag = false;
			List<IWordID> wordIDs = idxWord.getWordIDs();
			if (wordIDs.size() < position) {
				System.out.println(wordList.get(i).toString()
						+ " Doesn't exist in dictionary");
				continue;
			}
			IWordID wordID = idxWord.getWordIDs().get(position - 1);
			IWordID antonymID = getDirect(wordID);
			if (antonymID == null) {
				indirectList.add(wordList.get(i));
			} else {
				flag = true;
				IWord antonym = dict.getWord(antonymID);
				IWord oriWOrd = dict.getWord(wordID);
				ISynset orisynset = oriWOrd.getSynset();
				ISynset antonymSynset = antonym.getSynset();
				String finalString = "";
				finalString += wordID.getLemma() + ", " + (position-1) + ", "	+ pos.toString() + ", " + polarity + "\n";
//				finalString += wordID.getLemma() + "\t" + position + "\t"
//						+ pos.toString() + "\t" + polarity + "\t{";
//				for (int k = 0; k < orisynset.getWords().size(); k++) {
//					List<IWord> allWords = orisynset.getWords();
//					finalString += allWords.get(k).getLemma() + " ";
//				}
//				finalString += "}\t";
//				// System.out.println(listLocal.get(antonymID.toString().substring(0,14)));
//				// System.out.println(antonymID.toString().substring(0,14));
//				if ((listLocal.get(antonymID.toString().substring(0, 14)) != null)
//						&& (listLocal
//								.get(antonymID.toString().substring(0, 14)))
//								.equalsIgnoreCase(polarity)) {
//
//					finalString += "Rule C-same P" + "\t"
//							+ dict.getWord(antonymID).getLemma() + "\t"
//							+ "Not Classify" + "\t{";
//				} else {
//					finalString += "Rule A" + "\t"
//							+ dict.getWord(antonymID).getLemma() + "\t"
//							+ negate(polarity) + "\t{";
//				}
//				for (int l = 0; l < antonymSynset.getWords().size(); l++) {
//					List<IWord> temp = antonymSynset.getWords();
//					finalString += temp.get(l).getLemma() + " ";
//				}
//				finalString += "}\t" + antonymSynset.getGloss() + "\n";
				Antonym.appendToFile(finalString);
			}

		}
		indirect(indirectList);
	}

	private void indirect(ArrayList<InferredWord> indirectList) {

		for (int i = 0; i < indirectList.size(); i++) {
			if (indirectList.get(i).conflict == true)
				continue;

			String wordString = indirectList.get(i).infWord;
			String polarity = indirectList.get(i).infpolarity;
			int position = indirectList.get(i).position;
			IIndexWord idxWord = dict.getIndexWord(wordString, pos);
			List<IWordID> wordIDs = idxWord.getWordIDs();
			if (wordIDs.size() < position) {
				System.out.println(indirectList.get(i).toString()
						+ " Doesn't exist in dictionary");
				continue;
			}
			IWordID wordID = idxWord.getWordIDs().get(position - 1);
			IWord word = dict.getWord(wordID);
			ISynset synset = word.getSynset();
			List<ISynsetID> simSynset = null;
			simSynset = synset.getRelatedSynsets(Pointer.SIMILAR_TO);
			if (simSynset.size() != 0) {
				IWordID temp = null;
				for (int k = 0; k < simSynset.size(); k++) {
					List<IWord> aword = null;
					aword = dict.getSynset(simSynset.get(k)).getWords();
					temp = getDirect(aword.get(0).getID());
					if (temp != null) {
						break;
					} else
						continue;
				}
				if (temp == null)
					continue;
				else {
					String wordLemma = dict.getWord(temp).getLemma();
					// System.out.println(wordLemma);
					applyRules(synset, indirectList.get(i), temp, wordLemma);
				}
			} else
				continue;
		}
	}

	private static void applyRules(ISynset orisynset, InferredWord aword,
			IWordID temp, String wordLemma) {
		String polarity = aword.infpolarity;
		if (polarity.equalsIgnoreCase("positive")
				|| polarity.equalsIgnoreCase("negative")) {
			int one;
			one = isRuleOne(polarity, temp);
			if (one == 1) {
				Printing("Rule B-1", orisynset, aword, temp, wordLemma, false);
			} else {
				boolean two = isRuleTwo(polarity, temp);
				if (two && one != -1) {
					Printing("Rule B-2", orisynset, aword, temp, wordLemma,
							false);

				} else {
					boolean three = isRuleThree(orisynset, polarity, temp);
					if (three && one != -1) {
						Printing("Rule B-3", orisynset, aword, temp, wordLemma,
								false);
					} else {
						if (one == -1) {
							Printing("Rule C, Same Polarity in Antonym Synset",
									orisynset, aword, temp, wordLemma, true);
						} else
							Printing("Rule C", orisynset, aword, temp,
									wordLemma, true);
					}
				}
			}
		}
	}

	public static void appendToFile(String content) {
		try {
			FileWriter writer = new FileWriter(outPutFile, true);
			writer.write(content);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void Printing(String Rules, ISynset orisynset,
			InferredWord aword, IWordID temp, String wordLemma, boolean flag) {

		IWord antWord = dict.getWord(temp);
		ISynset antonymSynset = antWord.getSynset();
		String polarity = aword.infpolarity;
		String finalString = "";
		
//		finalString += aword.infWord + "\t" + aword.position + "\t"	+ aword.infPos + "\t" + polarity + "\t{";
		finalString += aword.infWord + "," + (aword.position-1) + ","	+ aword.infPos + "," + polarity + "\n";
		System.out.println(aword.infWord + "," + (aword.position-1) + ","	+ aword.infPos + "," + polarity + "\n");
//		for (int i = 0; i < orisynset.getWords().size(); i++) {
//			List<IWord> allWords = orisynset.getWords();
//			finalString += allWords.get(i).getLemma() + " ";
//		}
//		
//		finalString += "}\t" + Rules + "\t";
//		
//		if (flag)
//			finalString += wordLemma + "\t" + "NOT CLASSIFY" + "\t{";
//		if (!flag)
//			finalString += wordLemma + "\t" + negate(polarity) + "\t{";
//		for (int l = 0; l < antonymSynset.getWords().size(); l++) {
//			List<IWord> allWords = antonymSynset.getWords();
//			finalString += allWords.get(l).getLemma() + " ";
//		}
//		finalString += "}\t" + antonymSynset.getGloss() + "\n";
		Antonym.appendToFile(finalString);

	}

	public static int isRuleOne(String polarity, IWordID temp) {

		String notP = negate(polarity);
		IWord aWord = dict.getWord(temp);
		ISynset antonymSynset = aWord.getSynset();
		List<IWord> allWords = antonymSynset.getWords();
		// System.out.println("SIZE IS : " +allWords.size());
		int flag = 0;
		for (int i = 0; i < allWords.size(); i++) {
			String wordString = allWords.get(i).getLemma();
			// System.out.print(wordString + " 11111");
			if (listLocal.get(temp.toString().substring(0, 14)) != null) { // exist
																			// in
																			// the
																			// list
				if (listLocal.get(temp.toString().substring(0, 14))
						.equalsIgnoreCase(notP))
					flag = 1;
				if (listLocal.get(temp.toString().substring(0, 14))
						.equalsIgnoreCase(polarity)) {
					return -1;
				}
			} else {
				return 0;
			}
		}
		// System.out.println(flag);
		return flag;
	}

	public static boolean isRuleTwo(String polarity, IWordID temp) {
		String notP = negate(polarity);
		IWord aWord = dict.getWord(temp);
		ISynset antonymSynset = aWord.getSynset();
		String contentString = antonymSynset.getGloss();
		String[] content = contentString.split(" ");
		// System.out.println(contentString);
		int total = 0;
		int correct = 0;
		boolean negate = false;
		for (int i = 0; i < content.length; i++) {
			String toCheck = content[i].toLowerCase();
			toCheck = toCheck.replaceAll("\"", "");
			// System.out.println(toCheck);
			negationWords nList = new negationWords();
			if (nList.nw.get(toCheck) != null) {
				negate = true;
				continue;
			}
			StopWords aList = new StopWords();
			if (negate == true) {
				if (toCheck.contains(",") || toCheck.contains(".")
						|| toCheck.contains("!") || toCheck.contains(";")
						|| toCheck.contains("?")) {
					toCheck.replace(",", "");
					toCheck.replace(".", "");
					toCheck.replace(";", "");
					toCheck.replace("!", "");
					toCheck.replace("?", "");
					negate = false;
					if (isStopWord(toCheck)) {
						continue;
					}
				}
				String pol = findinList(toCheck);
				if (pol.equalsIgnoreCase(polarity)) {
					correct++;
					total++;
				} else if (pol.equalsIgnoreCase(notP)) {
					// System.out.println("---------Rule B-2-------Confilct");
					return false;
				} else {
					total++;
				}
			} else {
				if (isStopWord(toCheck))
					continue;
				else if (findinList(toCheck).equalsIgnoreCase(notP)) {
					correct++;
					total++;
				}

				else if (findinList(toCheck).equalsIgnoreCase(polarity)) {
					// System.out.println("---------Rule B-2-------Confilct");
					return false;
				} else {
					total++;
				}
			}

		}
		if (correct == 0)
			return false;
		else if ((double) correct / total > 0.5)
			return true;
		else
			return false;

	}

	private static boolean isStopWord(String toCheck) {
		StopWords aList = new StopWords();
		if (aList.sw.get(toCheck) != null)
			return true;
		return false;
	}

	private static String findinList(String toCheck) {
		if (listALL.get(toCheck) == null)
			return "UNKNOWN";
		else {
			String toreturn = (String) listALL.get(toCheck);
			return toreturn;
		}
	}

	private static String findinListSuffix(String toCheck) {
		String toReturn = null;
		// de-, dis-, in-, il-, im-, ir- , non-, un-, and �less
		if (toCheck.startsWith("de"))
			toCheck.replace("de", "");
		else if (toCheck.startsWith("dis"))
			toCheck.replace("dis", "");
		else if (toCheck.startsWith("in"))
			toCheck.replace("in", "");
		else if (toCheck.startsWith("il"))
			toCheck.replace("il", "");
		else if (toCheck.startsWith("im"))
			toCheck.replace("im", "");
		else if (toCheck.startsWith("ir"))
			toCheck.replace("ir", "");
		else if (toCheck.startsWith("non"))
			toCheck.replace("non", "");
		else if (toCheck.startsWith("un"))
			toCheck.replace("un", "");
		else if (toCheck.endsWith("less"))
			toCheck.replace("less", "");
		else {
			toReturn = findinList(toCheck);
			return toReturn;
		}
		toReturn = findinList(toCheck);
		toReturn = negate(toReturn);
		return toReturn;
	}

	public static boolean isRuleThree(ISynset orisynset, String polarity,
			IWordID temp) {
		List<IWord> origWords = orisynset.getWords();
		String notP = negate(polarity);
		IWord aWord = dict.getWord(temp);
		ISynset antonymSynset = aWord.getSynset();
		List<IWord> antonymWords = antonymSynset.getWords();

		// boolean flag=false;
		int correct = 0;
		int total = antonymWords.size();
		for (int i = 0; i < total; i++) {
			IWord oneAWord = antonymWords.get(i);
			String wordString = oneAWord.getLemma();
			if (listLocal.get(oneAWord.getID().toString().substring(0, 14)) != null
					&& listLocal.get(
							oneAWord.getID().toString().substring(0, 14))
							.equalsIgnoreCase(notP)) {
				correct++;
			} else {
				String desuffix = removeSuffix(wordString);
				if (wordString.equalsIgnoreCase(desuffix)) {
					for (int j = 0; j < origWords.size(); j++) {
						IWord oneOWord = origWords.get(j);
						String oriwordString = oneOWord.getLemma();
						String desuffixOrig = removeSuffix(oriwordString);
						if (wordString.equalsIgnoreCase(desuffixOrig)) {
							correct++;
							break;
						}
					}
				} else {
					for (int j = 0; j < origWords.size(); j++) {
						IWord oneOWord = origWords.get(j);
						String oriwordString = oneOWord.getLemma();
						if (desuffix.equalsIgnoreCase(oriwordString)) {
							correct++;
							break;
						}
					}

				}

			}

			/*
			 * //System.out.print(wordString + " 11111"); if
			 * (findinListSuffix(wordString).equalsIgnoreCase(notP)){ flag=true;
			 * } else if
			 * (findinListSuffix(wordString).equalsIgnoreCase(polarity)){
			 * //System.out.println("---------Rule B-3---Confilct"); return
			 * false; } else { flag=false; }
			 */
		}

		if (((double) correct / total) > 0.5) {
			return true;
		} else
			return false;
		// System.out.println(flag);
	}

	private static String removeSuffix(String toCheck) {
		String toReturn = null;
		// de-, dis-, in-, il-, im-, ir- , non-, un-, and �less
		if (toCheck.startsWith("de"))
			toCheck.replace("de", "");
		else if (toCheck.startsWith("dis"))
			toCheck.replace("dis", "");
		else if (toCheck.startsWith("in"))
			toCheck.replace("in", "");
		else if (toCheck.startsWith("il"))
			toCheck.replace("il", "");
		else if (toCheck.startsWith("im"))
			toCheck.replace("im", "");
		else if (toCheck.startsWith("ir"))
			toCheck.replace("ir", "");
		else if (toCheck.startsWith("non"))
			toCheck.replace("non", "");
		else if (toCheck.startsWith("un"))
			toCheck.replace("un", "");
		else if (toCheck.endsWith("less"))
			toCheck.replace("less", "");
		return toCheck;

	}

	private static IWordID getDirect(IWordID iWordID) {

		IWord word = dict.getWord(iWordID);
		ISynset synset = word.getSynset();
		List<IWord> synWords = synset.getWords();
		List<IWordID> opposedWords = null;
		for (int i = 0; i < synWords.size(); i++) {
			opposedWords = synWords.get(i).getRelatedWords(Pointer.ANTONYM);
			if (opposedWords.size() != 0) {
				IWordID antonymID = opposedWords.get(0);
				return antonymID;
			} else
				continue;
		}
		return null;
	}

	public static String negate(String pol) {
		if (pol.equalsIgnoreCase("positive"))
			return "negative";
		else if (pol.equalsIgnoreCase("negative"))
			return "positive";
		else if (pol.equalsIgnoreCase("neutral"))
			return "neutral";
		else if (pol.equalsIgnoreCase("UNKNOWN"))
			return "UNKNOWN";
		else
			return "ERROR";
	}

	private static void getALL(String fileName) {
		Scanner sc = null;
		try {
			sc = fileScanner(fileName);
		} catch (FileNotFoundException e) {
			System.out.println("Error with input or output file");
			System.exit(0);
		}
		while (sc.hasNextLine()) {
			String geString[] = sc.nextLine().split("\t");
			listALL.put(geString[0], geString[2]);

		}

	}

	public void getList(String fileName) {
		URL url = null;
		System.out.println("Filename in getList --->" + fileName);
		try {
			url = new URL("file", null, wordNetPath+"dict");
			System.out.println("The URL in getList is--->" + url);
		} catch (MalformedURLException e) {
			throw new RuntimeException(e);
		}
		dict = new Dictionary(url);
		dict.open();

		Scanner sc = null;
		try {
			sc = fileScanner(fileName);
		} catch (FileNotFoundException e) {
			System.out.println("Error with input or output file");
			System.exit(0);
		}
		int count = 0;
		while (sc.hasNextLine()) {
			String geString[] = sc.nextLine().split(", ");
			count++;
			/*
			 * The content has 5 words separated by commas
			 * and if the conflict is false
			 */
			if (geString.length == 5 && geString[4].equalsIgnoreCase("False")) {

				// listLocal.put(geString[0],geString[3]);
				String wordString = geString[0];
				int position = Integer.parseInt(geString[1]);
				System.out.println("count => "+count +" word=>"+wordString +" position ==>" + position);
				/*
				 * Class from the MIT Word net Interface
				 * to represent a index word object
				 */
				IIndexWord idxWord = dict.getIndexWord(wordString, pos);
			//	System.out.println("position: " + (position - 1) + " --> "	+ idxWord.getWordIDs().get(position - 1));
		//		System.out.println("idxWord-->"+ idxWord);
		//		System.out.println("IWordID-->"+ idxWord.getWordIDs());
				if(idxWord!= null && idxWord.getWordIDs()!= null && position-1<idxWord.getWordIDs().size()) {
				IWordID wordID = idxWord.getWordIDs().get(position - 1);
				 System.out.println(wordID.toString().substring(0,14)+"\t"+geString[3]);
				listLocal.put(wordID.toString().substring(0, 14), geString[3]);
				}
			}

			// sc.next(); // consume non-int
		}

		// System.out.println("adjsize:"+listLocal.size());

	}

}// end class

