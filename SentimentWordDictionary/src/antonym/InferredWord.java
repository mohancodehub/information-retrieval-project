package antonym;

public class InferredWord {
	String infWord;
	int position;
	String infPos;
	String infpolarity;
	boolean conflict;
	
	InferredWord(){
		
	}
	InferredWord(String infWord,String position,String infPos,String infpolarity, String conflict){
		this.infWord=infWord;
		this.position=Integer.parseInt( position );
		this.infPos=infPos;
		this.infpolarity=infpolarity;
		if(conflict.equalsIgnoreCase("False"))
			this.conflict=false;
		else if (conflict.equalsIgnoreCase("True"))
			this.conflict=true;
	}
	
	public String toString(){
		return infWord+" "+infPos+" "+infpolarity;
	}
}
