package sentimentdictionaryapplication;

import hyponym.HyponymRule1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map.Entry;
import java.util.Set;

import polarityinconsistency.PolarityInconsistency;
import similarto.SimilarToRule1;
import wordPolarityInferenceFromSynsets.PolarityInferenceOfWordsFromSynsets;
import antonym.Antonym;
import data.DictionaryEntry;
import data.SynsetEntry;
import data.Utils;
import data.Utils.Polarity;
import data.Utils.Speech;
import edu.mit.jwi.item.POS;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class SWDOperations {

	ArrayList<DictionaryEntry> dictionary;
	ArrayList<DictionaryEntry> oldDictionary;
	ArrayList<SynsetEntry> allSynsets;
	ArrayList<SynsetEntry> oldSynsets;

	Speech runForSpeech;
	public static MaxentTagger tagger = null;

	public SWDOperations() {
		runForSpeech = Speech.ALL;
		try {
			tagger = new MaxentTagger("libraries/left3words-wsj-0-18.tagger");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		allSynsets = new ArrayList<SynsetEntry>();
		oldSynsets = new ArrayList<SynsetEntry>();
	}

	public ArrayList<DictionaryEntry> getDictionary() {
		return dictionary;
	}

	public void setDictionary(ArrayList<DictionaryEntry> dictionary) {
		this.dictionary = dictionary;
	}

	public void addToSynsetsList(SynsetEntry entry) {
		this.allSynsets.add(entry);
	}
	
	public void removeDuplicatesInDictionary() {
		LinkedHashSet<DictionaryEntry> listToSet = new LinkedHashSet<DictionaryEntry>(dictionary);
		dictionary = new ArrayList<DictionaryEntry>(listToSet);
	}
	
	//Not Used
	public void removeDuplicatesInDictionary2() {
		ArrayList<String> allList = new ArrayList<String>();
		for (DictionaryEntry de : dictionary)
			allList.add(de.getWord().toString() + ","
					+ de.getSpeech().toString() + ","
					+ de.getPolarity().toString());
		LinkedHashSet<String> listToSet = new LinkedHashSet<String>(allList);
		allList = new ArrayList<String>(listToSet);
		Collections.sort(allList);
		dictionary = convertToDictionaryEntries(allList, ",");
	}
	
	//Not Used
	public ArrayList<DictionaryEntry> removeDuplicatesInDictionary3() {
		System.out.println("Removing duplicates in dictionary");
		ArrayList<String> allList = new ArrayList<String>();
		ArrayList<DictionaryEntry> removeList = new ArrayList<DictionaryEntry>();
		for(int i=0;i<dictionary.size();i++) {
			if(i%200 == 0)
			System.out.println("Ran for " + i + "/" + dictionary.size());
			for(int j=i+1;j<dictionary.size();j++) {
				DictionaryEntry di = dictionary.get(i);
				DictionaryEntry dj = dictionary.get(j);
				if(!removeList.contains(di) && !removeList.contains(dj) && 
						di.getWord().equalsIgnoreCase(dj.getWord()) && 
						di.getSpeech().toString().equalsIgnoreCase(dj.getSpeech().toString()) && 
						di.getPolarity().toString().equalsIgnoreCase(dj.getPolarity().toString())) {
					removeList.add(dj);
				}
			}
		}
		dictionary.removeAll(removeList);
		
		for (DictionaryEntry de : dictionary)
			allList.add(de.getWord().toString() + ","
					+ de.getSpeech().toString() + ","
					+ de.getPolarity().toString() + ","
					+ de.getSource());
		Collections.sort(allList);
		dictionary = convertToDictionaryEntries(allList, ",");
		
		return removeList;
	}

	public void combineSmallSentimentDictionaries(Speech partOfSpeech,
			int runForNLines) {
		runForSpeech = partOfSpeech;
		Utils.print("Combining 3 small dictionaries into one single dictionary");
		dictionary = new ArrayList<DictionaryEntry>();
		ArrayList<String> opinionFinder = Utils.readFromFile("res/dictionaries/OpinionFinder.txt");
		ArrayList<String> generalInquirer = Utils.readFromFile("res/dictionaries/GeneralInquirer.txt");
		ArrayList<String> appraisalLexicon = Utils.readFromFile("res/dictionaries/AppraisalLexicon.txt");
		ArrayList<String> allList = new ArrayList<String>(opinionFinder.size()	+ generalInquirer.size() + appraisalLexicon.size());

		allList.addAll(Utils.appendStringToEachItem(opinionFinder, "OF", ","));
		allList.addAll(Utils.appendStringToEachItem(generalInquirer, "GI", ","));
		allList.addAll(Utils.appendStringToEachItem(appraisalLexicon, "AL", ","));

		dictionary = convertToDictionaryEntries(allList, ",");
		if (!partOfSpeech.equals(Speech.ALL)) {
			ArrayList<DictionaryEntry> tempDict = new ArrayList<DictionaryEntry>(dictionary.size());
			for (DictionaryEntry de : dictionary)
				if (de.getSpeech().equals(partOfSpeech))
					tempDict.add(de);
			dictionary = tempDict;
		}

		if (runForNLines != 0) {
			removeDuplicatesInDictionary();
			//System.out.println("TESTING: After removing duplicates: " + dictionary.size());
			ArrayList<DictionaryEntry> tempDict = new ArrayList<DictionaryEntry>(runForNLines);
			for (int i = 0; i < runForNLines; i++)
				tempDict.add(dictionary.get(i));
			dictionary = tempDict;
			//System.out.println("TESTING: After removing >nlines: " + dictionary.size());
		}
		
		oldDictionary = Utils.getCopyOfDictionary(dictionary);
	}
	
	public ArrayList<DictionaryEntry> getEntireDictionaryForMeronymProject() {
		ArrayList<DictionaryEntry>  tempDict = new ArrayList<DictionaryEntry>();
		ArrayList<String> opinionFinder = Utils.readFromFile("res/dictionaries/OpinionFinder.txt");
		ArrayList<String> generalInquirer = Utils.readFromFile("res/dictionaries/GeneralInquirer.txt");
		ArrayList<String> appraisalLexicon = Utils.readFromFile("res/dictionaries/AppraisalLexicon.txt");
		ArrayList<String> allList = new ArrayList<String>(opinionFinder.size() + generalInquirer.size() + appraisalLexicon.size());

		allList.addAll(Utils.appendStringToEachItem(opinionFinder, "OF", ","));
		allList.addAll(Utils.appendStringToEachItem(generalInquirer, "GI", ","));
		allList.addAll(Utils.appendStringToEachItem(appraisalLexicon, "AL", ","));

		tempDict = convertToDictionaryEntries(allList, ",");
		removeDuplicatesInDictionary();
		return tempDict;
	}

	public ArrayList<DictionaryEntry> convertToDictionaryEntries(
			ArrayList<String> inputList, String splitBy) {
		return convertToDictionaryEntries(inputList, splitBy, "");
	}
	
	public ArrayList<DictionaryEntry> convertToDictionaryEntries(
			ArrayList<String> inputList, String splitBy, String source) {
		ArrayList<String[]> tempList = Utils.splitListItemsWith(inputList,
				splitBy);
		ArrayList<DictionaryEntry> dictionary = new ArrayList<DictionaryEntry>();
		for (String[] string : tempList) {
			if (string.length >= 3)
				dictionary.add(new DictionaryEntry(string[0].toLowerCase(),
						Utils.getSpeech(string[1]), Utils
								.getPolarity(string[2])));
			if(!source.equalsIgnoreCase(""))
				dictionary.get(dictionary.size()-1).setSource(source);
			else if(string.length > 3)
				dictionary.get(dictionary.size()-1).setSource(string[3]);
		}
		return dictionary;
	}

	// Not Used
	public ArrayList<DictionaryEntry> storeInDictionary(
			ArrayList<DictionaryEntry> toDictionary,
			ArrayList<DictionaryEntry> fromDictionary) {

		if (toDictionary.size() == 0)
			return fromDictionary;

		ArrayList<DictionaryEntry> outDictionary = new ArrayList<DictionaryEntry>(
				fromDictionary.size() + toDictionary.size());

		ArrayList<DictionaryEntry> tempToDictionary = new ArrayList<DictionaryEntry>(
				toDictionary);
		ArrayList<DictionaryEntry> tempFromDictionary = new ArrayList<DictionaryEntry>(
				fromDictionary);

		// for (DictionaryEntry fromEntry : tempFromDictionary)
		// for(DictionaryEntry toEntry : tempToDictionary)
		// if(fromEntry.getWord().equalsIgnoreCase(toEntry.getWord()))
		// if(fromEntry.getSpeech() == toEntry.getSpeech()) {
		// if(fromEntry.getPolarity() != toEntry.getPolarity()) {
		// toDictionary.remove(toEntry);
		// fromDictionary.remove(fromEntry);
		// }
		// } else {
		// toDictionary.remove(toEntry);
		// }
		outDictionary.addAll(fromDictionary);
		outDictionary.addAll(toDictionary);
		return outDictionary;
	}

	public Polarity getPolarityOfWord(String word, Speech speech) {
		for (DictionaryEntry entry : dictionary)
			if (entry.getWord().equalsIgnoreCase(word)
					&& entry.getSpeech().equals(speech))
				return entry.getPolarity();

		return null;
	}

	public void writeDictionaryOutputToFile(String fileName) {
		writeDictionaryOutputToFile(dictionary, fileName);
	}

	public void writeDictionaryOutputToFile(
			ArrayList<DictionaryEntry> dictionary, String fileName) {
		// Utils.print("Writing the dictionary to file");
		BufferedWriter out = null;
		try {
			// String timestamp = new
			// SimpleDateFormat("yyyyMMddhhmm'.txt'").format(new Date());
			FileWriter fstream = new FileWriter(fileName);
			out = new BufferedWriter(fstream);
			for (DictionaryEntry entry : dictionary) {
				out.write(entry.printString());
				out.newLine();
			}
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}

	public void writeSynsetsOutputToFile(String fileName) {
		writeSynsetsOutputToFile(allSynsets, fileName);
	}
	
	public void writeSynsetsOutputToFile(ArrayList<SynsetEntry> outputList, String fileName) {
		BufferedWriter out = null;

		try {
			// Create file
			String timestamp = new SimpleDateFormat("yyyyMMddhhmm'.txt'")
					.format(new Date());
			FileWriter fstream = new FileWriter(fileName);
			out = new BufferedWriter(fstream);
			for (SynsetEntry entry : outputList) {
				out.write(entry.toString());
				out.newLine();
			}
			out.close();

		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}

	public void writeToFile(
			ArrayList<String> list, String fileName) {
		BufferedWriter out = null;
		try {
			// String timestamp = new
			// SimpleDateFormat("yyyyMMddhhmm'.txt'").format(new Date());
			FileWriter fstream = new FileWriter(fileName);
			out = new BufferedWriter(fstream);
			for (String string : list) {
				out.write(string);
				out.newLine();
			}
			out.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error: " + e.getMessage());
		}
	}
	
	public void removeInconsistenciesManually(String dictionaryFileName) {
		dictionary = new ArrayList<DictionaryEntry>();
		ArrayList<String> dictionaryList = Utils
				.readFromFile(dictionaryFileName);
		// Creating Arraylist without duplicate values
		LinkedHashSet<String> listToSet = new LinkedHashSet<String>(
				dictionaryList);
		dictionaryList = new ArrayList<String>(listToSet);

		dictionary = convertToDictionaryEntries(dictionaryList, "\t");
		removeInconsistenciesManually();
	}

	public ArrayList<DictionaryEntry> removeInconsistenciesManually() {
	//	Utils.print("Manually removing inconsistencies in polarity");

		ArrayList<DictionaryEntry> removeList = new ArrayList<DictionaryEntry>(
				dictionary.size());
		for (int i = 0; i < dictionary.size(); i++) {
			int flag = 0;
			for (int j = i + 1; j < dictionary.size(); j++) {
				if (!dictionary.get(i).getComment()
						.equalsIgnoreCase("MarkedWithInconsistency")
						&& dictionary.get(i).getWord()
								.equalsIgnoreCase(dictionary.get(j).getWord()))
					if (dictionary.get(i).getSpeech() == dictionary.get(j)
							.getSpeech()) {
						if (dictionary.get(i).getPolarity() != dictionary
								.get(j).getPolarity()) {
							flag = 1;
							removeList.add(dictionary.get(j));
							dictionary.get(j)
									.setComment("MarkedWithInconsistency");
						}
					}
			}
			if (flag == 1) {
				removeList.add(dictionary.get(i));
				dictionary.get(i).setComment("MarkedWithInconsistency");
			}
		}
		dictionary.removeAll(removeList);
		return removeList;
	}

	public void removeInconsistenciesUsingPolarityInconsistencyProject(
			String wordNetPath, String dictionaryFileName) {
		dictionary = new ArrayList<DictionaryEntry>();
		ArrayList<String> dictionaryList = Utils
				.readFromFile(dictionaryFileName);
		LinkedHashSet<String> listToSet = new LinkedHashSet<String>(
				dictionaryList);
		dictionaryList = new ArrayList<String>(listToSet);

		dictionary = convertToDictionaryEntries(dictionaryList, "\t");
		removeInconsistenciesManually();
	}

	public void removeInconsistenciesUsingPolarityInconsistencyProject(
			String wordNetPath) {
		//System.out.println("Running Polarity Inconsistency Project");
		ArrayList<DictionaryEntry> dictionaryAdj = new ArrayList<DictionaryEntry>();
		ArrayList<DictionaryEntry> dictionaryAdv = new ArrayList<DictionaryEntry>();
		ArrayList<DictionaryEntry> dictionaryNoun = new ArrayList<DictionaryEntry>();
		ArrayList<DictionaryEntry> dictionaryVerb = new ArrayList<DictionaryEntry>();

		for (DictionaryEntry dictionaryEntry : dictionary) {
			switch (dictionaryEntry.getSpeech()) {
			case ADJECTIVE:
				dictionaryAdj.add(dictionaryEntry);
				break;

			case ADVERB:
				dictionaryAdv.add(dictionaryEntry);
				break;

			case NOUN:
				dictionaryNoun.add(dictionaryEntry);
				break;

			case VERB:
				dictionaryVerb.add(dictionaryEntry);
				break;

			default:
				break;
			}
		}

		// writeDictionaryToProperties(dictionaryAdj,"res/PolarityInconsistencyProject/temp/Adjlist.properties");
		// writeDictionaryToProperties(dictionaryAdv,"res/PolarityInconsistencyProject/temp/Advlist.properties");
		// writeDictionaryToProperties(dictionaryNoun,"res/PolarityInconsistencyProject/temp/Nounlist.properties");
		// writeDictionaryToProperties(dictionaryVerb,"res/PolarityInconsistencyProject/temp/Verblist.properties");

		PolarityInconsistency polarityInconsistency = new PolarityInconsistency(
				wordNetPath);
		if (runForSpeech.equals(Speech.ADVERB)
				|| runForSpeech.equals(Speech.ALL)) {
			boolean flag = true;
			while (flag) {
				writeDictionaryToProperties(dictionaryAdv,
						"res/PolarityInconsistencyProject/temp/Advlist.properties");
				Set<String> inconsistentWords = polarityInconsistency
						.processFileToRemoveInconsistencies(
								"res/PolarityInconsistencyProject/temp/Advlist.properties",
								"adverb");
				dictionaryAdv = removeInconsistentWordsFrom(dictionaryAdv,
						inconsistentWords);
				if (inconsistentWords.size() == 0)
					flag = false;
			}
		}
		if (runForSpeech.equals(Speech.ADJECTIVE)
				|| runForSpeech.equals(Speech.ALL)) {
			boolean flag = true;
			while (flag) {
				writeDictionaryToProperties(dictionaryAdj,
						"res/PolarityInconsistencyProject/temp/Adjlist.properties");
				Set<String> inconsistentWords = polarityInconsistency
						.processFileToRemoveInconsistencies(
								"res/PolarityInconsistencyProject/temp/Adjlist.properties",
								"adjective");
				dictionaryAdj = removeInconsistentWordsFrom(dictionaryAdj,
						inconsistentWords);
				if (inconsistentWords.size() == 0)
					flag = false;
			}
		}
		if (runForSpeech.equals(Speech.NOUN) || runForSpeech.equals(Speech.ALL)) {
			boolean flag = true;
			while (flag) {
				writeDictionaryToProperties(dictionaryNoun,
						"res/PolarityInconsistencyProject/temp/Nounlist.properties");
				Set<String> inconsistentWords = polarityInconsistency
						.processFileToRemoveInconsistencies(
								"res/PolarityInconsistencyProject/temp/Nounlist.properties",
								"noun");
				dictionaryNoun = removeInconsistentWordsFrom(dictionaryNoun,
						inconsistentWords);
				if (inconsistentWords.size() == 0)
					flag = false;
			}
		}
		if (runForSpeech.equals(Speech.VERB) || runForSpeech.equals(Speech.ALL)) {
			boolean flag = true;
			while (flag) {
				writeDictionaryToProperties(dictionaryVerb,
						"res/PolarityInconsistencyProject/temp/Verblist.properties");
				Set<String> inconsistentWords = polarityInconsistency
						.processFileToRemoveInconsistencies(
								"res/PolarityInconsistencyProject/temp/Verblist.properties",
								"verb");
				dictionaryVerb = removeInconsistentWordsFrom(dictionaryVerb,
						inconsistentWords);
				if (inconsistentWords.size() == 0)
					flag = false;
			}
		}
		dictionary.clear();
		dictionary.addAll(dictionaryAdv);
		dictionary.addAll(dictionaryAdj);
		dictionary.addAll(dictionaryNoun);
		dictionary.addAll(dictionaryVerb);
		removeDuplicatesInDictionary();
		// writeDictionaryOutputToFile("res/PolarityInconsistencyProject/output/NoInconsistency.txt");
	}

	private ArrayList<DictionaryEntry> removeInconsistentWordsFrom(
			ArrayList<DictionaryEntry> dictionary, Set<String> removeSet) {

		ArrayList<DictionaryEntry> removeList = new ArrayList<DictionaryEntry>();
		for (String string : removeSet)
			for (DictionaryEntry dictionaryEntry : dictionary)
				if (dictionaryEntry.getWord().equalsIgnoreCase(string))
					removeList.add(dictionaryEntry);

		dictionary.removeAll(removeList);
		return dictionary;
	}

	void writeDictionaryToProperties(ArrayList<DictionaryEntry> listname,
			String filename) {

		// Wrinting the arraylist contents into files
		BufferedWriter out = null;
		try {

			FileWriter fstream = new FileWriter(filename);
			out = new BufferedWriter(fstream);
			for (DictionaryEntry entry : listname) {
				out.write(entry.getWord().toLowerCase() + "="
						+ entry.getPolarity().toString().toLowerCase());
				out.newLine();
			}

			out.close();
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
	}

	void invokePolarityInferenceProject(String path) {
		//System.out.println("Running Polarity Inference Project");
		writeDictionaryToSQLDatabase();
		runPIRExe(path);
	}

	void runPIRExe(String path) {
		Process process;
		try {
			
			ProcessBuilder pb = new ProcessBuilder(
					path + "\\PolarityInferenceEngine.exe",
					runForSpeech.toString());
			pb.directory(new File(path + "\\"));
			process = pb.start();

			InputStream is = process.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;
			System.out.println("Polarity Inference Project Output :");
			while ((line = br.readLine()) != null
					&& !line.equalsIgnoreCase("ENDPROGRAM")) {
				System.out.println(line);
			}

			process.destroy();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void writeDictionaryToSQLDatabase() {
		String url = "jdbc:sqlserver://localhost;databaseName=wordnet;integratedSecurity=true";
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection conn = DriverManager.getConnection(url);
			String query = "delete from dbo.InputFromJava";
			PreparedStatement st = conn.prepareStatement(query);
			st.executeUpdate();
			st.close();
			query = "insert into dbo.InputFromJava values (?,?,?)";
			PreparedStatement pStatement = conn.prepareStatement(query);
			int batchSize = 100;
			for (int count = 0; count < dictionary.size(); count++) {
				// System.out.println(dictionary.get(count).getWord().toString());
				pStatement.setString(1, dictionary.get(count).getWord()
						.toString());
				pStatement.setString(2, dictionary.get(count).getSpeech()
						.toString());
				pStatement.setString(3, dictionary.get(count).getPolarity()
						.toString());
				pStatement.addBatch();

				if (count % batchSize == 0) {
					pStatement.executeBatch();
				}
			}

			pStatement.executeBatch(); // for remaining batch queries if total
										// record is odd no.

			// conn.commit();
			pStatement.close();
			conn.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	public int getWordsFromPirOutputAndAddToDictionary(String pirPath) {
		int count = 0;
		ArrayList<String> wordsWithPolarityList = Utils.readFromFile(pirPath
				+ "\\InferredPolarWords.txt");
		ArrayList<String[]> tempList = Utils.splitListItemsWith(
				wordsWithPolarityList, ",");
		for (String[] string : tempList) {
			if (string.length >= 3) {
				if (!string[0].contains(" ")) {
					dictionary.add(new DictionaryEntry(string[0].toLowerCase().replaceAll("\\s", ""),
							runForSpeech, Utils.getPolarity(string[1].replaceAll("\\s", "")), "PolInfer"));
					count++;
				}
			}
		}
		return count;
	}

	public ArrayList<SynsetEntry> readAndStoreSynsets(String inputFileName,
			String splitBy, String source) {
		ArrayList<String> inputList = Utils.readFromFile(inputFileName);
		LinkedHashSet<String> listToSet = new LinkedHashSet<String>(inputList);
		inputList = new ArrayList<String>(listToSet);
		Collections.sort(inputList);
		ArrayList<String[]> tempList = Utils.splitListItemsWith(inputList,
				splitBy);
		ArrayList<SynsetEntry> outputSynsetList = new ArrayList<SynsetEntry>(inputList.size());
		for (String[] string : tempList) {
			if (string.length >= 3)
				outputSynsetList.add(new SynsetEntry(string[0].replaceAll(
						"\\s", "").toLowerCase(), Integer.parseInt(string[1]
						.replaceAll("\\s", "")), Utils.getSpeech(string[2]
						.replaceAll("\\s", "")), Utils.getPolarity(string[3]
						.replaceAll("\\s", "")), source));
		}
		return outputSynsetList;
	}
	
	public int inferPolarityOfWordsFromObtainedSynsets_new(String pathforwnhome,
			WordNetDatabase database, String pirOutputPath, POS pos, String tempPath, int count, int stepCount)
			throws MalformedURLException {
		int i = 0;
		PolarityInferenceOfWordsFromSynsets piws = new PolarityInferenceOfWordsFromSynsets(allSynsets,runForSpeech,dictionary);
		i = piws.inferPolarityOfWordsFromObtainedSynsets(pathforwnhome, database, pirOutputPath, pos, tempPath, count, stepCount);
		dictionary = piws.getDictionary();
		allSynsets = piws.getAllSynsets();
		runForSpeech = piws.getRunForSpeech();
		return i;
	}
	
	public int inferPolarityOfWordsFromObtainedSynsets(String pathforwnhome,
			WordNetDatabase database, String pirOutputPath, POS pos, String tempPath, int count, int stepCount)
			throws MalformedURLException {
		System.out.println("Inferring Polarity of Words from Synsets");
		ArrayList<DictionaryEntry> newWords = new ArrayList<DictionaryEntry>();
		ArrayList<String> processedWords = new ArrayList<String>(allSynsets.size());
		// SynsetType synsetType = new SynsetType(0);
		float positive = 0;
		float negative = 0;
		float neutral = 0;
		float unknown = 0;

		for (SynsetEntry synsetEntry : allSynsets) {
			if(!synsetEntry.getSpeech().equals(runForSpeech))
				continue;
			String word = synsetEntry.getWord();
			if(processedWords.contains(word))
				continue;
			else
				processedWords.add(word);

			positive = 0;
			negative = 0;
			neutral = 0;
			unknown = 0;

			for (SynsetEntry se : allSynsets) {
				if(!se.getSpeech().equals(runForSpeech))
					continue;
				if (se.getWord().equalsIgnoreCase(word)	&& database.getSynsets(word, getSynsetType()).length > se.getSynsetIndex()) {
					Synset[] synsets = database.getSynsets(word,getSynsetType());
					
					if(!(synsets[se.getSynsetIndex()].toString().contains(word)))
						continue;
					
					if(synsets[se.getSynsetIndex()].toString().equalsIgnoreCase("actinozoa"))
						continue;
					
					se.setFrequency(synsets[se.getSynsetIndex()].getTagCount(word));

					switch (se.getPolarity()) {
					case POSITIVE:
						positive += se.getFrequency();
						break;

					case NEGATIVE:
						negative += se.getFrequency();
						break;

					case NEUTRAL:
						neutral += se.getFrequency();
						break;

					case UNKNOWN:
						unknown += se.getFrequency();
						break;
					default:
						break;
					}
				}
			}
			
//
//			System.out.println("word positive total: " + positive);
//			System.out.println("word negative total: " + negative);

			float total = 0;
			if (positive != 0)
				total += positive;
			if (negative != 0)
				total += negative;
			if (negative != 0)
				total += neutral;
			if (neutral != 0)
				total += unknown;

			float wordPositive = 0;
			wordPositive = positive / total;

			float wordNegative = 0;
			wordNegative = negative / total;

			float wordNeutral = 0;
			wordNeutral = neutral / total;

			float wordUnknown = 0;
			wordUnknown = unknown / total;

			Polarity wordPolarity = Polarity.UNKNOWN;

			if (wordPositive > 0.5)
				wordPolarity = Polarity.POSITIVE;

			if (wordNegative > 0.5)
				wordPolarity = Polarity.NEGATIVE;
			
			if (wordNeutral > 0.5 || ((positive+unknown)/total <= 0.5 && (negative+unknown)/total <= 0.5))
				wordPolarity = Polarity.NEUTRAL;

			if (!wordPolarity.equals(Polarity.UNKNOWN))
				newWords.add(new DictionaryEntry(word, runForSpeech,
						wordPolarity, "ObtainedFromInferredSynsets"));
		}
		dictionary.addAll(newWords);

		System.out.println("New words added: " + newWords.size());
		for (DictionaryEntry de : newWords)
			System.out.println(de.printString());
		
		return newWords.size();
	}
	
	public SynsetType getSynsetType() {
		switch(runForSpeech) {
		case ADJECTIVE:
			return SynsetType.ADJECTIVE;
			
		case ADVERB:
			return SynsetType.ADVERB;
			
		case NOUN:
			return SynsetType.NOUN;
			
		case VERB:
			return SynsetType.VERB;
			
		default:
			return SynsetType.ADVERB;
		}
	}
	
	public boolean checkIfDictionaryChanged() {
		boolean flag = true;
		System.out.println("oldDictionary size: " + oldDictionary.size());
		System.out.println("current dictionary size: " + dictionary.size());
		ArrayList<String> oldList = new ArrayList<String>(oldDictionary.size());
		ArrayList<String> currentList = new ArrayList<String>(dictionary.size());
		
		for(DictionaryEntry de : oldDictionary)
			oldList.add(de.getWord()+","+de.getSpeech().toString()+","+de.getPolarity().toString());
		for(DictionaryEntry de : dictionary)
			currentList.add(de.getWord()+","+de.getSpeech().toString()+","+de.getPolarity().toString());
		
		if(oldList.size()==currentList.size() && oldList.containsAll(currentList)) {
			flag = false;
		}
		
		oldDictionary = Utils.getCopyOfDictionary(dictionary);
//		System.out.println("AFTER: olddictionarysize: " + oldDictionary.size());
//		System.out.println("AFTER: currentictionarysize: " + dictionary.size());
		return flag;
	}
	
	public boolean checkIfSynsetsChanged() {
		boolean flag = true;
		System.out.println("oldsynsets size: " + oldSynsets.size());
		System.out.println("current synsets size: " + allSynsets.size());
		ArrayList<String> oldList = new ArrayList<String>(oldDictionary.size());
		ArrayList<String> currentList = new ArrayList<String>(allSynsets.size());
		
		for(SynsetEntry de : oldSynsets)
			oldList.add(de.getWord()+","+de.getSynsetIndex()+","+de.getSpeech().toString()+","+de.getPolarity().toString());
		for(SynsetEntry de : allSynsets)
			currentList.add(de.getWord()+","+de.getSynsetIndex()+","+de.getSpeech().toString()+","+de.getPolarity().toString());
		
		if(oldList.size()==currentList.size() && oldList.containsAll(currentList)) {
			flag = false;
		}
		
		oldSynsets = Utils.getCopyOfSynsets(allSynsets);
		return flag;
	}
	
	//Not used
	public void discardPIRSynsetsWithMultiplePolarities(String pirOutputFile) {
		ArrayList<String> inputList = Utils.readFromFile(pirOutputFile);
		LinkedHashSet<String> listToSet = new LinkedHashSet<String>(inputList);
		inputList = new ArrayList<String>(listToSet);
		Collections.sort(inputList);
		ArrayList<String[]> tempList = Utils.splitListItemsWith(inputList, ",");
		ArrayList<SynsetEntry> outputSynsetList = new ArrayList<SynsetEntry>(inputList.size());
		//dont do anything
	}
	
	public ArrayList<SynsetEntry> discardSynsetsWithMultiplePolarities() {
		Utils.print("Manually removing inconsistencies in polarity in synsets");
		ArrayList<SynsetEntry> removeList = new ArrayList<SynsetEntry>(
				allSynsets.size());
		HashMap<String, ArrayList<SynsetEntry>> synsetMap = new HashMap<String, ArrayList<SynsetEntry>>(allSynsets.size());
		for(SynsetEntry se : allSynsets) {
			if(synsetMap.containsKey(se.getWord())) {
				synsetMap.get(se.getWord()).add(se);
			} else {
				ArrayList<SynsetEntry> newList = new ArrayList<SynsetEntry>();
				newList.add(se);
				synsetMap.put(se.getWord(), newList);				
			}
		}
		
		for (Entry<String, ArrayList<SynsetEntry>> entry : synsetMap.entrySet()) {
			if(entry.getValue().size()>1) {
				for(int i=0;i<entry.getValue().size();i++) {
					boolean flag = false; 
					for(int j=i+1;j<entry.getValue().size();j++) {
						if(entry.getValue().get(i).isInconsistentWith(entry.getValue().get(j))) {
							removeList.add(entry.getValue().get(j));
							flag = true;
						}
					}
					if(flag)
						removeList.add(entry.getValue().get(i));
				}
			}
		}
		
		/*
		for (int i = 0; i < allSynsets.size(); i++) {
			boolean flag = false;
			for (int j = i + 1; j < allSynsets.size(); j++) {
				if (!removeList.contains(allSynsets.get(i))
						&& !removeList.contains(allSynsets.get(j))
						&& allSynsets.get(i).isInconsistentWith(
								allSynsets.get(j))) {
					removeList.add(allSynsets.get(j));
					flag = true;
				}
				if(flag)
					removeList.add(allSynsets.get(i));
			}
			
		}*/
		allSynsets.removeAll(removeList);
		removeList = sortSynsets(removeList);
		return removeList;
	}
	

	public void removeDuplicatesInSynsets() {
		LinkedHashSet<SynsetEntry> listToSet = new LinkedHashSet<SynsetEntry>(allSynsets);
		allSynsets = new ArrayList<SynsetEntry>(listToSet);
		sortSynsets();
	}
	
	//Not used
	public void removeDuplicatesInSynsets2() {
		ArrayList<String> allList = new ArrayList<String>();
		for (SynsetEntry de : allSynsets) {
//			System.out.println(de.toString());
			allList.add(de.getWord().toString() + ","
					+ de.getSynsetIndex() + ","
					+ de.getSpeech().toString() + ","
					+ de.getPolarity().toString());
		}
		LinkedHashSet<String> listToSet = new LinkedHashSet<String>(allList);
		allList = new ArrayList<String>(listToSet);
		Collections.sort(allList);
		ArrayList<String[]> tempList = Utils.splitListItemsWith(allList, ",");
		allSynsets = new ArrayList<SynsetEntry>(allList.size());
		for (String[] string : tempList) {
			if (string.length >= 3)
				allSynsets.add(new SynsetEntry(string[0].replaceAll(
						"\\s", "").toLowerCase(), Integer.parseInt(string[1]
						.replaceAll("\\s", "")), Utils.getSpeech(string[2]
						.replaceAll("\\s", "")), Utils.getPolarity(string[3]
						.replaceAll("\\s", ""))));
		}
	}
	
	public void runAntonymProject(String wordnetPath, String pirOutputFile, POS pos) throws IOException {
	//	System.out.println("Running Antonym Project");
		Antonym antonym = new Antonym();
		antonym.executeAntonym(wordnetPath, pirOutputFile, pos);
	}
	
	public void runSimilarToProject(String wordnetPath, String pirOutputFile, POS pos) throws IOException {
	//	System.out.println("Running SimilarTo Project");
		SimilarToRule1 similartoRule1 = new SimilarToRule1();
		similartoRule1.testDictionary(wordnetPath, pirOutputFile, pos);
	}
	
	public void runHyponymProject(String wordnetPath, String pirOutputFile, POS pos) throws IOException {
	//	System.out.println("Running Hyponym Project");
		HyponymRule1 hyponymRule1 = new HyponymRule1();
		hyponymRule1.testDictionary(wordnetPath, pirOutputFile, pos);
	}
	
	public int getSynsetsFromPirOutputAndAddToSynsets(String fileName) {
		int count = 0;
		ArrayList<String> synsetsWithPolarity = Utils.readFromFile(fileName);
		ArrayList<SynsetEntry> addList = convertStringsToSynsets(synsetsWithPolarity, "PolInfer");
		allSynsets.addAll(addList);
		return addList.size();
	}
	
	public ArrayList<SynsetEntry> convertStringsToSynsets(ArrayList<String> synsetsWithPolarity, String source) {
		ArrayList<String[]> tempList = Utils.splitListItemsWith(
				synsetsWithPolarity, ",");
		ArrayList<SynsetEntry> addList = new ArrayList<SynsetEntry>(tempList.size());
		for (String[] string : tempList) {
			if (string.length >= 4) {
				if (!string[0].contains(" ") && Utils.getPolarity(string[3].replaceAll("\\s", "")) != null) {
					String src = source;
					if(src.equals(""))
						src = string[4].replaceAll("\\s", "");
					addList.add(new SynsetEntry(string[0].toLowerCase()
							.replaceAll("\\s", ""), Integer.parseInt(string[1]
							.replaceAll("\\s", "")), Utils.getSpeech(string[2].replaceAll("\\s", "")), Utils
							.getPolarity(string[3].replaceAll("\\s", "")), src));
				}
			}
		}
		return addList;
	}
	
	/**
	 * This method is used to remove the words which
	 * have synsets count more than n, mentioned in 
	 * the configuration properties using the property
	 * maxSynsetsForAWord
	 * @param n
	 * @param pathforwnhome
	 * @param database
	 * @return
	 */
	public ArrayList<DictionaryEntry> removeWordsWithMoreThanXSynsets(int n, String pathforwnhome,WordNetDatabase database) {
		ArrayList<DictionaryEntry> removeList = new ArrayList<DictionaryEntry>(dictionary.size());
		int i = 0;
		for(DictionaryEntry de : dictionary) {
		//	if(i%200 == 0)
		//		System.out.println("Ran removeWordsWithMoreThan"+n+"Synsets() for " + i + "/" + dictionary.size());
			Synset[] synsets = database.getSynsets(de.getWord(),getSynsetType());
			if(synsets.length>n)
				removeList.add(de);
			i++;
		}
		dictionary.removeAll(removeList);
		return removeList;
	}
	
	public void sortSynsets() {
		ArrayList<String> inputList = new ArrayList<String>(allSynsets.size());
		for(SynsetEntry se : allSynsets)
			inputList.add(se.toString());
		Collections.sort(inputList);
		allSynsets = convertStringsToSynsets(inputList, "");
	}
	
	public ArrayList<SynsetEntry> sortSynsets(ArrayList<SynsetEntry> inputSynsets) {
		ArrayList<String> inputList = new ArrayList<String>(inputSynsets.size());
		for(SynsetEntry se : inputSynsets)
			inputList.add(se.getWord()+","+se.getSynsetIndex()+","+se.getSpeech().toString()+","+se.getPolarity().toString()+","+se.getSource());
		Collections.sort(inputList);
		inputSynsets = convertStringsToSynsets(inputList, "");
		return inputSynsets;
	}
}
