package sentimentdictionaryapplication;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import meronym.Meronym;
import data.DictionaryEntry;
import data.SynsetEntry;
import data.Utils;
import data.Utils.Speech;
import edu.mit.jwi.item.POS;
import edu.smu.tspell.wordnet.WordNetDatabase;

public class SentimentWordDictionaryMain {
	public static void main(String[] args) throws IOException, Exception {

		ArrayList<String> log = new ArrayList<String>();
		boolean keepRunning = true;
		Properties prop = new Properties();
		prop.load(new FileInputStream("lib/configuration.properties"));
		String linesString = prop.getProperty("line_count");
		int no_of_lines = 0;

		try {
			if (linesString != null && !linesString.equalsIgnoreCase(""))
				no_of_lines = Integer.parseInt(linesString);
		} catch (NumberFormatException e) {
			no_of_lines = 0;
		}

		linesString = prop.getProperty("synset_count");
		int maxSynsetsForAWord = 0;

		try {
			if (linesString != null && !linesString.equalsIgnoreCase(""))
				maxSynsetsForAWord = Integer.parseInt(linesString);
		} catch (NumberFormatException e) {
			maxSynsetsForAWord = 0;
		}

		Speech partOfSpeech = Utils.getSpeech(prop.getProperty("partOfSpeech"));
		String PIRPath = prop.getProperty("polarityInferenceProjectPath");
		String tempPath = prop.getProperty("output_path") + "\\temp\\";
		Utils.createTempDir(tempPath);

		String tempVariable = prop.getProperty("writeDictionaryAfterEachLoop");
		boolean writeLoop = false;
				if (tempVariable.equalsIgnoreCase("true"))
			writeLoop = true;

		tempVariable = prop.getProperty("writeDictionaryAfterEachStep");
		boolean writeStep = false;
		if (tempVariable.equalsIgnoreCase("true"))
			writeStep = true;

		tempVariable = prop.getProperty("Meronym");
		boolean runMeronym = false;
		if (tempVariable.equalsIgnoreCase("true"))
			runMeronym = true;

		tempVariable = prop.getProperty("PolarityInconsistencyProject");
		boolean runPolarityInconsistency = false;
		if (tempVariable.equalsIgnoreCase("true"))
			runPolarityInconsistency = true;

		tempVariable = prop.getProperty("PolarityInferenceProject");
		boolean runPIR = false;
		if (tempVariable.equalsIgnoreCase("true"))
			runPIR = true;

		tempVariable = prop.getProperty("AntonymProject");
		boolean runAntonym = false;
		if (tempVariable.equalsIgnoreCase("true"))
			runAntonym = true;

		tempVariable = prop.getProperty("HyponymProject");
		boolean runHyponym = false;
		if (tempVariable.equalsIgnoreCase("true"))
			runHyponym = true;

		tempVariable = prop.getProperty("SimilarToProject");
		boolean runSimilarTo = false;
		if (tempVariable.equalsIgnoreCase("true"))
			runSimilarTo = true;

		int tempCount = 0;

		// Initialize the database, dictionaries
		System.setProperty("wordnet.database.dir",prop.getProperty("wordnet3path") + "dict");
		WordNetDatabase database = WordNetDatabase.getFileInstance();
		SWDOperations swd = new SWDOperations();

		System.out.println("PART OF SPEECH: " + partOfSpeech.toString());
		log.add("Running Project for " + partOfSpeech.toString());



		swd.combineSmallSentimentDictionaries(partOfSpeech, no_of_lines);
		System.out.println("PROGRAM STATUS: Combining the three dictionaries into one");

		if (writeStep) {
			String tmpPath = tempPath + "\\pre-process";
			Utils.createTempDir(tmpPath);
			swd.writeDictionaryOutputToFile(tempPath + "\\pre-process\\InitialDictionary.txt");
			System.out.println("FILE OUTPUT: COMBINED DICTIONARY -> " + tempPath + "\\pre-process\\InitialDictionary.txt");
		}

		log.add(" Number of words in combined dictionary: " + swd.dictionary.size());

		tempCount = swd.dictionary.size();

		System.out.println("PROGRAM STATUS: Removing duplicates in dictionary");
		swd.removeDuplicatesInDictionary();

		if (writeStep) {
			swd.writeDictionaryOutputToFile(tempPath + "\\pre-process\\DinctionaryAfterDuplicatesRemoval.txt");
			System.out.println("FILE OUTPUT : dictionary - After duplicates removal ->  " + tempPath + "\\pre-process\\DinctionaryAfterDuplicatesRemoval.txt");
		}

		log.add(" Number of duplicate words: " + (tempCount - swd.dictionary.size()));
		log.add(" Number of words after removing duplicates: " + swd.dictionary.size());

		if(maxSynsetsForAWord > 0) {
			ArrayList<DictionaryEntry> dictionaryXSynsets = swd.removeWordsWithMoreThanXSynsets(maxSynsetsForAWord, prop.getProperty("wordnet3path"), database);
			if (writeStep) {
		//		swd.writeDictionaryOutputToFile(tempPath + "\\pre-process\\WordsHavingMaxSynsetsRemoved.txt");
		//		System.out.println("FILE OUTPUT: dictionary - After removing words with synsets >" + maxSynsetsForAWord +" -> "+ tempPath + "\\pre-process\\WordsHavingMaxSynsetsRemoved.txt");
			}
			log.add(" Number of words with > " + maxSynsetsForAWord + " synsets: " + dictionaryXSynsets.size());
			log.add(" Number of words in after removing words with > " + maxSynsetsForAWord + " synsets: " + swd.dictionary.size());
		}

		/*
		 * Initial Steps:
		 * Find Synsets With Neutral Polarity using the Meronym Project
		 */
		if (runMeronym) {

			System.out.println("PROGRAM STATUS : Running Meronym Project");
			Meronym meronym = new Meronym(swd, database, partOfSpeech);

			if (writeStep) {
				swd.writeSynsetsOutputToFile(tempPath + "SynsetsFromMeronym.txt");
				System.out.println("FILE OUTPUT : Synsets obtained from Meronym Project -> " + tempPath + "SynsetsFromMeronym.txt");
			}

			log.add(" Number of synsets obtained from Meronym Project: " + swd.allSynsets.size());
			int synsetsSize = swd.allSynsets.size();

			// Remove any duplicates in synsets
			swd.removeDuplicatesInSynsets();

			if (writeStep) {
				swd.writeSynsetsOutputToFile(tempPath + "SynsetsFromMeronymAfterDuplicateRemoval.txt");
				swd.writeSynsetsOutputToFile("res/meronym/PreviouslyExecuted.txt");
				System.out.println("FILE OUTPUT : Synsets obtained from Meronym Project after duplicates removal -> " + tempPath + "SynsetsFromMeronymAfterDuplicateRemoval.txt");
			}

			log.add(" Number of duplicate synsets: " + (synsetsSize-swd.allSynsets.size()));
			log.add(" Number of synsets after removing duplicates: " + swd.allSynsets.size());
		}

		int count = 1;
		int stepCount = 1;

		/**
		 * Run each cycle until the size of the dictionary at
		 * the start of the cycle and the end of the cycle
		 * is the same
		 */
		while (keepRunning) {

			log.add("\nCYCLE  -> " + count);

			if (writeStep) {
				swd.writeDictionaryOutputToFile(tempPath +"Cycle"+count + "-Step" 	+ (stepCount) + "StartingDictionary.txt");
				System.out.println("STATUS: Dictionary at the start of cycle : " + tempPath +"Cycle"+ count + "-Step" + (stepCount++) + "StartingDictionary.txt");
			}

			log.add(" Number of words at start of cycle: " + swd.dictionary.size());
			tempCount = swd.dictionary.size();

			/*Step 2.a: Remove inconsistencies manually*/
			System.out.println("Running inconsistency manual removal project \n");
			ArrayList<DictionaryEntry> removeList = swd.removeInconsistenciesManually();

			if (writeStep) {
				swd.writeDictionaryOutputToFile(removeList, tempPath+"Cycle" + count + "-Step" + (stepCount) + "InconsistentWordsFoundManually.txt");
				System.out.println("Words with inconsistency in polarity having the same speech ->"
						+ tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount++)
						+ "InconsistentWordsFoundManually.txt");

				swd.writeDictionaryOutputToFile(tempPath +"Cycle"+ count + "-Step"	+ (stepCount) + "DictionaryAfterManualInconsistencyRemoval.txt");
				System.out.println("Dictionary after removing inconsistency in polarity with words having the same speech -> "
								+ tempPath +"Cycle"
								+ count
								+ "-Step"
								+ (stepCount++)
								+ "DictionaryAfterManualInconsistencyRemoval.txt");
			}
			log.add(" Number of words with same speech and different polarity: " + (tempCount-swd.dictionary.size()));
			log.add(" Number of words after removing the words with same speech and different polarity: " + swd.dictionary.size());

			/*Step 2.b Remove inconsistencies using PolarityInconsistencyProject*/
			if (runPolarityInconsistency) {
				if(maxSynsetsForAWord > 0) {
					ArrayList<DictionaryEntry> dictionaryXSynsets = swd.removeWordsWithMoreThanXSynsets(maxSynsetsForAWord, prop.getProperty("wordnet3path"), database);
					if (writeStep) {
//						swd.writeDictionaryOutputToFile(tempPath +"Cycle"
//								+ count
//								+ "-Step"
//								+ (stepCount)
//								+ "WordsHavingMaxSynsetsRemoved.txt");
//						System.out
//								.println("Output dictionary after removing words having no. of synsets > " + maxSynsetsForAWord +"->"
//										+ tempPath +"Cycle"+ count
//										+ "-Step"
//										+ (stepCount++)
//										+ "WordsHavingMaxSynsetsRemoved.txt");
					}
					log.add(" Number of words with > " + maxSynsetsForAWord + " synsets: " + dictionaryXSynsets.size());
					log.add(" Number of words in after removing words with > " + maxSynsetsForAWord + " synsets: " + swd.dictionary.size());
				}

				tempCount = swd.dictionary.size();

				swd.removeInconsistenciesUsingPolarityInconsistencyProject(prop.getProperty("wordnet2.1path"));
				System.out.println("Running Polarity Inconsistency Project \n");
				if (writeStep) {

					swd.writeDictionaryOutputToFile(tempPath +"Cycle"+ count + "-Step"	+ (stepCount) + "DictionaryAfterRunningPolarityInconsistencyProject.txt");
					System.out.println("Dictionary after removing inconsistency using polarity inconsistency project written to "
									+ tempPath +"Cycle"
									+ count
									+ "-Step"
									+ (stepCount++)
									+ "DictionaryAfterRunningPolarityInconsistencyProject.txt");
				}
				log.add(" Number of inconsistent words returned by Polarity Inconsistency Project: " + (tempCount-swd.dictionary.size()));
				log.add(" Number of words after removing the inconsistent words: " + swd.dictionary.size());
			}

			/*Step 3. Polarity Inference Project*/
			if (runPIR) {

				System.out.println("Running Polarity Inference Project \n");
				swd.invokePolarityInferenceProject(PIRPath);

				tempCount = swd.getWordsFromPirOutputAndAddToDictionary(PIRPath);

				log.add(" Number of words in output of Polarity Inference Project : " + tempCount);
				log.add(" Number of words after adding the words from Polarity Inference Project : " + swd.dictionary.size());

				if (writeStep) {
					swd.writeDictionaryOutputToFile(tempPath +"Cycle"+ count + "-Step"	+ (stepCount) + "DictionaryAfterAddingWordsFromPolarityInferenceProject.txt");
					System.out.println("Dictionary after adding new words from Polarity Inference Project "
									+ tempPath +"Cycle"
									+ count
									+ "-Step"
									+ (stepCount++)
									+ "DictionaryAfterAddingWordsFromPolarityInferenceProject.txt");
				}

				tempCount = swd.dictionary.size();

				System.out.println("Running Manual Inconsistency Removal after Polarity Inference Project");
				removeList = swd.removeInconsistenciesManually();

				if (writeStep) {
					swd.writeDictionaryOutputToFile(removeList, tempPath +"Cycle"+ count + "-Step" 	+ (stepCount) + "InconsistentWordsAfterManualInconsistencyProject.txt");
					System.out.println("Words with inconsistency in polarity having the same speech written to "
							+ tempPath +"Cycle"
							+ count
							+ "-Step"
							+ (stepCount++)
							+ "InconsistentWordsAfterManualInconsistencyProject.txt");

					swd.writeDictionaryOutputToFile(tempPath +"Cycle"+ count + "-Step" + (stepCount) + "DictionaryAfterManualInconsistencyRemovalOfPolarityInference.txt");

					System.out.println("Dictionary after manually removing inconsistency written to "
									+ tempPath +"Cycle"
									+ count
									+ "-Step"
									+ (stepCount++)
									+ "DictionaryAfterManualInconsistencyRemovalOfPolarityInference.txt");
				}
				log.add(" Number of words with same speech and different polarity: " + (tempCount-swd.dictionary.size()));
				log.add(" Number of words after removing the words with same speech and different polarity: " + swd.dictionary.size());

				/*Running polarity inconsistency project*/
				if (runPolarityInconsistency) {

					System.out.println("Running Polarity Inconsistency Project");
					if(maxSynsetsForAWord > 0) {
						ArrayList<DictionaryEntry> dictionaryXSynsets = swd.removeWordsWithMoreThanXSynsets(maxSynsetsForAWord, prop.getProperty("wordnet3path"), database);
						if (writeStep) {
//							swd.writeDictionaryOutputToFile(tempPath +"Cycle"
//									+ count
//									+ "-Step"
//									+ (stepCount)
//									+ "WordsHavingMaxSynsetsRemoved.txt");
//							System.out.println("Dictionary after words having synsets >"+maxSynsetsForAWord+" removed -> "
//											+ tempPath +"Cycle"+ count
//											+ "-Step"
//											+ (stepCount++)
//											+ "WordsHavingMaxSynsetsRemoved.txt");
						}
						log.add(" Number of words with > " + maxSynsetsForAWord + " synsets: " + dictionaryXSynsets.size());
						log.add(" Number of words in after removing words with > " + maxSynsetsForAWord + " synsets: " + swd.dictionary.size());
					}

					tempCount = swd.dictionary.size();
					swd.removeInconsistenciesUsingPolarityInconsistencyProject(prop.getProperty("wordnet2.1path"));
					if (writeStep) {
						swd.writeDictionaryOutputToFile(tempPath +"Cycle"+ count + "-Step"
								+ (stepCount)
								+ "DictionaryAfterPolarityInferenceAndPolarityInconsistency.txt");
						System.out
								.println("Dictionary after removing inconsistency using polarity inconsistency project written to "
										+ tempPath
										+ count
										+ "-"
										+ (stepCount++)
										+ "DictionaryAfterPolarityInferenceAndPolarityInconsistency.txt");
					}
					log.add(" Number of inconsistent words returned by Polarity Inconsistency Project: " + (tempCount-swd.dictionary.size()));
					log.add(" Number of words after removing the inconsistent words: " + swd.dictionary.size());
				}
			}

			String pirOutputFile = PIRPath + "\\InferredPolarSenses.txt";
			POS pos = POS.valueOf(partOfSpeech.toString());

			/*
			 * 7. For the synsets, check and discard the synsets with
			 * multiple polarities.
			 */
			log.add("  Number of current synsets: " + swd.allSynsets.size());
			log.add("  Number of synsets obtained from Polarity Inference Project: " + swd.getSynsetsFromPirOutputAndAddToSynsets(pirOutputFile));
			log.add("  Number of current synsets after adding the ones from Polarity Inference Project: " + swd.allSynsets.size());

			if (writeStep) {
				swd.writeSynsetsOutputToFile(tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount)
						+ "SynsetsAfterAddingPolarityInferenceProjectOutputSynsets.txt");
				System.out.println("Synsets after adding Polarity Inference Project -> " + tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount++)
						+ "SynsetsAfterAddingPolarityInferenceProjectOutputSynsets.txt");
			}
			tempCount = swd.allSynsets.size();
			swd.removeDuplicatesInSynsets();
			if (writeStep) {
				swd.writeSynsetsOutputToFile(tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount)
						+ "SynsetsAfterRemovingDuplicates.txt");
				System.out.println("Synsets after removing duplicates written to " + tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount++)
						+ "SynsetsAfterRemovingDuplicates.txt");
			}
			log.add("  Number of duplicate synsets: " + (tempCount - swd.allSynsets.size()));
			log.add("  Number of synsets after removing duplicates: " + swd.allSynsets.size());

			tempCount = swd.allSynsets.size();

			// Hyponym Project
			if (runHyponym) {
				swd.runHyponymProject(prop.getProperty("wordnet3path"),	pirOutputFile, pos);
			}

			// SimilarTo Project
			if (runSimilarTo) {
				swd.runSimilarToProject(prop.getProperty("wordnet3path"),pirOutputFile, pos);
			}

			// Antonym Project
			if (runAntonym) {
				swd.runAntonymProject(prop.getProperty("wordnet3path"),	pirOutputFile, pos);
			}

			tempCount = swd.allSynsets.size();
			swd.allSynsets.addAll(swd.readAndStoreSynsets("res/HyponymProject/output/output.txt", ",", "HyponymProject"));

			if (runHyponym && writeStep) {
				swd.writeSynsetsOutputToFile(tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount)
						+ "SynsetsAddedAfterRunningHyponym.txt");
				System.out.println("Synsets after adding from hyponym written to " + tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount++)
						+ "SynsetsAddedAfterRunningHyponym.txt");
			}
			log.add("  Number of synsets obtained from hyponym project: " + (swd.allSynsets.size() - tempCount));

			tempCount = swd.allSynsets.size();

			swd.allSynsets.addAll(swd.readAndStoreSynsets("res/SimilarToProject/output/output.txt", ",", "SimilarToProject"));

			if (runSimilarTo && writeStep) {
				swd.writeSynsetsOutputToFile(tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount)
						+ "SynsetsAddedAfterRunningSimilarTo.txt");
				System.out.println("Synsets after adding from similarto written to " + tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount++)
						+ "SynsetsAddedAfterRunningSimilarTo.txt");
			}
			log.add("  Number of synsets obtained from similarto project: " + (swd.allSynsets.size() - tempCount));
			tempCount = swd.allSynsets.size();

			swd.allSynsets.addAll(swd.readAndStoreSynsets("res/AntonymProject/output/output.txt", ",", "AntonymProject"));

			if (runAntonym && writeStep) {
				swd.writeSynsetsOutputToFile(tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount)
						+ "SynsetsAddedAfterRunningAntonym.txt");
				System.out.println("Synsets after adding from antonym written to " + tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount++)
						+ "SynsetsAddedAfterRunningAntonym.txt");
			}
			log.add("  Number of synsets obtained from antonym project: " + (swd.allSynsets.size() - tempCount));
			log.add("  Number of synsets after adding the synsets from hyponym, antonym and similar-to projects: " + swd.allSynsets.size());


			tempCount = swd.allSynsets.size();
			swd.removeDuplicatesInSynsets();
			if (writeStep) {
				swd.writeSynsetsOutputToFile(tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount)
						+ "SynsetsAfterRemovingDuplicates.txt");
				System.out.println("Synsets after removing duplicates written to " + tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount++)
						+ "SynsetsAfterRemovingDuplicates.txt");
			}
			log.add("  Number of duplicate synsets: " + (tempCount - swd.allSynsets.size()));
			log.add("  Number of synsets after removing duplicates: " + swd.allSynsets.size());
			tempCount = swd.allSynsets.size();

			ArrayList<SynsetEntry> removeSynsetsList = swd.discardSynsetsWithMultiplePolarities();

			if (writeStep) {
				swd.writeSynsetsOutputToFile(removeSynsetsList, tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount)
						+ "InconsistentSynsets.txt");
				System.out.println("Synsets with inconsistencies written to " + tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount++)
						+ "InconsistentSynsets.txt");
				swd.writeSynsetsOutputToFile(tempPath + "Cycle"
						+ count
						+ "-Step"
						+ (stepCount)
						+ "SynsetsAfterRemovingInconsistencies.txt");
				System.out.println("Synsets after removing inconsistencies written to " + tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount++)
						+ "SynsetsAfterAddingRemovingInconsistencies.txt");
			}
			log.add("  Number of synsets with multiple polarities: " + (tempCount - swd.allSynsets.size()));
			log.add("  Number of synsets after removing synsets with multiple polarities: " + swd.allSynsets.size());

			/*Infer polarity of words from obtained synsets*/
			tempCount = swd.inferPolarityOfWordsFromObtainedSynsets(prop.getProperty("wordnet3path"), database, pirOutputFile,pos, tempPath, count, stepCount);
			//stepCount++;

			if (writeStep) {
				swd.writeDictionaryOutputToFile(tempPath +"Cycle"+ count + "-Step"
						+ (stepCount)
						+ "AfterInferringPolarityOfWordsObtainedFromSynsets.txt");
				System.out.println("Dictionary after inference of polarity of words obtained from synsets "
								+ tempPath +"Cycle"
								+ count
								+ "-Step"
								+ (stepCount++)
								+ "AfterInferringPolarityOfWordsObtainedFromSynsets.txt");
			}
			log.add(" Number of new words obtained after inferring polarity: " + tempCount);
			log.add(" Number of words after adding the new words: " + swd.dictionary.size());

			tempCount = swd.dictionary.size();
			removeList = swd.removeInconsistenciesManually();

			if (writeStep) {
				swd.writeDictionaryOutputToFile(removeList, tempPath +"Cycle"+ count + "-Step"	+ (stepCount) + "InconsistentWordsUsingManualRemoval.txt");
				System.out.println("Words with inconsistency in polarity having the same speech written to "
						+ tempPath +"Cycle"
						+ count
						+ "-Step"
						+ (stepCount++)
						+ "InconsistentWordsUsingManualRemoval.txt");
				swd.writeDictionaryOutputToFile(tempPath +"Cycle"+ count + "-Step"
						+ (stepCount) + "DictionaryAfterManualRemoval.txt");
				System.out.println("Dictionary after manually removing inconsistency written to "
								+ tempPath +"Cycle"
								+ count
								+ "-Step"
								+ (stepCount++)
								+ "DictionaryAfterManualRemoval.txt");
			}
			log.add(" Number of words with same speech and different polarity: " + (tempCount-swd.dictionary.size()));
			log.add(" Number of words after removing the words with same speech and different polarity: " + swd.dictionary.size());

			if (runPolarityInconsistency) {
				if(maxSynsetsForAWord > 0) {
					ArrayList<DictionaryEntry> dictionaryXSynsets = swd.removeWordsWithMoreThanXSynsets(maxSynsetsForAWord, prop.getProperty("wordnet3path"), database);
					if (writeStep) {
//						swd.writeDictionaryOutputToFile(tempPath +"Cycle"
//								+ count
//								+ "-Step"
//								+ (stepCount)
//								+ "WordsHavingMaxSynsetsRemoved.txt");
//
//						System.out.println("Output dictionary after removing duplicates written to "
//										+ tempPath +"Cycle"+ count
//										+ "-Step"
//										+ (stepCount++)
//										+ "WordsHavingMaxSynsetsRemoved.txt");
					}
					log.add(" Number of words with > " + maxSynsetsForAWord + " synsets: " + dictionaryXSynsets.size());
					log.add(" Number of words in after removing words with > " + maxSynsetsForAWord + " synsets: " + swd.dictionary.size());
				}

				tempCount = swd.dictionary.size();
				swd.removeInconsistenciesUsingPolarityInconsistencyProject(prop.getProperty("wordnet2.1path"));
				if (writeStep) {
					swd.writeDictionaryOutputToFile(tempPath +"Cycle"+ count + "-Step"
							+ (stepCount)
							+ "DictionaryAfterPolarityInconsistencyProject.txt");
					System.out
							.println("Dictionary after removing inconsistency using polarity inconsistency project written to "
									+ tempPath +"Cycle"
									+ count
									+ "-Step"
									+ (stepCount++)
									+ "DictionaryAfterPolarityInconsistencyProject.txt");
				}
				log.add(" Number of inconsistent words returned by Polarity Inconsistency Project: " + (tempCount-swd.dictionary.size()));
				log.add(" Number of words after removing the inconsistent words: " + swd.dictionary.size());
			}

			log.add(" Dictionary Size at Start of Loop: " + swd.oldDictionary.size());
			log.add(" Dictionary Size at End of Loop: " + swd.dictionary.size());
			log.add("  Synsets Size at Start of Loop: " + swd.oldSynsets.size());
			log.add("  Synsets Size at End of Loop: " + swd.allSynsets.size());

			keepRunning = swd.checkIfDictionaryChanged();

			if (swd.checkIfSynsetsChanged())
				keepRunning = true;

			stepCount = 1;
			count++;
		}

		swd.writeDictionaryOutputToFile(tempPath + "..//SentimentDictionary.txt");
		swd.writeSynsetsOutputToFile(tempPath + "..//FinalSynsets.txt");

		log.add("Please find the output written to output/SentimentDictionary.txt and output/FinalSynsets.txt");
		FileWriter fw = new FileWriter(tempPath + "log.txt");
		BufferedWriter bw = new BufferedWriter(fw);
		System.out.println("Sentiment Word Dictionary Log written to: " + tempPath + "log.txt");
		for(String s : log) {
			//System.out.println(s);
			bw.write(s);
			bw.write("\n");
		}
		bw.close();
	}
}
