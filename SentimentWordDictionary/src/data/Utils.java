package data;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import sentimentdictionaryapplication.SWDOperations;


public class Utils {

	public static void print(int i) {
		System.out.println(i);
	}
	
	public static void print(String str) {
		System.out.println(str);
	}
	
	public enum Speech {
	    NOUN, VERB, ADJECTIVE, ADVERB, ALL 
	}
	
	public enum Polarity {
	    POSITIVE, NEGATIVE, NEUTRAL, UNKNOWN
	}
	
	public static Speech getSpeech(String speech) {
		speech = speech.toUpperCase();
		switch(speech) {
		case "NOUN":
			return Speech.NOUN;
		
		case "ADJECTIVE":
			return Speech.ADJECTIVE;
			
		case "VERB":
			return Speech.VERB;
			
		case "ADVERB":
			return Speech.ADVERB;
			
		case "ALL":
			return Speech.ALL;
		default:
			return null;
		}
	}
	
	public static Polarity getPolarity(String polarity) {
		polarity = polarity.toUpperCase();
		switch(polarity) {
		case "POSITIVE":
			return Polarity.POSITIVE;
		
		case "NEGATIVE":
			return Polarity.NEGATIVE;
			
		case "NEUTRAL":
			return Polarity.NEUTRAL;
			
		case "UNKNOWN":
			return Polarity.UNKNOWN;
			
		default:
			return null;
		}
	}
	
	public static ArrayList<String> readFromFile(String filename) {
		long startTime = System.currentTimeMillis();
		//Utils.print("Starting to read from file: " + filename);
		ArrayList<String> tempList = new ArrayList<String>();
		try {
			File f = new File(filename);
			if(f.exists()) {
			FileInputStream fstream = new FileInputStream(filename);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));

			String strLine;

			// Read File Line By Line

			while ((strLine = br.readLine()) != null) {
				tempList.add(strLine);
			}
			in.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		long totalTime = System.currentTimeMillis()-startTime;
		//Utils.print("Finished reading from file: " + filename + " took " + (totalTime/1000));
		return tempList;
	}
	
	public static ArrayList<String[]> splitListItemsWith(ArrayList<String> inputList, String split) {
		ArrayList<String[]> tempList = new ArrayList<String[]>();
		for (String string : inputList)
			tempList.add(string.split(split));		
		return tempList;
	}
	
	public static String tagger(String string) {
		return SWDOperations.tagger.tagString(string);
	}
	
	public static HashMap<String, Speech> tagString(String string) {
		HashMap<String, Speech> tempMap = new HashMap<String, Speech>();
		String taggedString = tagger(string);
		String[] process_word = taggedString.split(" ");
		for(String word : process_word) {
			String[] splitWord = process_word[0].split("_");
			tempMap.put(splitWord[0], getSpeechOfWord(splitWord[1]));
		}
		return tempMap;
	}
	
	public static Speech getSpeechOfWord(String string) {
		if (string.startsWith("NN"))
			return Speech.NOUN;
		else if (string.startsWith("VB"))
			return Speech.VERB;
		else if (string.startsWith("JJ"))
			return Speech.ADJECTIVE;
		else if (string.equals("RB"))
			return Speech.ADVERB;
		else
			return null;
	}
	
	public static String formatSynsetFromString(String[] string) {
		String temp = "";
		for(String s : string)
			temp += "," + s;
		temp = temp.length()>0 ? temp.substring(1) : temp; 
		temp = "{" + temp + "}";
		return temp;
	}	
	
	public static ArrayList<DictionaryEntry> getCopyOfDictionary(ArrayList<DictionaryEntry> inputDict) {
		ArrayList<DictionaryEntry> dictionaryCopy = new ArrayList<DictionaryEntry>(inputDict.size());
		
		for(DictionaryEntry de : inputDict)
			dictionaryCopy.add(new DictionaryEntry(de.getWord(), de.getSpeech(), de.getPolarity()));

		return dictionaryCopy;
	}
	
	public static ArrayList<SynsetEntry> getCopyOfSynsets(ArrayList<SynsetEntry> inputSynsets) {
		ArrayList<SynsetEntry> synsetCopy = new ArrayList<SynsetEntry>(inputSynsets.size());
		
		for(SynsetEntry de : inputSynsets)
			synsetCopy.add(new SynsetEntry(de.getWord(), de.getSynsetIndex(), de.getSpeech(), de.getPolarity()));

		return synsetCopy;
	}
	
	public static ArrayList<String> appendStringToEachItem(ArrayList<String> inputList, String item, String joinWith) {
		ArrayList<String> outputList = new ArrayList<String>(inputList.size());
		for(String s : inputList)
			outputList.add(s + joinWith + item);
		return outputList;
	}
	
	public static boolean deleteDir(File dir) {
	    if (dir.isDirectory()) {
	        String[] children = dir.list();
	        for (int i=0; i<children.length; i++) {
	            boolean success = deleteDir(new File(dir, children[i]));
	            if (!success) {
	                return false;
	            }
	        }
	    }
	    return dir.delete();
	}
	
	public static void createTempDir(String path) {
		File file = new File(path);
		deleteDir(file);
		if(!file.mkdir())
			System.out.println("Could not create temp output directory!");
	}
}
