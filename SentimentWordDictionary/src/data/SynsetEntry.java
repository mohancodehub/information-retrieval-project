package data;

import data.Utils.Polarity;
import data.Utils.Speech;



public class SynsetEntry {
	
	String word;
	int synsetIndex;
	Speech speech;
	Polarity polarity;
	int frequency;
	String comment;
	String source;
	
	public SynsetEntry(String word, int synsetIndex, Speech speech, Polarity polarity) {
		this.word = word;
		this.synsetIndex = synsetIndex;
		this.speech = speech;
		this.polarity = polarity;
		this.frequency = -1;
		this.comment="";
		this.source="";
	}
	
	public SynsetEntry(String word, int synsetIndex, Speech speech, Polarity polarity, String source) {
		this.word = word;
		this.synsetIndex = synsetIndex;
		this.speech = speech;
		this.polarity = polarity;
		this.source = source;
		this.frequency = -1;
		this.comment="";
	}
	
	public String toString() {
		return (word + "," + synsetIndex + "," + speech + "," + polarity + "," + source);
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public int getSynsetIndex() {
		return synsetIndex;
	}
	public void setSynsetIndex(int synsetIndex) {
		this.synsetIndex = synsetIndex;
	}
	public Speech getSpeech() {
		return speech;
	}
	public void setSpeech(Speech speech) {
		this.speech = speech;
	}
	public Polarity getPolarity() {
		return polarity;
	}
	public void setPolarity(Polarity polarity) {
		this.polarity = polarity;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
//		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result + frequency;
		result = prime * result
				+ ((polarity == null) ? 0 : polarity.hashCode());
//		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((speech == null) ? 0 : speech.hashCode());
		result = prime * result + synsetIndex;
		result = prime * result + ((word == null) ? 0 : word.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SynsetEntry other = (SynsetEntry) obj;
		if (polarity != other.polarity)
			return false;
		if (speech != other.speech)
			return false;
		if (synsetIndex != other.synsetIndex)
			return false;
		if (word == null) {
			if (other.word != null)
				return false;
		} else if (!word.equals(other.word))
			return false;
		return true;
	}
	
	public boolean isInconsistentWith(SynsetEntry other) {
		if(word.equalsIgnoreCase(other.word)
				&& synsetIndex == other.synsetIndex
				&& speech.equals(other.speech)
				&& !polarity.equals(other.polarity))
			return true;
		else
			return false;
	}
}
